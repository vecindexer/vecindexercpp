#!/usr/bin/env python
# coding utf-8

import argparse
import faiss
import numpy as np
import os
import glob
import logging
import time

from make_sample import read_embs


ResultHeapL2 = faiss.ResultHeap

###########################################
# ResultHeapIP
###########################################

class ResultHeapIP:
    """Accumulate query results from a sliced dataset. The final result will
    be in self.D, self.I."""

    def __init__(self, nq, k):
        " nq: number of query vectors, k: number of results per query "
        self.I = np.zeros((nq, k), dtype='int64')
        self.D = np.zeros((nq, k), dtype='float32')
        self.nq, self.k = nq, k
        heaps = faiss.float_minheap_array_t()
        heaps.k = k
        heaps.nh = nq
        heaps.val = faiss.swig_ptr(self.D)
        heaps.ids = faiss.swig_ptr(self.I)
        heaps.heapify()
        self.heaps = heaps

    def add_result(self, D, I):
        """D, I do not need to be in a particular order (heap or sorted)"""
        assert D.shape == (self.nq, self.k)
        assert I.shape == (self.nq, self.k)
        self.heaps.addn_with_ids(
            self.k, faiss.swig_ptr(D),
            faiss.swig_ptr(I), self.k)

    def finalize(self):
        self.heaps.reorder()

def create_base_index(args, d):
    if args.metric == 'L2':
        index = faiss.IndexFlatL2(d)
    elif args.metric == 'IP':
        index = faiss.IndexFlatIP(d)
    else:
        raise RuntimeError ("Unknown metric! %s" % args.metric )

    if hasattr(args, 'verbose'):
        index.verbose = args.verbose

    return index


def create_flat_index(args, d):
    index = create_base_index(args, d)
    if args.gpu:
        res = faiss.StandardGpuResources()
        index = faiss.index_cpu_to_gpu(res, 0, index)

    return index


def make_D_I_fnames(fname, args):
    out_fname_base = os.path.join(args.out_dir,
                                  '_'.join([os.path.basename(args.sample_embs),
                                            os.path.basename(fname).split('.')[0]]))
    D_fname = out_fname_base + '_D.npz'
    I_fname = out_fname_base + '_I.npz'
    return D_fname, I_fname


def process(fname, sample_embs, args):
    top = args.top

    D_out, I_out = make_D_I_fnames(fname, args)

    if os.path.exists(D_out) and os.path.exists(I_out):
        logging.info("Skip {} (out files already exist)".format(fname))
        return False

    index = create_flat_index(args, sample_embs.shape[1])

    embs = read_embs(fname)
    if args.metric == "IP":
        faiss.normalize_L2(embs)

    index.add(embs)

    D, I = index.search(sample_embs, top)

    np.savez(D_out, D)
    np.savez(I_out, I)
    return True



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--embs_dir", "-ed", default="embs",
                        help="input directory with *npz files with vectors")
    parser.add_argument("--sample_embs", "-se", default="embs_sample.npz",
                        help="npz file with sample")
    parser.add_argument("--top", "-t", default=100, type=int,
                        help="top N nearest neigbours")
    parser.add_argument("--out_dir", "-o", default='exp1',
                        help="output directory with D and I")
    parser.add_argument("--metric", default="L2", type=str,
                        help="")
    parser.add_argument("--merge", action="store_true",
                        help="")
    parser.add_argument("--gpu", default=True, type=bool,
                        help="")
    parser.add_argument("--verbose", "-v", action="store_true",
                        help="")

    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO,
                        format=FORMAT)

    logging.debug("Args: %s", args)

    sample_embs = read_embs(args.sample_embs)
    if args.metric == "IP":
        faiss.normalize_L2(sample_embs)

    if args.merge:
        if args.metric == "L2":
            result_heap = ResultHeapL2(nq=sample_embs.shape[0], k=args.top)
        elif args.metric == "IP":
            result_heap = ResultHeapIP(nq=sample_embs.shape[0], k=args.top)

        current_shift = 0

    for fname in sorted(glob.glob(os.path.join(args.embs_dir, '*npz'))):
        start = time.time()
        b = process(fname, sample_embs, args)
        end = time.time()
        if b:
            logging.info("Processed {}, time elapsed: {}".format(fname, end-start))

        if not args.merge:
            continue

        start = time.time()
        D_fname, I_Fname = make_D_I_fnames(fname, args)
        D = read_embs(D_fname)
        I = read_embs(I_Fname)
        embs = read_embs(fname)
        result_heap.add_result(D=D, I=I+current_shift)
        current_shift += embs.shape[0]
        end = time.time()
        logging.info("Merged {}, time elapsed: {}".format(fname, end - start))

    if args.merge:
        start = time.time()
        result_heap.finalize()
        end = time.time()
        logging.info("result_heap Finalized. Time elapsed: {}".format(end - start))
        np.savez(os.path.join(args.out_dir,"merged_O.npz"), result_heap.D)
        np.savez(os.path.join(args.out_dir, "merged_I.npz"), result_heap.I)


if __name__ == '__main__':
    main()