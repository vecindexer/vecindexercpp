#!/usr/bin/env python
# coding utf-8

import argparse
import logging
import os
import numpy as np


def convert(args):

    with open(args.input_file, 'r') as f:
        shape = [0, 0]
        curr_id = None
        curr_vec_size = 0
        for line in f.readlines():
            i, _, _ = line.split(args.input_sep)
            curr_vec_size += 1
            if i != curr_id:
                shape[0] += 1
                shape[1] = curr_vec_size
                curr_id = i
                curr_vec_size = 0

    resI = np.ndarray(shape, np.dtype('int64'))
    resO = np.ndarray(shape, np.dtype('float32'))

    with open(args.input_file, 'r') as f:
        curr_i = 0
        line = f.readline()
        curr_id, fi, d = line.split(args.input_sep)
        curr_I = [int(fi, 16)]
        curr_O = [float(d)]
        for line in f.readlines():
            i, fi, d = line.split(args.input_sep)
            if i != curr_id:
                curr_id = i
                resI[curr_i][:] = np.array(curr_I, np.dtype('int64'))
                resO[curr_i][:] = np.array(curr_O, np.dtype('float32'))
                curr_I = []
                curr_O = []
                curr_i += 1
            curr_I.append(int(fi, 16))
            curr_O.append(float(d))

        np.savez(os.path.join(args.output_dir,
                              os.path.basename(args.input_file) + "_I.npz"),
                 resI)
        np.savez(os.path.join(args.output_dir,
                              os.path.basename(args.input_file) + "_O.npz"),
                 resO)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", "-i", required=True,
                        help="")
    parser.add_argument("--input_sep", "-s", default=',')
    parser.add_argument("--output_dir", "-o", required=True,
                        help="")
    parser.add_argument("--verbose", "-v", action="store_true",
                        help="")

    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO,
                        format=FORMAT)

    logging.debug("Args: %s", args)
    convert(args)

if __name__ == '__main__':
    main()
