#!/usr/bin/env python
# coding utf-8

import argparse
import logging
from make_sample import read_embs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--ground_truth_I", "-gi", default="exp1/merged_I.npz",
                        help="etalon I (that got by brute-force)")
    parser.add_argument("--ground_truth_O", "-go", default="exp1/merged_O.npz",
                        help="etalon O (that got by brute-force)")

    parser.add_argument("--sample_embs", "-se", default="embs_sample.npz",
                        help="npz file with sample")
    parser.add_argument("--top", "-t", default=100, type=int,
                        help="top N nearest neigbours")

    parser.add_argument("--input_i", "-ii", required=True,
                        help="I (that got by another method)")
    parser.add_argument("--input_o", "-io", required=True,
                        help="O (that got by another method)")


    parser.add_argument("--verbose", "-v", action="store_true",
                        help="")

    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO,
                        format=FORMAT)

    #sample_embs = read_embs(args.sample_embs)

    gt_I = read_embs(args.ground_truth_I)
    #gt_O = read_embs(args.ground_truth_O)

    I = read_embs(args.input_i)
    #O = read_embs(args.input_o)



    print('\t', '1-R@1\t1-R@10\t1-R@100')
    for rank in 1, 10, 100:
        n_ok = (I[:, :rank] == gt_I[:, :1]).sum()
        print("\t%.4f" % (n_ok / float(gt_I.shape[0])), end=' ')
    print ('')

    print('\t', '1-R@2\t1-R@10\t1-R@100')
    for rank in 2, 10, 100:
        n_ok = (I[:, :rank] == gt_I[:, 1:2]).sum()
        print("\t%.4f" % (n_ok / float(gt_I.shape[0])), end=' ')
    print ('')

    print('\t', '1-R@3\t1-R@10\t1-R@100')
    for rank in 2, 10, 100:
        n_ok = (I[:, :rank] == gt_I[:, 2:3]).sum()
        print("\t%.4f" % (n_ok / float(gt_I.shape[0])), end=' ')
    print ('')





if __name__ == '__main__':
    main()
