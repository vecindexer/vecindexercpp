#!/usr/bin/env python
# coding utf-8

import argparse
import faiss
import logging
import glob
import os
import time
import numpy as np

from make_sample import read_embs
from brute_force_exp import create_base_index


def locate_index(args, index):
    if args.no_precomputed_tables:
        if isinstance(index, faiss.IndexIVFPQ):
            logging.info("disabling precomputed table")
            index.use_precomputed_table = -1
            index.precomputed_table.clear()

    if hasattr(args, 'verbose'):
        if hasattr(index, 'verbose'):
            index.verbose = args.verbose
        if hasattr(index, 'cp') and hasattr(index.cp, 'verbose'):
            index.cp.verbose = args.verbose
        if hasattr(index, 'pq') and hasattr(index.pq, 'cp') and hasattr(index.pq.cp, 'verbose'):
            index.pq.cp.verbose = args.verbose

    if args.gpu:
        co = faiss.GpuClonerOptions()
        # here we are using a 64-byte PQ, so we must set the lookup tables to
        # 16 bit float (this is due to the limited temporary memory).
        if args.usefloat16:
            co.useFloat16 = True
        res = faiss.StandardGpuResources()
        index = faiss.index_cpu_to_gpu(res, 0, index, co)

    return index


def create_index_ivf(args, d):
    coarse_quantizer = create_base_index(args, d)

    if args.metric == 'L2':
        metric = faiss.METRIC_L2
    elif args.metric == 'IP':
        metric = faiss.METRIC_INNER_PRODUCT
    else:
        raise RuntimeError ("Unknown metric! %s" % args.metric )

    index = faiss.IndexIVFPQ(coarse_quantizer,
                             d,
                             args.nclusters,
                             args.pq,
                             args.bits,
                             metric)
    logging.debug("Created IndexIVFPQ with metric {}".format(metric))

    if hasattr(args, 'max_points_per_centroid') and isinstance(args.max_points_per_centroid, (int,)):
        if hasattr(index, 'cp') and hasattr(index.cp, 'max_points_per_centroid'):
            index.cp.max_points_per_centroid = args.max_points_per_centroid
            logging.debug("Increase .cp.max_points_per_centroid = {}".format(args.max_points_per_centroid))

        if hasattr(index, 'pq') and hasattr(index.pq, 'cp') and hasattr(index.pq.cp, 'max_points_per_centroid'):
            index.pq.cp.max_points_per_centroid = args.max_points_per_centroid
            logging.debug("Increase .pq.cp.max_points_per_centroid = {}".format(args.max_points_per_centroid))

    return locate_index(args, index)


def save_index(args, index):
    if args.gpu:
        start = time.time()
        index = faiss.index_gpu_to_cpu(index)
        end = time.time()
        logging.info("Index converted. Time elapsed: {}".format(end - start))
    start = time.time()
    faiss.write_index(index, args.index_file)
    end = time.time()
    logging.info("Index saved to {}. Time elapsed: {}".format(args.index_file, end - start))


def read_and_split_to_batches(embs_fname, batch_size=1000000):
    start = time.time()
    embs = read_embs(embs_fname)
    end = time.time()
    logging.info("Loaded embs from {}. Time elapsed: {}".format(embs_fname, end - start))
    logging.info("Embs shape {}.".format(embs.shape))

    for i in range(0,embs.shape[0],batch_size):
        yield i, embs[i:i+batch_size]


def train(args):
    index = None

    for i, train_embs in read_and_split_to_batches(args.train_embs, args.batch_size):
        if index is None:
            start = time.time()
            index = create_index_ivf(args, train_embs.shape[1])
            end = time.time()
            logging.info("Index object created. Time elapsed: {}".format(end - start))

        if args.metric == "IP":
            start = time.time()
            faiss.normalize_L2(train_embs)
            end = time.time()
            logging.info("Embs l2-normalizesd. Time elapsed: {}".format(end - start))

        start = time.time()
        index.train(train_embs)
        end = time.time()
        logging.info("Index trained. Time elapsed: {}".format(end - start))

        break

    save_index(args, index)


def do_index(args):
    start = time.time()
    index = locate_index(args, faiss.read_index(args.index_file))
    end = time.time()
    logging.info("Index loaded from {}. Time elapsed: {}".format(args.index_file, end - start))


    for i, fname in enumerate(sorted(glob.glob(os.path.join(args.embs_dir, '*npz')))):
        start = time.time()
        embs = read_embs(fname)
        if args.metric == "IP":
            faiss.normalize_L2(embs)
        index.add(embs)

        end = time.time()
        logging.info("[{}] Added from {} embs {}. Time elapsed: {}".format(i, fname, embs.shape, end - start))

    save_index(args, index)


def process(args):
    start = time.time()
    index = locate_index(args, faiss.read_index(args.index_file))
    index.nprobe = args.nprobe
    end = time.time()
    logging.info("Index loaded from {}. Time elapsed: {}".format(args.index_file, end - start))

    # result_heap = faiss.ResultHeap(nq=sample_embs.shape[0], k=args.top)
    #
    # for i, sample_embs in read_and_split_to_batches(args.sample_embs, args.batch_size):
    #     logging.info("Current batch size {}".format(sample_embs.shape))
    #     start = time.time()
    start = time.time()
    sample_embs = read_embs(args.sample_embs)
    end = time.time()
    logging.debug("Sample embs loaded. Sample size: {}. Time elapsed: {}".format(sample_embs.shape, end - start))

    if args.metric == "IP":
        start = time.time()
        faiss.normalize_L2(sample_embs)
        end = time.time()
        logging.debug("Sample embs L2-normalized. Time elapsed: {}".format(end - start))

    start = time.time()
    D, I = index.search(sample_embs, args.top)
    # logging.debug("Result D.shape: {}".format(D.shape))
    # logging.debug("Result I.shape: {}".format(D.shape))
    # result_heap.add_result(D=D, I=I)

    end = time.time()
    logging.info("Search finised. Time elapsed: {}".format(end - start))

    # start = time.time()
    # result_heap.finalize()
    # end = time.time()
    # logging.info("result_heap Finalized. Time elapsed: {}".format(end - start))

    pfix="_".join((args.metric, args.nclusters, args.per_centr, 'gpu' if args.gpu else 'no-gpu'))
    np.savez(os.path.join(args.out_dir, "merged_{}_O.npz".format(pfix)), D)
    np.savez(os.path.join(args.out_dir, "merged_{}_I.npz".format(pfix)), I)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--embs_dir", "-ed", default="embs",
                        help="input directory with *npz files with vectors")
    parser.add_argument("--train_embs", "-te", default="embs_sample_15.npz",
                        help="train vectors")
    parser.add_argument("--sample_embs", "-se", default="embs_sample.npz",
                        help="npz file with sample")
    parser.add_argument("--top", "-t", default=100, type=int,
                        help="top N nearest neigbours")
    parser.add_argument("--nprobe", default=100, type=int,
                        help="top N nearest neigbours per cluster")

    parser.add_argument('--gpu', dest='gpu', action='store_true')
    parser.add_argument('--no-gpu', dest='gpu', action='store_false')
    parser.set_defaults(gpu=True)


    parser.add_argument('--no_precomputed_tables', dest='no_precomputed_tables', action='store_true')
    parser.set_defaults(no_precomputed_tables=False)


    parser.add_argument('--usefloat16', dest='usefloat16', action='store_true')
    parser.add_argument('--no-usefloat16', dest='usefloat16', action='store_false')
    parser.set_defaults(usefloat16=True)

    parser.add_argument("--nclusters", default=16384, type=int,
                        help="")
    parser.add_argument("--pq", default=64, type=int,
                        help="")
    parser.add_argument("--bits", default=8, type=int,
                        help="")
    parser.add_argument("--max_points_per_centroid", default=256, type=int,
                        help="")

    parser.add_argument("--index_file", default='index_ivf_exp2_l2.findex',
                        help="")
    # parser.add_argument("--train_by_batches", action="store_true",
    #                     help="")
    parser.add_argument("--batch_size", type=int, default=100000,
                        help="")
    parser.add_argument("--do_index", action="store_true",
                        help="")
    parser.add_argument("--process", action="store_true",
                        help="")

    parser.add_argument("--out_dir", "-o", default='exp2',
                        help="output directory with D and I")
    parser.add_argument("--metric", default="L2", type=str,
                        help="")
    parser.add_argument("--verbose", "-v", action="store_true",
                        help="")

    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO,
                        format=FORMAT)


    logging.debug("Args: %s", args)

    if not os.path.exists(args.index_file):
        train(args)
    else:
        logging.info("Index file {} already exists. Skip train step!".format(args.index_file))

    if args.do_index:
        do_index(args)
    if args.process:
        process(args)


if __name__ == '__main__':
    main()
