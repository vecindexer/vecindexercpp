#!/usr/bin/env python
# coding utf-8

import argparse
import numpy as np
import glob
import logging
import os

def read_embs(fname, key='arr_0'):
    with np.load(fname) as _data:
        return _data[key]


def make_sample(embs, seed=42, percent=1):
    np.random.seed(seed)
    sample_ids = np.random.randint(embs.shape[0], size=int(percent * embs.shape[0] / 100))
    return embs.shape[0], sample_ids, embs[sample_ids, :]


def sample_batch_generator(args, fnames):
    for fname in fnames:
        yield fname, make_sample(read_embs(fname), seed=args.random_seed, percent=args.percent)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--embs_dir", "-ed", default="embs",
                        help="input directory with *npz files with vectors")
    parser.add_argument("--random_seed", "-rs", default=42,
                        help="seed for random")
    parser.add_argument("--percent", type=float, default=0.1,
                        help="percent for making sample data")
    parser.add_argument("--sample_embs", "-se", default='embs_sample.npz',
                        help="output sample file with embs")
    parser.add_argument("--sample_ids", "-si", default='embs_sample_ids.npz',
                        help="output sample file with ids")
    parser.add_argument("--no_copy_but_two_forward", action="store_true")
    parser.add_argument("--verbose", "-v", action="store_true",
                        help="")

    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO,
                        format=FORMAT)

    sample_embs = None
    sample_ids = None

    fnames = sorted(glob.glob(os.path.join(args.embs_dir, '*npz')))

    if args.no_copy_but_two_forward:
        x, y, dtype = 0, None, None
        curr_shift = 0
        for fname in fnames:
            embs = read_embs(fname)
            if y is None:
                y = embs.shape[1]
            else:
                assert (y == embs.shape[1] )

            if dtype is None:
                dtype = embs.dtype
            else:
                assert (dtype == embs.dtype )

            x += int(args.percent * embs.shape[0] / 100)
            curr_shift += embs.shape[0]

        sample_embs = np.zeros((x, y), dtype)
        sample_ids = np.zeros((x,), np.int64)
        logging.info("Future sample size: {}, {}, {}".format(x,y,dtype))
        current_first = 0
        current_shift = 0
        for fname, (batch_sample_size, batch_sample_ids, sample_embs_) in sample_batch_generator(args, fnames):
            fr, to = current_first, current_first+sample_embs_.shape[0]
            sample_embs[fr:to] = sample_embs_[:]
            sample_ids[fr:to] = batch_sample_ids + current_shift
            logging.info("After processing {} filled from {} to {}. Ex. {}: {}".format(fname, fr, to, fr, sample_ids[fr]))
            current_first += sample_embs_.shape[0]
            current_shift += batch_sample_size

    else:
        current_shift = 0
        for fname, (batch_sample_size, batch_sample_ids, sample_embs_) in sample_batch_generator(args, fnames):
            if sample_embs is None and sample_ids is None:
                sample_embs = sample_embs_
                sample_ids = batch_sample_ids
            else:
                sample_embs = np.concatenate((sample_embs, sample_embs_), axis=0)
                sample_ids = np.concatenate((sample_ids, batch_sample_ids + current_shift))
                current_shift += batch_sample_size
            logging.info("After processing {} there are shape {}".format(fname, sample_embs.shape))

    np.savez(args.sample_embs, sample_embs)
    with open(args.sample_ids, 'w') as f:
        f.writelines([str(i)+'\n' for i in sample_ids])

if __name__ == '__main__':
    main()
