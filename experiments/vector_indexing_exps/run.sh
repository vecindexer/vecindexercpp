#!/usr/bin/env bash

train_size=15000000

process_batch_size=10000
te="embs_sample/embs_sample_15.npz"
se="embs_sample/embs_sample_0.1.npz"
si="embs_sample/embs_sample_ids_0.1.txt"
ed="embs"

metrics="L2 IP"
nclusters_list="8192 16384 32768"
#per_centr_list="100000 1000000 5000000"
per_centr_list="1024 4096 8192"

function make_sample()
{
  mkdir -p make_sample embs_sample
  python make_sample.py -ed $ed -se $se -si $si --no_copy_but_two_forward -v &>> make_sample/make_sample.log
}


function brute_force()
{
  mkdir -p brute_force_exp exp1L2 exp1IP
  python brute_force_exp.py -ed $ed -se $se -t 100 --metric=L2 -o exp1L2 --merge &>> brute_force_exp/brute_force_exp_L2.log
  python brute_force_exp.py -ed $ed -se $se -t 100 --metric=IP -o exp1IP --merge &>> brute_force_exp/brute_force_exp_IP.log
}


function train_and_index()
{

    ### DEFAULT PARAMS ###
    for metric in IP ;
    do
      d="exp2_index_ivf" ;
      mkdir -p $d ;

      pfix=$metric"_default" ;

      python ./index_ivf_exp.py -ed $ed -te $te -se $se --no-gpu --index_file=$d/$d"_"$pfix".findex" --do_index --batch_size=$train_size --metric=$metric -o $d -v &>> $d/$d"_"$pfix"_train_and_indexing.log" ;

    done

    ### PARAMS GRID ###
    d="exp2_index_ivf" ;
    mkdir -p $d ;

    for nclusters in $nclusters_list ;
      do
        for per_centr in $per_centr_list ;
          do
            for metric in $metrics ;
              do
                pfix=$metric"_"$nclusters"_"$per_centr ;
                python ./index_ivf_exp.py -ed $ed -te $te -se $se --no-gpu --index_file=$d/$d"_"$pfix".findex" --do_index --batch_size=$train_size --metric=$metric --max_points_per_centroid=$per_centr --nclusters=$nclusters -o $d -v &>> $d/$d"_"$pfix"_train_and_indexing.log" ;
              done;
          done ;
      done ;
}


function process()
{
    for metric in $metrics ;
    do
      ### DEFAULT PARAMS ###
      d="exp2_index_ivf" ;
      pfix=$metric"_default" ;

      python ./index_ivf_exp.py -ed $ed -te $te -se $se --no-gpu --index_file=$d/$d"_"$pfix".findex" --process --batch_size=$train_size --metric=$metric -o $d -v &>> $d/$d"_"$pfix"_process.log" ;

      ### PARAMS GRID ###
      for nclusters in $nclusters_list ;
      do
        for per_centr in $per_centr_list ;
          do
            pfix=$metric"_"$nclusters"_"$per_centr ;
            python ./index_ivf_exp.py -ed $ed -te $te -se $se --no-gpu --index_file=$d/$d"_"$pfix".findex" --process --batch_size=$train_size --metric=$metric --max_points_per_centroid=$per_centr --nclusters=$nclusters -o $d -v &>> $d/$d"_"$pfix"_process.log" ;
          done;
      done ;

    done ;
}

function process_gpu()
{
    for metric in $metrics ;
    do
      ### DEFAULT PARAMS ###
      d="exp2_index_ivf" ;
      pfix=$metric"_default" ;

      python ./index_ivf_exp.py -ed $ed -te $te -se $se --gpu --index_file=$d/$d"_"$pfix".findex" --process --batch_size=$train_size --metric=$metric -o $d -v &>> $d/$d"_"$pfix"_process_gpu.log" ;

      ### PARAMS GRID ###
      for nclusters in $nclusters_list ;
      do
        for per_centr in $per_centr_list ;
          do
            pfix=$metric"_"$nclusters"_"$per_centr ;
            python ./index_ivf_exp.py -ed $ed -te $te -se $se --gpu --index_file=$d/$d"_"$pfix".findex" --process --batch_size=$train_size --metric=$metric --max_points_per_centroid=$per_centr --nclusters=$nclusters -o $d -v &>> $d/$d"_"$pfix"_process_gpu.log" ;
          done;
      done ;

    done ;
}

function milvus_index()
{
    python ex_pipeline/vec_indexing_tasks.py -v vec_uploading -c 100 -p etc/vec_uploading/params.json --embeddings_path=/home/vvyadrincev/develop/vector_indexing_exps/embs &>> vec_indexing_milvus.log &
}


function milvus_search()
{
    python ex_pipeline/vec_searching_tasks.py -v vec_batch_search -c 100 -p etc/vec_searching/params.json --ids_file=/home/vvyadrincev/develop/vector_indexing_exps/embs_sample/embs_sample_ids_0.1.txt -o /home/vvyadrincev/develop/vector_indexing_exps/exp3_milvus_IP/result_100_100.txt &>> vec_searching_milvus.log &
}


#make_sample
#brute_force
train_and_index
process
process_gpu

#milvus_index
#milvus_search

