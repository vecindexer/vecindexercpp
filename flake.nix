{
  description = "Vector search engine";

  inputs.nixpkgs.url = "nixpkgs";

  outputs = { self, nixpkgs }:

    let cuda-overlay = final: prev:{
          #enable cuda support
          nvidia-thrust = prev.nvidia-thrust.override{deviceSystem="CUDA";};
          faiss = prev.faiss.override{cudaSupport=true;};
        } ;


    pkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [ cuda-overlay self.overlays.default ];
          config = {allowUnfree = true;};
        };
    in {
      overlays.default = final: prev: {
        vec_indexer = final.callPackage ./nix {src=self; };
      };

      defaultPackage.x86_64-linux = pkgs.vec_indexer;
      packages.x86_64-linux = {

        inherit (pkgs)
          vec_indexer;

      };

      devShell.x86_64-linux =
        pkgs.mkShell {

          inputsFrom = [ pkgs.vec_indexer ];
          buildInputs = [
            pkgs.ccls
            pkgs.gdb
          ];

          shellHook=''
            #https://github.com/NixOS/nixpkgs/issues/11390
            export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/nvidia/current/:$LD_LIBRARY_PATH
            '';


        };
    };

}
