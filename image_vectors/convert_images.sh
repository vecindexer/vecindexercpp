#!/usr/bin/env bash

mkdir -p result

while true ;
do
    for d in downloads/* ;
    do
        python image_to_1darray.py --delete_after -m resnet18 --process_dir $d --out_dir  result &>> image_to_1darray.log ;
    done
    echo Sleep 120; 
    sleep 120 ;
done ;
