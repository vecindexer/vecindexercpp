#!/usr/bin/env bash

for f in image_ids_and_rotation_split_lines/* ;
do
    mkdir -p downloads/`basename $f` ;
    python downloader.py $f --num_processes 8  --download_folder downloads/`basename $f` &>> download.log ;
done