# python3
# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Open Images image downloader.

This script downloads a subset of Open Images images, given a list of image ids.
Typical uses of this tool might be downloading images:
- That contain a certain category.
- That have been annotated with certain types of annotations (e.g. Localized
Narratives, Exhaustively annotated people, etc.)

The input file IMAGE_LIST should be a text file containing one image per line
with the format <SPLIT>/<IMAGE_ID>, where <SPLIT> is either "train", "test",
"validation", or "challenge2018"; and <IMAGE_ID> is the image ID that uniquely
identifies the image in Open Images. A sample file could be:
  train/f9e0434389a1d4dd
  train/1a007563ebc18664
  test/ea8bfd4e765304db

"""

import argparse
from concurrent import futures
import logging
import os
import pandas as pd
import requests
import sys

import boto3
import botocore

BUCKET_NAME = 'open-images-dataset'


def download_one_image(bucket, image_id, split, orig_url, thumbnail_url, download_folder):
  out_file = os.path.join(download_folder, f'{image_id}.jpg')
  try:
    bucket.download_file(f'{split}/{image_id}.jpg',
                         out_file)
    logging.info(f"Downloaded {image_id} from bucket {bucket}.")
  except botocore.exceptions.ClientError as exception:
    try:
        res = requests.get(orig_url)
        if not res:
            logging.error(f"Failed to download {image_id} by orig_url {orig_url}. Code: {res.status_code}. Skip!")
        else:
            logging.info(f"Downloaded {image_id} by orig_url {orig_url}.")
        if not res:
            res = requests.get(thumbnail_url)
            if not res:
                logging.error(f"Failed to download {image_id} by thumbnail_url {thumbnail_url}. Code: {res.status_code}.")
            else:
                logging.info(f"Downloaded {image_id} by thumbnail_url {thumbnail_url}.")
        if res:
            with open(out_file, 'wb') as of:
                of.write(res.content)
    except Exception as exception:
        logging.error(f"Failed to download {image_id}. Exception: {str(exception)}. Skip!")


def download_all_images(args):
  """Downloads all images specified in the input file."""
  bucket = boto3.resource(
      's3', config=botocore.config.Config(
          signature_version=botocore.UNSIGNED)).Bucket(BUCKET_NAME)

  download_folder = args['download_folder'] or os.getcwd()

  if not os.path.exists(download_folder):
    os.makedirs(download_folder)

  logging.info(f"Processing {args['image_list']}")
  try:
    image_list = list()
    df = pd.read_csv(args['image_list'], header=None)
    for row in df.values:
        image_id, split, orig_url, thumbnail_url = row[0], row[1], row[2], row[10]
        image_list.append((image_id, split, orig_url, thumbnail_url))


  except ValueError as exception:
    sys.exit(exception)

  with futures.ThreadPoolExecutor(
      max_workers=args['num_processes']) as executor:
    all_futures = [
        executor.submit(download_one_image, bucket, image_id, split, orig_url, thumbnail_url,
                        download_folder) for (image_id, split, orig_url, thumbnail_url) in image_list
    ]
    for future in futures.as_completed(all_futures):
      future.result()


if __name__ == '__main__':
  parser = argparse.ArgumentParser(
      description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument("--verbose", "-v", action="store_true")
  parser.add_argument(
      'image_list',
      type=str,
      default=None,
      help=('Filename that contains the split + image IDs of the images to '
            'download. Check the document'))
  parser.add_argument(
      '--num_processes',
      type=int,
      default=5,
      help='Number of parallel processes to use (default is 5).')
  parser.add_argument(
      '--download_folder',
      type=str,
      default=None,
      help='Folder where to download the images.')
  args = parser.parse_args()
  log_format = "%(asctime)s %(levelname)s: [%(process)d]: %(message)s"
  logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, format=log_format)
  download_all_images(vars(args ))
