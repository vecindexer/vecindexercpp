#!/usr/bin/env python
# coding: utf-8

import argparse
import cv2
import logging
import numpy as np
import os
import os.path as fs
import time

from model_converter import EmbFromImage


def convert(image_path):
    # convert image to 32*32 = 3072 normalized bytes array!
    im = cv2.imread(image_path)
    v = cv2.resize(im, dsize=(32,32)).reshape(-1)
    return v / max(v)


def images_fetcher(process_dir):
    image_paths = []
    for root, dirs, files in os.walk(process_dir):
        for file in files:
            if file.lower().endswith('.jpg'):
                image_paths.append(fs.join(root, file))
    return image_paths


def process(opts):
    ts = time.time()
    out_name = fs.join(opts.out_dir, fs.basename(opts.process_dir)+'.npz')
    array = None
    array_ids = list()
    converted_images = list()
    if opts.model is not None:
        cnv = EmbFromImage(opts.model)
    else:
        cnv = convert

    for i, im in enumerate(images_fetcher(opts.process_dir)):
        try:
            v = cnv(im)

            logging.info(f"{fs.basename(opts.process_dir)}: {i}. {fs.basename(im)}: {v.shape}")

            if array is None:
                array = v
            else:
                array = np.vstack((array, v))

            converted_images.append(im)
            array_ids.append(fs.basename(im).replace('.jpg', ''))
        except Exception as err:
            logging.error(f'Skip! ERROR while converting image `{im}`: {str(err)}')

    array_ids = np.array(array_ids)

    if array is not None:
        if fs.exists(out_name):
            try:
                exists_file = np.load(out_name)
                array_exists = exists_file['arr_0']
                array = np.vstack((array_exists, array))
                array_ids_exists = exists_file['ids']
                array_ids = np.concatenate((array_ids_exists, array_ids), axis=0)
                logging.info(f"Stacked with {out_name}. Shape exists: {array_exists.shape}. Shape final: {array.shape}")
            except Exception as err:
                logging.warning(f"Failed to stack with exists array {out_name}: {str(err)}")
                i = 1
                while fs.exists(out_name.replace('.npz', f'_{i}.npz')):
                    i += 1
                out_name = out_name.replace('.npz', f'_{i}.npz')
        np.savez(out_name, arr_0 = array, ids = array_ids)
        te = time.time()
        logging.info(f"Processed {opts.process_dir}. Saved file: {out_name}. Shape: {array.shape}. Spend: {te-ts:.2f}s.")
    else:
        logging.debug(f"Processed {opts.process_dir}. To save file: {out_name}. Nothing to save!")

    if opts.delete_after:
        for im in converted_images:
            try:
                os.remove(im)
            except FileNotFoundError as ferr:
                pass


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--verbose", "-v", action="store_true")
    parser.add_argument("--delete_after", action="store_true")
    parser.add_argument("--process_dir", "-d")
    parser.add_argument("--out_dir", "-o")
    parser.add_argument("--model", "-m")


    args = parser.parse_args()

    log_format = "%(asctime)s %(levelname)s: [%(process)d]: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, format=log_format)

    process(args)

if __name__ == '__main__':
    main()