#!/usr/bin/env python
# coding: utf-8

import torch
from torchvision import datasets, transforms as T
import torchvision.models as models
from PIL import Image


class EmbFromImage(object):
    def __init__(self, model='resnet18'):

        if model is None:
            model = 'resnet18'

        if model == 'resnet18':
            self.model = models.resnet18(pretrained=True)
        elif model == 'resnet34':
            self.model = models.resnet34(pretrained=True)

        self.model.fc = torch.nn.Identity()
        self.model.eval()

    def __call__(self, image_path):
        normalize = T.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        transform = T.Compose([T.Resize(256), T.CenterCrop(224), T.ToTensor(), normalize])
        img = Image.open(image_path).convert('RGB')

        with torch.no_grad():
            input_tensor = transform(img).unsqueeze(0)
            return self.model(input_tensor)