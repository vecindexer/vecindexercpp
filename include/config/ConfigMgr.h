#pragma once

#include <string>

#include "config/ConfigNode.h"
#include "utils/Status.h"

namespace vecindexer {

class ConfigMgr {
 public:
    virtual Status
    LoadConfigFile(const std::string& filename) = 0;

    virtual void
    Print() const = 0;  // will be deleted

    virtual std::string
    DumpString() const = 0;

    virtual const ConfigNode&
    GetRootNode() const = 0;

    virtual ConfigNode&
    GetRootNode() = 0;
};

}  // namespace vecindexer
