#pragma once

#include <yaml-cpp/yaml.h>
#include <string>

#include "ConfigMgr.h"
#include "utils/Status.h"

namespace vecindexer {

class YamlConfigMgr : public ConfigMgr {
 public:
    static ConfigMgr*
    GetInstance() {
        static YamlConfigMgr mgr;
        return &mgr;
    }

    virtual Status
    LoadConfigFile(const std::string& filename);

    virtual void
    Print() const;

    virtual std::string
    DumpString() const;

    virtual const ConfigNode&
    GetRootNode() const;

    virtual ConfigNode&
    GetRootNode();

 private:
    bool
    SetConfigValue(const YAML::Node& node, const std::string& key, ConfigNode& config);

    bool
    SetChildConfig(const YAML::Node& node, const std::string& child_name, ConfigNode& config);

    bool
    SetSequence(const YAML::Node& node, const std::string& child_name, ConfigNode& config);

    void
    LoadConfigNode(const YAML::Node& node, ConfigNode& config);

 private:
    YAML::Node node_;
    ConfigNode config_;
};

}  // namespace vecindexer
