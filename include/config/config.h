
#pragma once

#include <fstream>
#include <iostream>
#include <mutex>
#include <regex>
#include <string>
#include <unordered_map>

#include <fiu-local.h>

#include "config/ConfigNode.h"
#include "utils/Status.h"


namespace vecindexer {
    
#define CONFIG_CHECK(func) \
    do {                   \
        Status s = func;   \
        if (!s.ok()) {     \
            return s;      \
        }                  \
    } while (false)

/* consul config */
static const char* CONFIG_CONSUL = "consul";
static const char* CONFIG_CONSUL_ADDRESS = "address";
static const char* CONFIG_CONSUL_ADDRESS_DEFAULT = "http://localhost";
static const char* CONFIG_CONSUL_PORT = "port";
static const char* CONFIG_CONSUL_PORT_DEFAULT = "8500";
static const char* CONFIG_CONSUL_WAIT = "wait";
static const char* CONFIG_CONSUL_WAIT_DEFAULT = "10s";
static const char* CONFIG_CONSUL_LEADERSHIP_SLEEP_MS = "leadership_sleep_ms";
static const char* CONFIG_CONSUL_LEADERSHIP_SLEEP_MS_DEFAULT = "5000";
static const char* CONFIG_CONSUL_SERVICE_CHECK = "service_check";
static const char* CONFIG_CONSUL_SERVICE_CHECK_DEFAULT = "";

/* server config */
static const char* CONFIG_SERVER = "server_config";
static const char* CONFIG_SERVER_SERVICE_NAME = "service_name";
static const char* CONFIG_SERVER_SERVICE_NAME_DEFAULT = "vecindexer";
static const char* CONFIG_SERVER_SHARD_NAME = "shard_name";
static const char* CONFIG_SERVER_SHARD_NAME_DEFAULT = "1";
static const char* CONFIG_SERVER_REPLICA_NAME = "replica_name";
static const char* CONFIG_SERVER_REPLICA_NAME_DEFAULT = "1";
static const char* CONFIG_SERVER_ADDRESS = "address";
static const char* CONFIG_SERVER_ADDRESS_DEFAULT = "127.0.0.1";
static const char* CONFIG_SERVER_PORT = "port";
static const char* CONFIG_SERVER_PORT_DEFAULT = "21000";
static const char* CONFIG_SERVER_WORKERS = "workers";
static const char* CONFIG_SERVER_WORKERS_DEFAULT = "6";
static const char* CONFIG_SERVER_CLUSTER_SERVICE_ADDRESS = "cluster_service_address";
static const char* CONFIG_SERVER_CLUSTER_SERVICE_ADDRESS_DEFAULT = "127.0.0.1";
static const char* CONFIG_SERVER_CLUSTER_SERVICE_PORT = "cluster_service_port";
static const char* CONFIG_SERVER_CLUSTER_SERVICE_PORT_DEFAULT = "21001";
static const char* CONFIG_SERVER_TIME_ZONE = "time_zone";
static const char* CONFIG_SERVER_TIME_ZONE_DEFAULT = "UTC+3";

/* indexer config */
static const char* CONFIG_INDEXER = "indexer_config";
static const char* CONFIG_INDEXER_OMP_NUM_THREADS = "omp_num_threads";
static const char* CONFIG_INDEXER_OMP_NUM_THREADS_DEFAULT = "0";

/* storage config */
static const char* CONFIG_STORAGE = "storage_config";
static const char* CONFIG_STORAGE_PRIMARY_PATH = "primary_path";
static const char* CONFIG_STORAGE_PRIMARY_PATH_DEFAULT = "/tmp/vecindexer";


static const char* CONFIG_TRACING = "tracing_config";
static const char* CONFIG_TRACING_JSON_CONFIG_PATH = "json_config_path";


class Config {
 public:
    static Config& GetInstance();
    Status LoadConfigFile(const std::string& filename);
    Status ValidateConfig(bool debug = false);
    
    friend std::ostream& operator<<(std::ostream& os, Config& cnf);

 private:
    ConfigNode& GetConfigRoot();
    ConfigNode& GetConfigNode(const std::string& name);
    bool ConfigNodeValid(const std::string& parent_key, const std::string& child_key);
    Status GetConfigValueInMem(const std::string& parent_key, const std::string& child_key, std::string& value);
    Status SetConfigValueInMem(const std::string& parent_key, const std::string& child_key, const std::string& value);

    ///////////////////////////////////////////////////////////////////////////
    /* consul config */
    Status CheckConsulAddress(const std::string& value);
    Status CheckLeadershipSleepMs(const int& value);
    Status CheckConsulWait(const std::string& value);
    Status CheckConsulServiceCheck(const std::string& value);
    
    /* server config */
    Status CheckServerServiceName(const std::string& value);
    Status CheckServerShardName(const std::string& value);
    Status CheckServerReplicaName(const std::string& value);
    Status CheckServerConfigAddress(const std::string& value);
    Status CheckServerConfigPort(const std::string& value);
    Status CheckServerWorkers(const int& value);
    Status CheckServerConfigTimeZone(const std::string& value);
    
    /* indexer config */
    Status CheckIndexerOmpNumThreads(const int& value);

    /* storage config */
    Status CheckStorageConfigPrimaryPath(const std::string& value);
    
    std::string
    GetConfigStr(const std::string& parent_key, const std::string& child_key, const std::string& default_value = "");
    std::string
    GetConfigSequenceStr(const std::string& parent_key, const std::string& child_key, const std::string& delim = ",",
                         const std::string& default_value = "");

 public:
    /* server config */
    inline Status GetServerServiceName(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_SERVICE_NAME, CONFIG_SERVER_SERVICE_NAME_DEFAULT);
        return CheckServerServiceName(value);
    }
    inline Status GetServerShardName(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_SHARD_NAME, CONFIG_SERVER_SHARD_NAME_DEFAULT);
        return CheckServerShardName(value);
    }
    inline Status GetServerReplicaName(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_REPLICA_NAME, CONFIG_SERVER_REPLICA_NAME_DEFAULT);
        return CheckServerReplicaName(value);
    }
    inline Status GetServerConfigAddress(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_ADDRESS, CONFIG_SERVER_ADDRESS_DEFAULT);
        return CheckServerConfigAddress(value);
    }
    inline Status GetServerConfigPort(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_PORT, CONFIG_SERVER_PORT_DEFAULT);
        return CheckServerConfigPort(value);
    }
    inline Status GetServerWorkers(int& value)
    {
        std::string s_value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_WORKERS, CONFIG_SERVER_WORKERS_DEFAULT);
        value = std::stoi(s_value);
        return CheckServerWorkers(value);
    }
    inline Status GetServerConfigClusterServiceAddress(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_CLUSTER_SERVICE_ADDRESS, CONFIG_SERVER_CLUSTER_SERVICE_ADDRESS_DEFAULT);
        return CheckServerConfigAddress(value);
    }
    inline Status GetServerConfigClusterServicePort(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_CLUSTER_SERVICE_PORT, CONFIG_SERVER_CLUSTER_SERVICE_PORT_DEFAULT);
        return CheckServerConfigPort(value);
    }
    Status GetServerConfigTimeZone(std::string& value)
    {
        value = GetConfigStr(CONFIG_SERVER, CONFIG_SERVER_TIME_ZONE, CONFIG_SERVER_TIME_ZONE_DEFAULT);
        return CheckServerConfigTimeZone(value);
    }
    
    /* consul */
    inline Status GetConsulAddress(std::string& value)
    {
        value = GetConfigStr(CONFIG_CONSUL, CONFIG_CONSUL_ADDRESS, CONFIG_CONSUL_ADDRESS_DEFAULT);
        return CheckConsulAddress(value);
    }
    inline Status GetConsulPort(std::string& value)
    {
        value = GetConfigStr(CONFIG_CONSUL, CONFIG_CONSUL_PORT, CONFIG_CONSUL_PORT_DEFAULT);
        return CheckServerConfigPort(value);
    }
    Status GetLeadershipSleepMs(int& value)
    {
        std::string s_value = GetConfigStr(CONFIG_CONSUL, CONFIG_CONSUL_LEADERSHIP_SLEEP_MS, CONFIG_CONSUL_LEADERSHIP_SLEEP_MS_DEFAULT);
        value = std::stoi(s_value);
        return CheckLeadershipSleepMs(value);
    }
    Status GetConsulWait(std::string& value)
    {
        value = GetConfigStr(CONFIG_CONSUL, CONFIG_CONSUL_WAIT, CONFIG_CONSUL_WAIT_DEFAULT);
        return CheckConsulWait(value);
    }
    Status GetConsulServiceCheck(std::string& value)
    {
        value = GetConfigStr(CONFIG_CONSUL, CONFIG_CONSUL_SERVICE_CHECK, CONFIG_CONSUL_SERVICE_CHECK_DEFAULT);
        return CheckConsulServiceCheck(value);
    }
    
    /* indexer config */
    Status GetIndexerOmpNumThreads(int& value)
    {
        std::string s_value = GetConfigStr(CONFIG_INDEXER, CONFIG_INDEXER_OMP_NUM_THREADS, CONFIG_INDEXER_OMP_NUM_THREADS_DEFAULT);
        value = std::stoi(s_value);
        return CheckIndexerOmpNumThreads(value);
    }

    /* storage config */
    Status GetStorageConfigPrimaryPath(std::string& value)
    {
        value = GetConfigStr(CONFIG_STORAGE, CONFIG_STORAGE_PRIMARY_PATH, CONFIG_STORAGE_PRIMARY_PATH_DEFAULT);
        return CheckStorageConfigPrimaryPath(value);
    }

    /* tracing config */
    Status GetTracingConfigJsonConfigPath(std::string& value)
    {
        value = GetConfigStr(CONFIG_TRACING, CONFIG_TRACING_JSON_CONFIG_PATH, "");
        fiu_do_on("get_config_json_config_path_fail", value = "error_config_json_path");
        if (!value.empty()) {
            std::ifstream tracer_config(value);
            Status s = tracer_config.good() ? Status::OK()
                                            : Status(SERVER_INVALID_ARGUMENT, "Failed to open tracer config file " + value +
                                                                                ": " + std::strerror(errno));
            tracer_config.close();
            return s;
        }
        return Status::OK();
    }

 private:
    std::unordered_map<std::string, std::unordered_map<std::string, std::string>> config_map_;
    std::mutex mutex_;
};


} // namespace vecindexer
