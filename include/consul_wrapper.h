#ifndef CONSUL_WRAPPER_H
#define CONSUL_WRAPPER_H

#include <stdexcept>

#include <chrono>
#include <thread>
#include <mutex>

#include <nlohmann/json.hpp>

#include <boost/assign/list_of.hpp>
#include <boost/unordered_map.hpp>
#include <iostream>

#include "utils/Status.h"

using json = nlohmann::json;

namespace vecindexer 
{    
    
using ThreadPtr = std::shared_ptr<std::thread>;
using MutexPtr = std::shared_ptr<std::mutex>;


class Leader
{
  private:
    std::string session_id_; 
    std::string name_;

  public:
    Leader(): session_id_(""), name_("") {}
    Leader(const std::string& session_id,
           const std::string& name
          ): 
           session_id_(session_id),
           name_(name)
    {}
    
    inline void Update(
        const std::string& session_id,
        const std::string& name
    ) 
    {
        session_id_ = session_id; 
        name_ = name; 
    }
    
    inline std::string GetName() { return name_ ; }
    
    inline bool Empty() { return session_id_.empty() && name_.empty(); }
    
    inline std::string ToString()
    {
        std::stringstream s ;
        s << *this ;
        return s.str();
    }
    
    inline bool operator == (const Leader& rhs) const 
    {
        if (this->session_id_.empty() || rhs.session_id_.empty())
            return false;

        return this->session_id_ == rhs.session_id_
            && this->name_ == rhs.name_ ;
    }
    
    inline friend std::ostream& operator<<(std::ostream& os, const Leader& l)
    {
        os << "[" << l.session_id_ << ", " << l.name_ << "]" << ' ';
        return os;
    }

    ~Leader() {}
};

enum class ServiceStatus: int 
{
    Undefined = 0,
    Leader = 1, 
    Follower = 2 , 
    BecameLeader = 3, 
    DowngradedToAFollower = 4, 
    MissedLeader = 5,
    MissedFollower = 6,
};
const boost::unordered_map<ServiceStatus,const char*> ServiceStatusToString = boost::assign::map_list_of 
    (ServiceStatus::Undefined, "Undefined")
    (ServiceStatus::Leader, "Leader")
    (ServiceStatus::Follower, "Follower")
    (ServiceStatus::BecameLeader, "Follower->Leader")
    (ServiceStatus::DowngradedToAFollower, "Leader->Follower")
    (ServiceStatus::MissedLeader, "MissedLeader")
    (ServiceStatus::MissedFollower, "MissedFollower")
    ;
    
    
struct ConsulSessionInfo
{
  public:
    std::string node;
    std::string name;
    std::string id;
    ConsulSessionInfo(): node(""), name(""), id("") {}
    ConsulSessionInfo(
        const std::string& node_,
        const std::string& name_,
        const std::string& id_
    ): node(node_), name(name_), id(id_) 
    {}
    
    void Update(
        const std::string& node_,
        const std::string& name_,
        const std::string& id_ )
    {
        node = node_;
        name = name_;
        id = id_;
    }
    
    void SetEmpty() { Update("", "", ""); }
        
    
    ~ConsulSessionInfo(){}
};


struct ConsulSessionOpts
{
    std::string endpoint;
    std::string service_name;
    std::string service_leadership_key;
    std::string service_tag;
    std::string service_id;
    std::string service_addr;
    std::string service_port;
    std::string shard_name;
    std::string replica_name;
    std::string id;
    Leader leader;
    Leader current_leader;
    int leadership_sleep_ms;
    std::string consul_wait;
    std::string cluster_service_addr;
    std::string cluster_service_port;
    std::string service_check;
    

    inline ConsulSessionOpts():
        endpoint(""),
        service_name(""),
        service_leadership_key(""),
        service_tag(""),
        service_id(""),
        service_addr(""),
        service_port(""),
        shard_name(""),
        replica_name(""),
        id(""),
        leader(Leader()),
        current_leader(Leader()),
        leadership_sleep_ms(3000),
        consul_wait(""),
        cluster_service_addr(""),
        cluster_service_port(""),
        service_check("")
    {}

    inline ConsulSessionOpts(const std::string& endpoint,
                             const std::string& service_name,
                             const std::string& service_leadership_key,
                             const std::string& service_tag,
                             const std::string& service_id,
                             const std::string& service_addr,
                             const std::string& service_port,
                             const std::string& shard_name,
                             const std::string& replica_name,
                             const std::string& id,
                             const Leader& leader,
                             const Leader& current_leader,
                             const int& leadership_sleep_ms,
                             const std::string& consul_wait,
                             const std::string& cluster_service_addr,
                             const std::string& cluster_service_port,
                             const std::string& service_check
                            )
    {
        this->endpoint = endpoint;
        this->service_name = service_name;
        this->service_leadership_key = service_leadership_key;
        this->service_tag = service_tag;
        this->service_id = service_id;
        this->service_addr = service_addr;
        this->service_port = service_port;
        this->shard_name = shard_name;
        this->replica_name = replica_name;
        this->id = id;
        this->leader = leader;
        this->current_leader = current_leader;
        this->leadership_sleep_ms = leadership_sleep_ms;
        this->consul_wait = consul_wait;
        this->cluster_service_addr = cluster_service_addr;
        this->cluster_service_port = cluster_service_port;
        this->service_check = service_check;
    }
};

struct ServiceHealth;

std::ostream& operator<<(std::ostream& os, const ServiceHealth& s_h);

std::ostream& operator<<(std::ostream& os, const std::vector<ServiceHealth>& s_h_l);

struct ServiceHealth
{
    std::string service_id;
    std::string service_name;
    std::string service_addr;
    int port;
    int cluster_service_port;
    std::string shard_name;
    std::vector<std::string> tags;
    std::string aggregated_status;
    
    ServiceHealth(): 
        service_id(""),
        service_name(""),
        service_addr(""),
        port(0),
        cluster_service_port(0),
        shard_name(""),
        tags({}),
        aggregated_status("")
    {}
    
    ServiceHealth(
        const std::string& service_id_,
        const std::string& service_name_,
        const std::string& service_addr_,
        int port_,
        int cluster_service_port_,
        const std::string& shard_name_,
        std::vector<std::string> tags_,
        const std::string& aggregated_status_
    ):
    service_id(service_id_),
    service_name(service_name_),
    service_addr(service_addr_),
    port(port_),
    cluster_service_port(cluster_service_port_),
    shard_name(shard_name_),
    tags(tags_),
    aggregated_status(aggregated_status_)
    {}
    
    inline std::string ToString()
    {
        std::stringstream s ;
        s << *this ;
        return s.str();
    }
};


struct ClusterHealth
{
    std::map<std::string, ServiceHealth> health_map_;
    
    ClusterHealth(){health_map_ = std::map<std::string, ServiceHealth>();}
    
    void update(const json& json_result);
    
    inline bool contains(const std::string service_id) const { return health_map_.contains(service_id); }
    
    std::vector<ServiceHealth> get_services_by_tags(const std::vector<std::string>& tags = {}) const;
    
    inline std::string ToString()
    {
        std::stringstream s ;
        s << *this ;
        return s.str();
    }
    
    inline friend std::ostream& operator<<(std::ostream& os, const ClusterHealth& c_h)
    {
        os << c_h.get_services_by_tags() ;
        return os;
    }
};

class ConsulSession
{
  private:    
    ConsulSessionOpts opts_;
    
    MutexPtr mutex_;
    bool programm_running_;
    
    ClusterHealth cluster_health_;

  protected:
    inline std::string EndpointBaseUrl ()
    {
        return opts_.endpoint + "/v1";
    }
    
    inline std::string CreateSessionUrl ()
    {
        return EndpointBaseUrl() + "/session/create";
    }
    
    inline std::string CreateBaseSessionLeaderUrl()
    {
        return EndpointBaseUrl() + "/kv/service/" + opts_.service_leadership_key + "/leader";
    }
    
    inline std::string CreateSessionLeaderBlockingUrl(const int& index, const std::string& wait)
    {
        return CreateBaseSessionLeaderUrl() + "?index="+std::to_string(index) + "&wait=" + wait;
    }
    
    inline std::string CreateAcquireSessionUrl ()
    {
        return CreateBaseSessionLeaderUrl() + "?acquire=" + opts_.id;
    }
    
    inline std::string ReleaseSessionUrl ()
    {
        return CreateBaseSessionLeaderUrl() + "?release=" + opts_.id;
    }
    
    inline std::string RetrieveSessionInfoUrl(const std::string& session_id)
    {
        return EndpointBaseUrl() + "/session/info/" + session_id ;
    }
    
    inline std::string RegisterServiceUrl()
    {
        return EndpointBaseUrl() + "/agent/service/register";
    }

    inline std::string DeregisterServiceUrl(const std::string& service_id)
    {
        return EndpointBaseUrl() + "/agent/service/deregister/" + service_id;
    }

    inline std::string HealthServiceUrl(const std::string& service_name)
    {
        return EndpointBaseUrl() + "/health/service/" + service_name;
    }

    inline void SetLeader(const std::string& session_id, 
                          const std::string& leader_name) 
    {
        opts_.leader.Update(session_id, leader_name);
    }
    
    inline Leader& GetLeader() { return opts_.leader; }

    inline Leader& GetCurrentLeader() { return opts_.current_leader; }
    
    inline void SetCurrentLeader(const std::string& session_id, 
                                 const std::string& leader_name) 
    {
        opts_.current_leader.Update(session_id, leader_name);
    }
    
    inline void SetEmptyCurrentLeader() { SetCurrentLeader("", ""); }
    
    inline void Lock() { mutex_->lock(); }
    
    inline void UnLock() { mutex_->unlock(); }
    
  public:
    inline ConsulSession(): 
        opts_(ConsulSessionOpts()), 
        programm_running_(true) 
    {
        mutex_ = std::make_shared<std::mutex>();
        cluster_health_ = ClusterHealth();
    }

    inline ConsulSession(const ConsulSessionOpts& opts): 
        opts_(opts),
        programm_running_(true)
    {
        mutex_ = std::make_shared<std::mutex>();
        cluster_health_ = ClusterHealth();
    }
    
    Status Start();
    void Stop();
    
    Status CreateSession(bool recreate=false);
    Status RetrieveLeader(int& index);
    Status RetrieveLeader(bool blocking, int& index, const std::string& wait);
    bool AcquireSession();
    bool ReleaseSession();
    Status RetrieveSessionInfo(const std::string& session_id, ConsulSessionInfo& session_info);
    
    Status RegisterService();
    Status DeregisterService();
    
    Status HealthService();
    
    std::vector<ServiceHealth> GetShardInstances() const;
    std::vector<ServiceHealth> GetShardFollowers() const;
    ServiceHealth GetShardLeader() const;
    std::vector<ServiceHealth> GetShardLeaders() const;
    std::vector<ServiceHealth> GetClusterInstances() const;
    std::vector<ServiceHealth> GetClusterFollowers() const;
    ServiceHealth GetClusterLeader() const;
    
    inline bool ServiceRegistered() { return cluster_health_.contains(opts_.service_id); }
    
    
    inline bool AmITheLeader() const { return opts_.leader == opts_.current_leader; }
    
    inline ConsulSessionOpts& GetOpts()  { return opts_; }
    
    inline bool IsProgrammRunning() { return programm_running_; }
    
    inline void ProgrammStopped() { programm_running_ = false; }
    
    inline std::string ToString()
    {
        std::stringstream s ;
        s << *this ;
        return s.str();
    }
    
    friend std::ostream& operator<<(std::ostream& os, ConsulSession& c_s);
  
    inline ~ConsulSession() { ReleaseSession(); }
    
    friend class LeaderShipTask;
};


struct ConsulServiceOpts: ConsulSessionOpts 
{
    std::string service_name_suffix;
    
    ConsulServiceOpts(): ConsulSessionOpts(), service_name_suffix("") {}
};


class ConsulService
{
  private:
    ConsulServiceOpts opts_;
    ServiceStatus status_;
    std::string service_name_suffix_;

  protected:
    ConsulSession session_;
    bool programm_running_;
    ThreadPtr leadership_thread_ ;

  public:
    ConsulService(): opts_(ConsulServiceOpts()), status_(ServiceStatus::Undefined), service_name_suffix_("") {}
    ConsulService(const std::string& service_name_suffix): 
        opts_(ConsulServiceOpts()), 
        status_(ServiceStatus::Undefined),
        service_name_suffix_(service_name_suffix)
    {}
    
    Status Start();
    Status Start(const ConsulServiceOpts& opts);
    
    void Stop();
    
    inline bool AmITheLeader() { return session_.AmITheLeader(); }
    
    void Setup(const ServiceStatus& service_status);
    
    void TryToEnsureSession();
    
    inline ConsulSession& GetSession() { return session_; }
    
    inline ServiceStatus GetServiceStatus() { return status_; }
    
    inline ConsulServiceOpts& GetOpts()  { return opts_; }
    
    inline bool IsProgrammRunning() { return programm_running_ && session_.IsProgrammRunning(); }
    
    inline void ProgrammStopped()
    {
        programm_running_ = false;
        this->session_.ProgrammStopped();
    }
    
    virtual bool inline PermissionToAcquireLeadership() { return true; }
    
    inline ~ConsulService() { programm_running_ = false; }
    
    friend class LeaderShipTask;
};


inline ConsulService GetConsulServiceInstance()
{
    ConsulService consul_service = ConsulService();
    return consul_service;
}

const std::string REPLICA_SUFFIX = "_replica";

class ConsulReplicaService: public ConsulService
{
    /* 
     * Service for syncronization between instances inside one shard.
     */
  public:  
    ConsulReplicaService(): ConsulService(REPLICA_SUFFIX) {}
    
    inline static ConsulReplicaService& GetInstance()
    {
        static ConsulReplicaService consul_service = ConsulReplicaService();
        return consul_service;
    }
    
    bool inline PermissionToAcquireLeadership() override
    {
        return true;
    }
    
    Status Start() ; 
    void Stop() ;
    
    friend class ConsulShardService;
    
    ~ConsulReplicaService() {}
};


const std::string SHARD_SUFFIX = "_shard";

class ConsulShardService: public ConsulService
{   
    /* 
     * Service for syncronization between shard leaders.
     */
  public:  
    inline ConsulShardService(): ConsulService(SHARD_SUFFIX) {}
    
    inline static ConsulShardService& GetInstance()
    {
        static ConsulShardService consul_service = ConsulShardService();
        return consul_service;
    }
    
    bool inline PermissionToAcquireLeadership() override
    {
        ConsulReplicaService& consul_replica_service = ConsulReplicaService::GetInstance();
        bool res = consul_replica_service.session_.AmITheLeader();
        return res;
    }
    
    Status Start() ;
    void Stop() ;
        
    
    inline ~ConsulShardService() {}
};


class LeaderShipTask
{
  private:
    ConsulService* consul_service_=nullptr;
    int sleep_ms_;
    std::string consul_wait_;

  public:
    
    inline LeaderShipTask(ConsulService* consul_service): 
            consul_service_(consul_service),
            sleep_ms_(consul_service_->GetOpts().leadership_sleep_ms),
            consul_wait_(consul_service_->GetOpts().consul_wait) 
    {}
    
    void operator () ();
    
    ~LeaderShipTask(){}
    
    
  friend class ConsulService;
    
};


inline bool isSlashSymb(char c){return c == '/';}

std::string decode64(const std::string &val);
std::string encode64(const std::string &val);

inline void sleep_for(int sleep_ms_)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms_));
}

Status get_consul_service_opts_from_config(ConsulServiceOpts& opts, 
                                           Config& config);

} // namespace vecindexer

#endif // CONSUL_WRAPPER_H
