#ifndef INDEXER_H
#define INDEXER_H

#include <faiss/Index.h>
#include "nlohmann/json.hpp"

#include <boost/algorithm/string.hpp>
#include <string>
#include <shared_mutex>
#include <omp.h>
#include <unistd.h>

#include "config/config.h"
#include "utils/Status.h"
#include "utils/Log.h"
#include "utils/Types.h"


namespace vecindexer {
namespace server {

using json = nlohmann::json;
    
using ShMutexPtr = std::shared_ptr<std::shared_mutex>; 


inline std::string make_index_filename(const std::string& coll_dir, const int coll_id)
{
    return coll_dir+"/"+std::to_string(coll_id)+".faiss";
}

inline bool isSlashSymb(char c){return c == '/';}


void ensure_directory_created(const std::string& dirname);


inline int getNumCores() // vcpus !
{
    return sysconf(_SC_NPROCESSORS_ONLN);
}


struct Collection
{
    int coll_id;
    std::string coll_dir;
    std::string index_file;
    int dimension=0;
    int64_t count=0;
    std::string index_str="";
    std::shared_ptr<faiss::Index> index_ptr=nullptr;
    ShMutexPtr mutex_;
    bool ok = false;
    bool on_gpu = false;
    
    
    Collection(){}
    Collection(int coll_id_, const std::string& coll_dir_, int dim, const std::string& index_): 
               coll_id(coll_id_), coll_dir(coll_dir_), 
               index_file(make_index_filename(coll_dir_, coll_id_)), 
               dimension(dim), index_str(index_)
    {
        mutex_ = std::make_shared<std::shared_mutex>(); 
        ensure_directory_created(coll_dir); 
        Load();
        
    }
    Collection(int coll_id_, const std::string& coll_dir_): 
               coll_id(coll_id_), coll_dir(coll_dir_), 
               index_file(make_index_filename(coll_dir_, coll_id_))
    {
        mutex_ = std::make_shared<std::shared_mutex>(); 
        ensure_directory_created(coll_dir); 
        Load();
    }
    
    Status Create();
    Status Train(const VectorsData& vectors);
    Status Add(const VectorsData& vectors);
    Status Search(const VectorsData& vectors, int k, ResultIds& I, ResultDistances& D);
    
    void Load();
    void Save();
    uintmax_t GetOnDiskSize();
    
    Status MoveToGpu(int device,
                     bool useFloat16CoarseQuantizer, 
                     bool useFloat16, 
                     bool usePrecomputed, 
                     bool storeTransposed);
    Status MoveToCpu();
    
    json GetInfo() const;
    
    inline std::string ToString()
    {
        std::stringstream s ;
        s << *this ;
        return s.str();
    }
    
    friend std::ostream& operator<<(std::ostream& os, const Collection& c)
    {
        os << c.index_str << ';' << c.dimension;
        return os;
    }
};


struct IndexerOptions 
{
    std::string storage_path;
    int omp_num_threads=-1;
    
    inline IndexerOptions(): storage_path(""), omp_num_threads(-1) {}
    
    inline IndexerOptions(const std::string& storage_path_, 
                          int omp_num_threads_
                          ):
                          storage_path(storage_path_),
                          omp_num_threads(omp_num_threads_)
                          {}
};


class Indexer
{
  private:
    IndexerOptions opts_;
    
    std::map<int, Collection> coll_map_;
    
  protected:
    bool IsCollectionOk(int coll_id);
    void EnsureCollectionLoaded(int coll_id);
    void LoadCollectionsFromStorage(std::vector<int> collections = {});
    
  public:  
    int omp_threads;
    
    Indexer(): omp_threads(0) 
    { 
        opts_ = IndexerOptions();
        coll_map_ = std::map<int, Collection>(); 
    }
    Indexer(const IndexerOptions& opts): opts_(opts)
    {
        if (opts_.omp_num_threads <= 0)
            omp_threads = getNumCores();
        else
            omp_threads = opts_.omp_num_threads;
        
        SERVER_LOG_DEBUG<<"Setup omp_set_num_threads: " << omp_threads <<std::endl;
        omp_set_num_threads(omp_threads);
        coll_map_ = std::map<int, Collection>();
    }
    
    inline static Indexer& GetInstance()
    {
        Config& config = Config::GetInstance();
        std::string storage_path;
        Status s;

        s = config.GetStorageConfigPrimaryPath(storage_path);
        if (!s.ok()) {throw std::runtime_error("Failed to storage_path!");}
        
        int omp_num_threads;
        s = config.GetIndexerOmpNumThreads(omp_num_threads);
        if (!s.ok()) {throw std::runtime_error("Failed to omp_num_threads!");}
        
        static Indexer indexer = Indexer(IndexerOptions(storage_path, omp_num_threads));
        return indexer;
    }
    
    Status Start();
    void Stop();
    
    Status CreateCollection(int coll_id, const std::string index_description, int dimension);
    Status Train(int coll_id, const VectorsData& vectors);
    Status Add(int coll_id, const VectorsData& vectors);
    Status Search(int coll_id, const VectorsData& vectors, int k, ResultIds& I, ResultDistances& D);
    
    Status CollectionInfo(json& collections_info, std::vector<int> collections = {});
    Status MoveToGpu(int coll_id,
                     bool useFloat16CoarseQuantizer, 
                     bool useFloat16, 
                     bool usePrecomputed, 
                     bool storeTransposed);
    Status MoveToCpu(int coll_id);
        
    
    ~Indexer() {}
};

Status get_indexer_opts_from_config(IndexerOptions& opts, 
                                    Config& config);


} // namespace server
} // namespace vecindexer

#endif // INDEXER_H
