#ifndef SERVER_H
#define SERVER_H

#include <ctime>

#include <zmq.hpp>

#include <opentracing/tracer.h>

#include "nlohmann/json.hpp"

#include "config/config.h"
#include "0mq/cbor_utils.hpp"
#include "utils/Context.h"
#include "utils/Log.h"
#include "utils/Types.h"

#include "consul_wrapper.h"
#include "indexer.h"
#include "vec_indexer_app.h"


namespace vecindexer {
namespace server {
    
using json = nlohmann::json;

namespace zeromq {

using Range = std::pair<std::string, std::string>;

struct TopKQueryResult 
{
    int64_t row_num_;
    ResultIds id_list_;
    ResultDistances distance_list_;

    TopKQueryResult(): row_num_(0) {}

    TopKQueryResult(int64_t row_num, const ResultIds& id_list, const ResultDistances& distance_list) 
    {
        row_num_ = row_num;
        id_list_ = id_list;
        distance_list_ = distance_list;
    }
};


class ServerOpts
{
  public:
    std::string endpoint;
    std::string cluster_service_endpoint;
    int         threads = 6;
};


class ServiceException: public std::runtime_error
{  
  public:
    std::string req_no;
  public:
    ServiceException (const char* msg,
                      std::string req_no_): 
    req_no (req_no_), std::runtime_error(msg)
    {}
};


class ZeroMQServer
{

public:

    static ZeroMQServer& GetInstance() 
    {
        Config& config = Config::GetInstance();
        std::string address, port;
        std::string cl_s_address, cl_s_port;
        int workers;
        Status s;

        s = config.GetServerConfigAddress(address);
        if (!s.ok()) {throw std::runtime_error("Failed to get addr!");}
        s = config.GetServerConfigPort(port);
        if (!s.ok()) {throw std::runtime_error("Failed to get port!");}
        s = config.GetServerWorkers(workers);
        if (!s.ok()) {throw std::runtime_error("Failed to get workers count!");}
        
        s = config.GetServerConfigClusterServiceAddress(cl_s_address);
        if (!s.ok()) {throw std::runtime_error("Failed to get cl_s_address!");}
        s = config.GetServerConfigClusterServicePort(cl_s_port);
        if (!s.ok()) {throw std::runtime_error("Failed to get cl_s_port!");}

        std::string server_address(address + ":" + port);
        std::string cluster_service_address(cl_s_address + ":" + cl_s_port);
        static ZeroMQServer server(ServerOpts{"tcp://" + server_address, "tcp://"+cluster_service_address, workers});
        return server;
    }

    ZeroMQServer(const ServerOpts& opts):opts_(opts),
                                         zmq_context_(1),
                                         tracer_(opentracing::Tracer::Global()),
                                         mutex_(std::make_shared<std::shared_mutex>())
    {
        ConsulShardService& consul_shard_service = ConsulShardService::GetInstance();
        shard_leaders = consul_shard_service.GetSession().GetShardLeaders();
    }

    void Start();
    void Stop();

protected:

    void main_loop();
    std::string recv_str(zmq::socket_t& socket);

    void try_process(zmq::socket_t& socket);
    void process();
    
    void try_cluster_service_process(zmq::socket_t& socket);
    void cluster_service_process();

    std::vector<uint8_t> handle_req(const zmq::message_t& msg);
    std::vector<uint8_t> handle_service_req(const zmq::message_t& msg);
    
    void replicated_message(const std::shared_ptr<Context>& pctx,
                            const zmq::message_t& msg,
                            const std::vector<ServiceHealth>& followers,
                            std::vector<std::string>& replicas_success, 
                            std::vector<std::string>& replicas_failed,
                            std::vector<std::string>& replicas_failed_errors);

    void handle_create_collection(const std::shared_ptr<Context>& pctx, Unpacker& unpacker, json& res);
    void handle_train(const std::shared_ptr<Context>& pctx, Unpacker& unpacker, json& res);
    void handle_add(const std::shared_ptr<Context>& pctx, Unpacker& unpacker, json& res);
    
//     std::vector<uint8_t> handle_create_index(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
//     std::vector<uint8_t> handle_drop_table(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
    std::vector<uint8_t> handle_search(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
//     std::vector<uint8_t> handle_search_by_id(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
    std::vector<uint8_t> search_impl(const std::shared_ptr<Context>& pctx,
                                     VectorsData&& vectors,
                                     Unpacker& unpacker
                                    );
//     std::vector<uint8_t> handle_compare_fragments_by_id(const std::shared_ptr<Context>& pctx,
//                                                         Unpacker& unpacker);
//     std::vector<uint8_t> handle_compare_fragments(const std::shared_ptr<Context>& pctx,
//                                                   Unpacker& unpacker);
//     std::vector<uint8_t> compare_fragments_impl(const std::shared_ptr<Context>& pctx,
//                                                 engine::CompareFragmentsReq&& req,
//                                                 Unpacker& unpacker);
//     std::vector<uint8_t> handle_get_vectors(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
//     std::vector<uint8_t> handle_clusterize(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
    
    
    std::vector<uint8_t> handle_info(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);
    std::vector<uint8_t> handle_manage_coll(const std::shared_ptr<Context>& pctx, Unpacker& unpacker);

//     Status create_table(const std::shared_ptr<Context>& pctx,
//                         const std::string& table_name);
//     Status create_index(const std::shared_ptr<Context>& pctx,
//                         const std::string& table_name);

    ServerOpts                           opts_;
    zmq::context_t                       zmq_context_;

//     RequestHandler                       request_handler_;
    std::shared_ptr<opentracing::Tracer> tracer_;
    
    std::vector<ServiceHealth> shard_leaders;
    ShMutexPtr mutex_;
    std::time_t last_check_time = std::time(nullptr) - 10;

};
    

} // namespace zeromq
} // namespace server
} // namespace vecindexer


#endif // SERVER_H
