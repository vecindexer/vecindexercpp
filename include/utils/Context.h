#pragma once

#include <opentracing/tracer.h>

#include <memory>
#include <string>
#include <unordered_map>


namespace vecindexer {
namespace server {
    
class TraceContext {
 public:
    explicit TraceContext(std::unique_ptr<opentracing::Span>& span);

    std::unique_ptr<TraceContext>
    Child(const std::string& operation_name) const;

    std::unique_ptr<TraceContext>
    Follower(const std::string& operation_name) const;

    const std::unique_ptr<opentracing::Span>&
    GetSpan() const;

 private:
    //    std::unique_ptr<opentracing::SpanContext> span_context_;
    std::unique_ptr<opentracing::Span> span_;
};


class Context {
 public:
    explicit Context(const std::string& request_id);
    
    inline std::string GetRequestId() const { return request_id_; }

    std::shared_ptr<Context>
    Child(const std::string& operation_name) const;

    std::shared_ptr<Context>
    Follower(const std::string& operation_name) const;

    void
    SetTraceContext(const std::shared_ptr<TraceContext>& trace_context);

    const std::shared_ptr<TraceContext>&
    GetTraceContext() const;

 private:
    std::string request_id_;
    std::shared_ptr<TraceContext> trace_context_;
};

}  // namespace server
}  // namespace vecindexer
