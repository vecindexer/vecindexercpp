#ifndef UTILS_ERROR_H
#define UTILS_ERROR_H

#include <cstdint>
#include <exception>
#include <string>

namespace vecindexer {

using ErrorCode = int32_t;

constexpr ErrorCode CONSUL_SERVICE_SUCCESS = 0;
constexpr ErrorCode CONSUL_SERVICE_ERROR_CODE_BASE = 100000;
constexpr ErrorCode
ToConsulServiceErrorCode(const ErrorCode error_code) {
    return CONSUL_SERVICE_ERROR_CODE_BASE + error_code;
}

constexpr ErrorCode SERVER_SUCCESS = 0;
constexpr ErrorCode SERVER_ERROR_CODE_BASE = 30000;

constexpr ErrorCode
ToServerErrorCode(const ErrorCode error_code) {
    return SERVER_ERROR_CODE_BASE + error_code;
}

constexpr ErrorCode DB_SUCCESS = 0;
constexpr ErrorCode DB_ERROR_CODE_BASE = 40000;

constexpr ErrorCode
ToDbErrorCode(const ErrorCode error_code) {
    return DB_ERROR_CODE_BASE + error_code;
}

constexpr ErrorCode KNOWHERE_SUCCESS = 0;
constexpr ErrorCode KNOWHERE_ERROR_CODE_BASE = 50000;

constexpr ErrorCode
ToKnowhereErrorCode(const ErrorCode error_code) {
    return KNOWHERE_ERROR_CODE_BASE + error_code;
}

// server error code
constexpr ErrorCode SERVER_UNEXPECTED_ERROR = ToServerErrorCode(1);
constexpr ErrorCode SERVER_INVALID_ARGUMENT = ToServerErrorCode(4);
constexpr ErrorCode SERVER_FILE_NOT_FOUND = ToServerErrorCode(5);

constexpr ErrorCode CONSUL_SERVICE_UNEXPECTED_ERROR = ToConsulServiceErrorCode(1);
constexpr ErrorCode CONSUL_SERVICE_LEADER_KEY_DOES_NOT_EXIST = ToConsulServiceErrorCode(10);
constexpr ErrorCode CONSUL_SERVICE_EMPTY_LEADER = ToConsulServiceErrorCode(11);
constexpr ErrorCode CONSUL_SERVICE_NO_CLUSTER_LEADER = ToConsulServiceErrorCode(12);

namespace server {
class ServerException : public std::exception {
 public:
    explicit ServerException(ErrorCode error_code, const std::string& message = std::string())
        : error_code_(error_code), message_(message) {
    }

 public:
    ErrorCode
    error_code() const {
        return error_code_;
    }

    virtual const char*
    what() const noexcept {
        return message_.c_str();
    }

 private:
    ErrorCode error_code_;
    std::string message_;
};

}  // namespace server
}  // namespace vecindexer

#endif // UTILS_ERROR_H
