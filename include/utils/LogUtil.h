#ifndef UTILS_LOG_UTIL_H
#define UTILS_LOG_UTIL_H

#include "easyloggingpp/easylogging++.h"
#include "utils/Status.h"

#include <sstream>
#include <string>

namespace vecindexer {

Status InitLog(const std::string& log_config_file);

void RolloutHandler(const char* filename, std::size_t size, el::Level level);

#define SHOW_LOCATION
#ifdef SHOW_LOCATION
#define LOCATION_INFO "[" << sql::server::GetFileName(__FILE__) << ":" << __LINE__ << "] "
#else
#define LOCATION_INFO ""
#endif

}  // namespace vecindexer

#endif // UTILS_LOG_UTIL_H
