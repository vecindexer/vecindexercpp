#ifndef UTILS_SIGNAL_UTILS_H
#define UTILS_SIGNAL_UTILS_H

namespace vecindexer {

class SignalUtil {
 public:
    static void
    HandleSignal(int signum);
    static void
    PrintStacktrace();
};

}  // namespace vecindexer

#endif // UTILS_SIGNAL_UTILS_H
