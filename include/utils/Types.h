#pragma once


#include <faiss/Index.h>

#include <stdint.h>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace vecindexer 
{

typedef int64_t IDNumber;
typedef IDNumber* IDNumberPtr;
typedef std::vector<IDNumber> IDNumbers;

typedef std::vector<int64_t> ResultIds;
typedef std::vector<float> ResultDistances;

struct VectorsData {
    uint64_t vector_count_ = 0;
    std::vector<float> float_data_;
    std::vector<uint8_t> binary_data_;
    IDNumbers id_array_;
    //Table_ids are used by search_id
    //If it is empty the default table_id is used(passed in the query)
    std::vector<std::string> query_table_ids;
    bool return_vectors;
    
    int get_dimension() const
    {
        return (int) (float_data_.size() / vector_count_);
    }
};

using File2ErrArray = std::map<std::string, std::vector<std::string>>;
using Table2FileErr = std::map<std::string, File2ErrArray>;
using File2RefCount = std::map<std::string, int64_t>;
using Table2FileRef = std::map<std::string, File2RefCount>;


}  // namespace vecindexer
