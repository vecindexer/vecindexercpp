#pragma once

#include "utils/Status.h"

#include <string>
#include <vector>

namespace vecindexer {

class ValidationUtil {
 private:
    ValidationUtil() = default;

 public:
    static Status
    ValidateUrl(const std::string& url);
    
    static Status
    ValidateIpAddress(const std::string& ip_address);

    static Status
    ValidateStringIsNumber(const std::string& str);

    static Status
    ValidateStringIsBool(const std::string& str);

    static Status
    ValidateStringIsFloat(const std::string& str);

    static Status
    ValidateDbURI(const std::string& uri);
};

}  // namespace vecindexer

