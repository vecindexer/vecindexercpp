#ifndef VEC_INDEXER_APP_H
#define VEC_INDEXER_APP_H

#include "indexer.h"
#include "server.h"
#include "consul_wrapper.h"
#include "config/config.h"
#include "utils/Status.h"


namespace vecindexer {

    
struct VecIndexerAppOptions 
{
    std::string config_file;
    std::string log_config_file;
    
    VecIndexerAppOptions(
        const std::string& config_file_ = "etc/config/server.yml",
        const std::string& log_config_file_ = "etc/config/log_config.conf"
    ): 
        config_file(config_file_),
        log_config_file(log_config_file_)
    {};
};


class VecIndexerApp 
{
  public:
    static inline VecIndexerApp& GetInstance() 
    {
        static VecIndexerApp app;
        return app;
    }
        
    void Init(const VecIndexerAppOptions& opts);
    Status Start();
    void Stop();

  private:
    VecIndexerApp () = default;
    ~VecIndexerApp () = default;

    Status LoadConfig();

    Status StartService();
    void StopService();

  private:  
    VecIndexerAppOptions opts_;
    
}; // VecIndexerApp


Status SetupTimezone();

} // namespace vecindexer

#endif // VEC_INDEXER_APP_H
