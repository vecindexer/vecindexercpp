{
  src,
  stdenv,
  cmake,
  pkg-config,
  faiss,
  boost,
  zeromq,
  cppzmq,
  nlohmann_json,
  libcbor,
  sqlite,
  yaml-cpp,
  opentracing-cpp,
  cudaPackages,
}:
stdenv.mkDerivation {
  pname = "vec_indexer";
  version = "0.2.0";
  inherit src;

  hardeningDisable = [ "all" ];

  nativeBuildInputs = [cmake pkg-config stdenv.cc];
  buildInputs=[faiss 
               boost 
               zeromq 
               cppzmq 
               nlohmann_json 
               libcbor
               sqlite
               yaml-cpp
               opentracing-cpp
               cudaPackages.cudatoolkit
               ];

  enableParallelBuilding=true;
}
