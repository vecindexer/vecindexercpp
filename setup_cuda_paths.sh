#!/bin/bash


sudo mkdir -p /usr/lib/x86_64-linux-gnu/nvidia/current/
cd /usr/lib/x86_64-linux-gnu/nvidia/current/
sudo rm ./*
sudo find /usr/lib/ -name "libnvidia*" -exec ln -s {} ./ \;
sudo find /usr/lib/ -name "libcuda*" -exec ln -s {} ./ \;
cd -

export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/nvidia/current/:$LD_LIBRARY_PATH
