
set(INCLUDE_DIRECTORIES
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_SOURCE_DIR}/thirdparty
    ${CMAKE_SOURCE_DIR}/thirdparty/libfiu/libfiu
    ${CMAKE_SOURCE_DIR}/thirdparty/HTTPRequest/include
    ${SQLite3_INCLUDE_DIRS}
    )
    
set(COMMON_LIBRARIES
    faiss
    Boost::boost
    Boost::filesystem
    Boost::program_options
    nlohmann_json::nlohmann_json
    OpenTracing::opentracing
    yaml-cpp
    ${SQLite3_LIBRARIES}
    CUDA::cublas
)


# server_config
aux_source_directory(${CMAKE_SOURCE_DIR}/thirdparty/easyloggingpp easyloggingpp_files)

add_library(server_config
    config/config.cpp
    config/ConfigNode.cpp
    config/YamlConfigMgr.cpp
    utils/Status.cpp
    utils/StringHelpFunctions.cpp
    utils/ValidationUtil.cpp
    ${easyloggingpp_files}
    )
    
target_include_directories(server_config PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )
    
target_link_libraries(server_config PUBLIC
    ${COMMON_LIBRARIES}
    )
    
    
# multi_level_db 
add_library(multi_level_db
    DocLevelOrganizer.hpp
)

target_include_directories(multi_level_db PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )
    
target_link_libraries(multi_level_db PUBLIC
    ${COMMON_LIBRARIES}
    server_config
    )

    
# indexer
add_library(indexer
    indexer.cpp
    )

target_include_directories(indexer PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )

target_link_libraries(indexer PUBLIC
    server_config
    )


# server
add_library(server 
    server.cpp
    0mq/cbor_utils.hpp
    utils/Context.cpp
    )
    
target_include_directories(server PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )

target_link_libraries(server PUBLIC
    server_config
    ${LIBCBOR_libraries}
    cppzmq
    cbor
    multi_level_db
    )    

    
# consul_wrapper    
add_library(consul_wrapper 
    consul_wrapper.cpp
    )
    
target_include_directories(consul_wrapper PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )

target_link_libraries(consul_wrapper PUBLIC
    server_config
    )

    
# vec_indexer_app
add_library(vec_indexer_app 
    vec_indexer_app.cpp
    utils/LogUtil.cpp
    )
    
target_include_directories(vec_indexer_app PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )

target_link_libraries(vec_indexer_app PUBLIC
    consul_wrapper
    indexer
    server
)
    
        
# vec_indexer
add_executable(vec_indexer
    main_app.cpp
    utils/SignalUtil.cpp
    )

target_link_libraries(vec_indexer PUBLIC
    vec_indexer_app
)


install(TARGETS vec_indexer
        RUNTIME DESTINATION bin)

        
#CLI
