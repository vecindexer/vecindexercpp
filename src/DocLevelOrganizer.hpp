#include <iostream>
#include <string>
#include <vector>
#include "stdlib.h"
#include <sqlite3.h>
#include <chrono>
#include "ini.h"
#include <thread>

struct BaseConfig {
public:
    static BaseConfig& getInstance() {

        static BaseConfig с_instance;
        return с_instance;
    };
    
    bool print_debug = false;
    bool print_time = false;
    int page_size = 16384;
    int cache_size = -8000000;
    std::string name = "";
    __int64_t debug_data_size = 100000000;
    int debug_batch_size = 100000;
    bool reindex = false;
    bool vac = false;
    bool d_track_time = false;
    int d_r_child = 100000;
    int d_r_parent = 100000;
    int d_r_relative = 100000;
    std::vector<int> deb_times;

    void read_config(const char* filename, int argc, char* argv[]) 
    {
        mINI::INIFile file(filename);
        mINI::INIStructure ini;
        file.read(ini);
        print_debug = std::stoi(ini["debug"]["print_debug"]);
        print_time = std::stoi(ini["debug"]["print_time"]);
        cache_size = std::stoi(ini["main"]["cache_size"]);
        page_size = std::stoi(ini["main"]["page_size"]);
        reindex = std::stoi(ini["main"]["reindex_on_start"]);
        vac = std::stoi(ini["main"]["vacuum_on_start"]);
        d_track_time = std::stoi(ini["main"]["track_time"]);
        name = ini["main"]["dbfile_name"];
        d_r_child = std::stoi(ini["debug"]["debug_random_children"]);
        d_r_parent = std::stoi(ini["debug"]["debug_random_parents"]);
        d_r_relative = std::stoi(ini["debug"]["debug_random_relatives"]);
        for (int i = 1; i < argc; i++) 
        {
            std::cout << argv[i] << std::endl;
            if (strcmp(argv[i], "-debug_print") == 0) { print_debug = true; }
            if (strcmp(argv[i], "-debug_time") == 0) { print_time = true; }
            if (strcmp(argv[i], "-cache_size") == 0) {
                cache_size = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-page_size") == 0) {
                page_size = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-base_name") == 0) {
                name = argv[i + 1];
            }
            if (strcmp(argv[i], "-d_r_children") == 0) {
                d_r_child = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-d_r_parents") == 0) {
                d_r_parent = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-d_r_relatives") == 0) {
                d_r_relative = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-reindex_on_start") == 0) { reindex = true; }
            if (strcmp(argv[i], "-vacuum_on_start") == 0) { vac = true; }
            if (strcmp(argv[i], "-track_debug_time") == 0) { d_track_time = true; }
        }
    }
        
    void read_config_ini(const char* filename) 
    {
        mINI::INIFile file(filename);
        mINI::INIStructure ini;
        file.read(ini);
        print_debug = std::stoi(ini["debug"]["print_debug"]);
        print_time = std::stoi(ini["debug"]["print_time"]);
        cache_size = std::stoi(ini["main"]["cache_size"]);
        page_size = std::stoi(ini["main"]["page_size"]);
        reindex = std::stoi(ini["main"]["reindex_on_start"]);
        vac = std::stoi(ini["main"]["vacuum_on_start"]);
        d_track_time = std::stoi(ini["debug"]["track_time"]);

        name = ini["main"]["dbfile_name"];
        d_r_child = std::stoi(ini["debug"]["debug_random_children"]);
        d_r_parent = std::stoi(ini["debug"]["debug_random_parents"]);
        d_r_relative = std::stoi(ini["debug"]["debug_random_relatives"]);
        /*for (int i = 1; i < argc; i++) {
            std::cout << argv[i] << std::endl;
            if (strcmp(argv[i], "-debug_print") == 0) { print_debug = true; }
            if (strcmp(argv[i], "-debug_time") == 0) { print_time = true; }
            if (strcmp(argv[i], "-cache_size") == 0) {
                cache_size = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-page_size") == 0) {
                page_size = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-base_name") == 0) {
                name = argv[i + 1];
            }
            if (strcmp(argv[i], "-d_r_children") == 0) {
                d_r_child = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-d_r_parents") == 0) {
                d_r_parent = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-d_r_relatives") == 0) {
                d_r_relative = std::stoi(argv[i + 1]);
            }
            if (strcmp(argv[i], "-reindex_on_start") == 0) { reindex = true; }
            if (strcmp(argv[i], "-vacuum_on_start") == 0) { vac = true; }
            if (strcmp(argv[i], "-track_debug_time") == 0) { d_track_time = true; }
        }*/
    }
};

class DocLevelOrganizer {
private:
    //static DocLevelOrganizer* instance = NULL;
public:
    sqlite3* db = NULL;
    char* err = NULL;
    BaseConfig config;

    DocLevelOrganizer(){
    };

    DocLevelOrganizer(BaseConfig config_obj) {
        config = config_obj;
    };

    static DocLevelOrganizer& getInstance() {

        static DocLevelOrganizer instance;
        return instance;
    }

    std::string get_name() {
        return config.name;
    }

    static int print_callback(void* list, int count, char** data, char** col) {
        for (int i = 0; i < count; i++) {
            std::cout << col[i] << " : " << data[i] << std::endl;
        }
        return 0;
    }


    std::vector<std::vector<__int64_t>> get_row(__int64_t id) {

        std::string query = "SELECT * FROM document_parts WHERE doc_id = ?";
        if (config.print_debug) std::cout << "QUERY - GET ROW:" << query << std::endl;
        sqlite3_stmt* stmt;
        sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, id);
        std::vector< std::vector<__int64_t>> result;
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int i;
            int num_cols = sqlite3_column_count(stmt);
            std::vector<__int64_t> ivec;
            for (i = 0; i < num_cols; i++)
            {
                ivec.push_back(sqlite3_column_int64(stmt, i));
            }
            result.push_back(ivec);
        }

        sqlite3_finalize(stmt);

        if (config.print_debug) {
            std::cout << "Row =" + std::to_string(id) + ": ";
            for (std::vector<__int64_t> i : result) {
                for (__int64_t i2 : i)
                    std::cout << i2 << ' ';
                std::cout << std::endl;
            }

        }
        if (result.size() > 0) return result;

        query = "SELECT * FROM document_parts WHERE frg_l1_id = ?";
        if (config.print_debug) std::cout << "QUERY - GET ROW:" << query << std::endl;
        sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, id);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int i;
            int num_cols = sqlite3_column_count(stmt);
            std::vector<__int64_t> ivec;
            for (i = 0; i < num_cols; i++)
            {
                ivec.push_back(sqlite3_column_int64(stmt, i));
            }

            result.push_back(ivec);

        }

        sqlite3_finalize(stmt);

        if (config.print_debug) {
            std::cout << "Row =" + std::to_string(id) + ": ";
            for (std::vector<__int64_t> i : result) {
                for (__int64_t i2 : i)
                    std::cout << i2 << ' ';
                std::cout << std::endl;
            }

        }
        if (result.size() > 0) return result;

        query = "SELECT * FROM document_parts WHERE frg_l2_id = ?";
        if (config.print_debug) std::cout << "QUERY - GET ROW:" << query << std::endl;
        sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, id);

        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int i;
            int num_cols = sqlite3_column_count(stmt);
            std::vector<__int64_t> ivec;
            for (i = 0; i < num_cols; i++)
            {
                ivec.push_back(sqlite3_column_int64(stmt, i));

            }

            result.push_back(ivec);

        }

        sqlite3_finalize(stmt);

        if (config.print_debug) {
            std::cout << "Row =" + std::to_string(id) + ": ";
            for (std::vector<__int64_t> i : result) {
                for (__int64_t i2 : i)
                    std::cout << i2 << ' ';
                std::cout << std::endl;
            }

        }
        return result;

    }

    std::vector<__int64_t> get_children(__int64_t id) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::vector<__int64_t>> results = get_row(id);
        std::vector<__int64_t> retres;
        for (int i = 0; i < results.size(); i++) {
            for (int i2 = 0; i2 < results[i].size(); i2++) {
                if (results[i][i2] == id) {

                    for (int i3 = results[i].size() - 1; i3 > i2; i3--) {

                        retres.push_back(results[i][i3]);
                    }

                }
            }
        }
        if (config.print_debug) {
            std::cout << "Children of id=" + std::to_string(id) + ": ";
            for (int i : retres) std::cout << i << ' ';
            std::cout << std::endl;
        }
        auto stop = std::chrono::high_resolution_clock::now();
        if (config.d_track_time) config.deb_times.push_back(std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count());
        if (config.print_time) {
            std::cout << "Get_children duration = " << std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count() << " microsec" << std::endl;

        }
        return retres;
    }

    std::vector<__int64_t> get_parents(__int64_t id) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::vector<__int64_t>> results = get_row(id);
        std::vector<__int64_t> retres;
        for (int i = 0; i < results.size(); i++) {
            for (int i2 = 0; i2 < results[i].size(); i2++) {
                if (results[i][i2] == id) {
                    for (int i3 = 0; i3 < i2; i3++) {
                        retres.push_back(results[i][i3]);
                    }
                }
            }
        }
        if (config.print_debug) {
            std::cout << "Parents of id=" + std::to_string(id) + ": ";
            for (int i : retres) std::cout << i << ' ';
            std::cout << std::endl;
        }
        auto stop = std::chrono::high_resolution_clock::now();
        if (config.d_track_time) config.deb_times.push_back(std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count());
        if (config.print_time) {
            std::cout << "Get_parents duration = " << std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count() << " microsec" << std::endl;
        }
        return retres;
    }

    std::vector<__int64_t> get_relatives(__int64_t id) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::vector<__int64_t>> results = get_row(id);
        std::vector<__int64_t> retres;
        for (int i = 0; i < results.size(); i++) {
            for (int i2 = 0; i2 < results[i].size(); i2++) {
                if (results[i][i2] != id and std::find(retres.begin(), retres.end(), results[i][i2]) == retres.end()) {

                    retres.push_back(results[i][i2]);
                }
            }
        }
        auto stop = std::chrono::high_resolution_clock::now();
        if (config.d_track_time) config.deb_times.push_back(std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count());
        if (config.print_time) {
            std::cout << "Get_relatives duration = " << std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count() << " microsec" << std::endl;
        }
        return retres;
    }

    const char* connect() {
        if (config.print_debug) std::cout << config.name << std::endl;
        sqlite3_open(config.name.c_str(), &db);
        return sqlite3_errmsg(db);
    };
    const char* open() {
        std::string query = "CREATE TABLE IF NOT EXISTS document_parts(doc_id INT,frg_l1_id INT,frg_l2_id INT);";
        if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
        return "DB connected";
    };
    const char* insert(std::vector<__int64_t> values[3]) {
        auto start = std::chrono::high_resolution_clock::now();
        std::string query = "BEGIN TRANSACTION;";
        if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
        for (int i = 0; i < (values[0].size()); i++) {
            if (config.print_debug) std::cout << std::to_string(values[0][i]) << ", " << std::to_string(values[1][i]) << ", " << std::to_string(values[2][i]) << std::endl;
            query = "INSERT INTO document_parts VALUES(" + std::to_string(values[0][i]) + ',' + std::to_string(values[1][i]) + ',' + std::to_string(values[2][i]) + ");";
            if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
        }
        query = "COMMIT;";
        if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
        auto stop = std::chrono::high_resolution_clock::now();
        if (config.d_track_time) config.deb_times.push_back(std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count());
        if (config.print_time) std::cout << "Insert duration = " << std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count() << " microsec" << std::endl;
        return "Ok";
    };
    const char* removedoc(int docid) {
        auto start = std::chrono::high_resolution_clock::now();
        std::string query = "DELETE FROM document_parts WHERE doc_id=" + std::to_string(docid) + ";";

        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        auto stop = std::chrono::high_resolution_clock::now();
        if (config.print_time) {
            std::cout << "Remove doc duration = " << std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count() << " microsec" << std::endl;
        }
    };
    const char* print() {
        std::string query = "SELECT * FROM document_parts;";
        if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
    };
    const char* print_count() {
        std::string query = "SELECT COUNT(doc_id) FROM document_parts;";
        if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
        return "Count ok";
    };

    const char* check_index() {
        std::string query;
        query = "CREATE INDEX IF NOT EXISTS index_doc_id ON document_parts(doc_id) ;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        query = "CREATE INDEX IF NOT EXISTS index_doc_f1 ON document_parts(frg_l1_id) ;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        query = "CREATE INDEX IF NOT EXISTS index_doc_f2 ON document_parts(frg_l2_id) ;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        return "Index checked";
    }
    const char* re_index() {
        std::string query = "REINDEX index_doc ;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        return "Index reassemble";
    }
    const char* delete_index() {
        std::string query = "DROP INDEX IF EXISTS index_doc_id;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        query = "DROP INDEX IF EXISTS index_doc_f1;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        query = "DROP INDEX IF EXISTS index_doc_f2;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        return "Index deleted";
    }
    const char* pragmas() {
        std::string query = "PRAGMA page_size=" + std::to_string(config.page_size) + "; PRAGMA cache_size=" + std::to_string(config.cache_size) + "; PRAGMA locking_mode=EXCLUSIVE; PRAGMA synchronous = NORMAL; PRAGMA temp_store = memory;PRAGMA journal_mode=WAL";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        return "Pragma set";
    }
    const char* vacuum() {
        std::string query = "VACUUM;";
        if (sqlite3_exec(db, query.c_str(), 0, 0, &err)) return err;
        return "Vacuumed";
    }
    const char* print_indexes() {
        std::string query = "SELECT * FROM sqlite_master WHERE type='index';";
        if (sqlite3_exec(db, query.c_str(), print_callback, 0, &err)) return err;
        return "Index printed";
    }
    void flush_debug_time() {
        config.deb_times.clear();
    }
    int get_debug_time() {
        int sum = 0;
        for (int i = 0; i < config.deb_times.size(); i++) {
            sum += config.deb_times[i];
        }
        return sum / config.deb_times.size();
    }
    const char* initialize() {
        if (config.print_debug) std::cout << "Initializing DB..." << std::endl;
        if (std::strcmp(connect(), "not an error") != 0) return "Error opening file"; else if (config.print_debug) std::cout << "File opened" << std::endl;
        if (std::strcmp(pragmas(), "Pragma set") != 0) return "Error setting pragma parameters"; else if (config.print_debug) std::cout << "Pragma parameters set" << std::endl;
        if (std::strcmp(open(), "DB connected") != 0) return "Error checking/creating table"; else if (config.print_debug) std::cout << "Table check ok" << std::endl;
        if (std::strcmp(check_index(), "Index checked")) return "Index error";
        if (config.print_debug && config.reindex) {
            std::cout << "Re-indexing..." << std::endl;
            re_index();
        }
        if (config.print_debug && config.vac) {
            std::cout << "Vacuuming..." << std::endl;
            vacuum();
        }
        return "DB init success";
    }
};
