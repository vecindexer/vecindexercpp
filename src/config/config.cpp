
#include <sys/stat.h>
#include <algorithm>
#include <iostream>
#include <regex>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <nlohmann/json.hpp>

#include "config/YamlConfigMgr.h"
#include "config/config.h"
#include "utils/StringHelpFunctions.h"
#include "utils/ValidationUtil.h"
#include "utils/Log.h"

#include "easyloggingpp/easylogging++.h"

INITIALIZE_EASYLOGGINGPP

namespace vecindexer {

Config&
Config::GetInstance() {
    static Config config_inst;
    return config_inst;
}

Status
Config::LoadConfigFile(const std::string& filename) {
    if (filename.empty()) {
        return Status(SERVER_UNEXPECTED_ERROR, "No specified config file");
    }

    struct stat file_stat;
    if (stat(filename.c_str(), &file_stat) != 0) {
        std::string str = "Config file not exist: " + filename;
        return Status(SERVER_FILE_NOT_FOUND, str);
    }

    ConfigMgr* mgr = YamlConfigMgr::GetInstance();
    Status s = mgr->LoadConfigFile(filename);
    if (!s.ok()) {
        return s;
    }

    return Status::OK();
}

Status Config::ValidateConfig(bool debug) {
    /* server config */
    std::string service_name;
    CONFIG_CHECK(GetServerServiceName(service_name));
    
    std::string shard_name;
    CONFIG_CHECK(GetServerShardName(shard_name));
    
    std::string replica_name;
    CONFIG_CHECK(GetServerReplicaName(replica_name));
    
    std::string server_addr;
    CONFIG_CHECK(GetServerConfigAddress(server_addr));

    std::string server_port;
    CONFIG_CHECK(GetServerConfigPort(server_port));
    
    int server_workers;
    CONFIG_CHECK(GetServerWorkers(server_workers));
    
    std::string server_cluster_service_addr;
    CONFIG_CHECK(GetServerConfigClusterServiceAddress(server_cluster_service_addr));

    std::string server_cluster_service_port;
    CONFIG_CHECK(GetServerConfigClusterServicePort(server_cluster_service_port));
    
    std::string consul_addr;
    CONFIG_CHECK(GetConsulAddress(consul_addr));

    std::string consul_port;
    CONFIG_CHECK(GetConsulPort(consul_port));
    
    int leadership_sleep_ms;
    CONFIG_CHECK(GetLeadershipSleepMs(leadership_sleep_ms));
    
    std::string consul_wait;
    CONFIG_CHECK(GetConsulWait(consul_wait));
    
    std::string service_check;
    CONFIG_CHECK(GetConsulServiceCheck(service_check));

    std::string server_time_zone;
    CONFIG_CHECK(GetServerConfigTimeZone(server_time_zone));

    /* storage config */
    std::string storage_primary_path;
    CONFIG_CHECK(GetStorageConfigPrimaryPath(storage_primary_path));
    /* tracing config */
    std::string tracing_config_path;
    CONFIG_CHECK(GetTracingConfigJsonConfigPath(tracing_config_path));
    
    
    std::string message = "";
    if (debug)
        message += ". addr: " + server_addr + 
                   ", port: " + server_port + 
                   ", consul_addr: " + consul_addr + 
                   ", consul_port: " + consul_port + 
                   ", service_name: " + service_name + 
                   ", shard_name: " + shard_name + 
                   ", leadership_sleep_ms: " + std::to_string(leadership_sleep_ms) + 
                   ", cl_srv_addr: " + server_cluster_service_addr + 
                   ", cl_srv_port: " + server_cluster_service_port + 
                   ", tz: " + server_time_zone + 
                   ", path: " + storage_primary_path + 
                   ", tr_cnf_path: " + tracing_config_path ;
        

    return Status(0, message);
}


std::ostream& operator << (std::ostream& os, Config& cnf)
{
    Status s = cnf.ValidateConfig(true);
    os << s.ToString() << std::endl;
    return os;
}

ConfigNode&
Config::GetConfigRoot() {
    ConfigMgr* mgr = YamlConfigMgr::GetInstance();
    return mgr->GetRootNode();
}

ConfigNode&
Config::GetConfigNode(const std::string& name) {
    return GetConfigRoot().GetChild(name);
}

bool
Config::ConfigNodeValid(const std::string& parent_key, const std::string& child_key) {
    if (config_map_.find(parent_key) == config_map_.end()) {
        return false;
    }
    return config_map_[parent_key].count(child_key) != 0;
}


////////////////////////////////////////////////////////////////////////////////
/* consul config */
Status
Config::CheckLeadershipSleepMs(const int& value) {
    if (!(value >= 0 && value <= 3600000))
        return Status(SERVER_INVALID_ARGUMENT, "incorrect leadership_sleep_ms: expected value between [0;3600000]");

    return Status::OK();
}

Status
Config::CheckConsulWait(const std::string& value) {
    if (value.empty())
        return Status(SERVER_INVALID_ARGUMENT, "incorrect consul.wait");

    return Status::OK();
}

Status
Config::CheckConsulServiceCheck(const std::string& value) {
    return Status::OK();
}

Status
Config::CheckConsulAddress(const std::string& value) {
    auto exist_error = !ValidationUtil::ValidateUrl(value).ok();
    fiu_do_on("check_consul_address_fail", exist_error = true);

    if (exist_error) {
        std::string msg =
            "Invalid consul URL: " + value + ". Possible reason: consul.address is invalid.";
        return Status(SERVER_INVALID_ARGUMENT, msg);
    }
    return Status::OK();
}

/* server config */
Status
Config::CheckServerServiceName(const std::string& value) {
    if (value.length() < 5)
        return Status(SERVER_INVALID_ARGUMENT, "server service_name is too small");

    return Status::OK();
}

Status
Config::CheckServerShardName(const std::string& value) {
    if (value.empty())
        return Status(SERVER_INVALID_ARGUMENT, "server shard_name is empty");

    return Status::OK();
}

Status
Config::CheckServerReplicaName(const std::string& value) {
    if (value.empty())
        return Status(SERVER_INVALID_ARGUMENT, "server replica_name is empty");

    return Status::OK();
}

Status
Config::CheckServerConfigAddress(const std::string& value) {
    auto exist_error = !ValidationUtil::ValidateIpAddress(value).ok();
    fiu_do_on("check_config_address_fail", exist_error = true);

    if (exist_error) {
        std::string msg =
            "Invalid server IP address: " + value + ". Possible reason: server_config.address is invalid.";
        return Status(SERVER_INVALID_ARGUMENT, msg);
    }
    return Status::OK();
}

Status
Config::CheckServerConfigPort(const std::string& value) {
    auto exist_error = !ValidationUtil::ValidateStringIsNumber(value).ok();
    fiu_do_on("check_config_port_fail", exist_error = true);

    if (exist_error) {
        std::string msg = "Invalid server port: " + value + ". Possible reason: server_config.port is not a number.";
        return Status(SERVER_INVALID_ARGUMENT, msg);
    } else {
        int32_t port = std::stoi(value);
        if (!(port > 1024 && port < 65535)) {
            std::string msg = "Invalid server port: " + value +
                              ". Possible reason: server_config.port is not in range (1024, 65535).";
            return Status(SERVER_INVALID_ARGUMENT, msg);
        }
    }
    return Status::OK();
}

Status 
Config::CheckServerWorkers(const int& value)
{
    if (value > 0 and value < 101)
        return Status::OK();
    
    return Status(SERVER_INVALID_ARGUMENT, "Incorrect server workers!");
}

Status
Config::CheckServerConfigTimeZone(const std::string& value) {
    fiu_return_on("check_config_time_zone_fail",
                  Status(SERVER_INVALID_ARGUMENT, "Invalid server_config.time_zone: " + value));

    if (value.length() <= 3) {
        return Status(SERVER_INVALID_ARGUMENT, "Invalid server_config.time_zone: " + value);
    } else {
        if (value.substr(0, 3) != "UTC") {
            return Status(SERVER_INVALID_ARGUMENT, "Invalid server_config.time_zone: " + value);
        } else {
            try {
                stoi(value.substr(3));
            } catch (...) {
                return Status(SERVER_INVALID_ARGUMENT, "Invalid server_config.time_zone: " + value);
            }
        }
    }
    return Status::OK();
}

/* indexer */
Status 
Config::CheckIndexerOmpNumThreads(const int& value)
{
    // TODO
    return Status::OK();
}

/* storage */
Status
Config::CheckStorageConfigPrimaryPath(const std::string& value) {
    fiu_return_on("check_config_primary_path_fail", Status(SERVER_INVALID_ARGUMENT, ""));
    if (value.empty()) {
        return Status(SERVER_INVALID_ARGUMENT, "storage_config.db_path is empty.");
    }
    return Status::OK();
}

Status
Config::GetConfigValueInMem(const std::string& parent_key, const std::string& child_key, std::string& value) {
    std::lock_guard<std::mutex> lock(mutex_);
    if (config_map_.find(parent_key) != config_map_.end() &&
        config_map_[parent_key].find(child_key) != config_map_[parent_key].end()) {
        value = config_map_[parent_key][child_key];
        return Status::OK();
    }
    return Status(SERVER_UNEXPECTED_ERROR, "key not exist");
}

Status
Config::SetConfigValueInMem(const std::string& parent_key, const std::string& child_key, const std::string& value) {
    std::lock_guard<std::mutex> lock(mutex_);
    config_map_[parent_key][child_key] = value;
    return Status::OK();
}

////////////////////////////////////////////////////////////////////////////////
std::string
Config::GetConfigStr(const std::string& parent_key, const std::string& child_key, const std::string& default_value) {
    std::string value;
    if (!GetConfigValueInMem(parent_key, child_key, value).ok()) {
        value = GetConfigNode(parent_key).GetValue(child_key, default_value);
        SetConfigValueInMem(parent_key, child_key, value);
    }
    return value;
}

std::string
Config::GetConfigSequenceStr(const std::string& parent_key, const std::string& child_key, const std::string& delim,
                             const std::string& default_value) {
    std::string value;
    if (!GetConfigValueInMem(parent_key, child_key, value).ok()) {
        std::vector<std::string> sequence = GetConfigNode(parent_key).GetSequence(child_key);
        if (sequence.empty()) {
            value = default_value;
        } else {
            server::StringHelpFunctions::MergeStringWithDelimeter(sequence, delim, value);
        }
        SetConfigValueInMem(parent_key, child_key, value);
    }
    return value;
}


} // namespace vecindexer
