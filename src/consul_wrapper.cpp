
#include <iostream>

#include <boost/algorithm/string.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>

#include <boost/thread/thread.hpp>

#include "HTTPRequest.hpp"

#include "config/config.h"
#include "utils/Status.h"
#include "utils/Log.h"

#include "consul_wrapper.h"
#include "vec_indexer_app.h"

using json = nlohmann::json;

namespace vecindexer
{


std::string decode64(const std::string &val) {
    // https://stackoverflow.com/questions/7053538/how-do-i-encode-a-string-to-base64-using-only-boost
    using namespace boost::archive::iterators;
    using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;
    return boost::algorithm::trim_right_copy_if(std::string(It(std::begin(val)), It(std::end(val))), [](char c) {
        return c == '\0';
    });
}


std::string encode64(const std::string &val) {
    // https://stackoverflow.com/questions/7053538/how-do-i-encode-a-string-to-base64-using-only-boost
    using namespace boost::archive::iterators;
    using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
    auto tmp = std::string(It(std::begin(val)), It(std::end(val)));
    return tmp.append((3 - val.size() % 3) % 3, '=');
}


std::ostream& operator << (std::ostream& os, ConsulSession& c_s)
{
    os << "ConsulSession " << 
//        &c_s << 
       " [" << 
       c_s.opts_.service_id << 
//        ", " << c_s.GetOpts().id << 
//        ", " << c_s.GetOpts().endpoint << 
       ", curr_l " << c_s.GetCurrentLeader() << 
       ", cl_l " << c_s.GetClusterLeader() << 
//        ", cl_inst " << c_s.GetClusterInstances() << 
//        ", cl_foll " << c_s.GetClusterFollowers() << 
       ", sh_l " << c_s.GetShardLeader() << 
//        ", shard_inst " << c_s.GetShardInstances() << 
//        ", shard_foll " << c_s.GetShardFollowers() << 
//        ", c_h " << c_s.cluster_health_ << 
       "] " ; 
    return os;
}


Status ConsulSession::Start()
{
    programm_running_ = true;
    Status s = RegisterService();
    if (!s.ok())
        return s;

    s = CreateSession();
    if (!s.ok())
        return s;

    s = HealthService();
    return s;
}


void ConsulSession::Stop()
{
    ReleaseSession();
    DeregisterService();
}


Status ConsulSession::CreateSession(bool recreate)
{
    if (!opts_.id.empty() && !recreate)
    {
        return Status::OK();
    }
    
    int att_count = 20;
    int att = 0;
    int sleep_seconds = 1;
    auto _create_session_request = [&] ()
    {
        auto __create_session_request = [&] ()
        {
            http::Request req{CreateSessionUrl()};
            json req_body;
            req_body["Name"] = opts_.service_name;
            if (!opts_.service_check.empty())
            {
                req_body["ServiceChecks"] = json::array();
                json service_check_1;
                service_check_1["ID"] = "service:"+opts_.service_id+":1";
                json service_check_2;
                service_check_2["ID"] = "service:"+opts_.service_id+":2";
                req_body["ServiceChecks"].push_back(service_check_1);
                req_body["ServiceChecks"].push_back(service_check_2);
            }
            std::string req_body_str = req_body.dump();
            const auto res = req.send("PUT",
                                    req_body_str,
                                    {
                                        {"Content-Type", "application/json"}
                                    }
                                    );
            std::string result = std::string{res.body.begin(), res.body.end()};
            boost::trim(result);
            return result;
        };
        
        std::string result = __create_session_request();
        //apply failed: Check 'service:vecindexer.shard.2.replica.1.level.shard:1' is in critical state

        if (boost::algorithm::starts_with(result, "apply failed: Check"))
        {
            att += 1;
            ReleaseSession();
            while (att <= att_count)
            {
                att += 1;
                std::this_thread::sleep_for(std::chrono::seconds(sleep_seconds));
                result = __create_session_request();
                if (boost::algorithm::starts_with(result, "apply failed: Check"))
                    continue;
                
                break;
            }
        }
        return result;
    };
    
    try
    {
        Lock();
        
        std::string result = _create_session_request();
        
        try
        {
            json json_result = json::parse(result);
            opts_.id = json_result["ID"];
            CONSUL_SRV_LOG_DEBUG << "Session created " << this->ToString() << std::endl;
            SetLeader(opts_.id, opts_.leader.GetName());
            UnLock();
        }
        catch (const std::exception& e)
        {
            UnLock();
            std::stringstream err_desc; 
            CONSUL_SRV_LOG_ERROR << "DEBUG result: " << result << std::endl;
            err_desc << "Create session request failed (parse error, see result above)! " << this->ToString() << "; error: " << e.what();
            CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
            return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
        }
    }
    catch (const std::exception& e)
    {
        UnLock();
        std::stringstream err_desc; 
        err_desc << "Create session request failed! " << this->ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
    }

    return Status::OK();
}


Status ConsulSession::RetrieveSessionInfo(const std::string& session_id, ConsulSessionInfo& session_info)
{
    try
    {
        http::Request req{RetrieveSessionInfoUrl(session_id)};
        const auto res = req.send("GET");
        std::string result = std::string{res.body.begin(), res.body.end()};
        boost::trim(result);
        if (result == "[]")
        {
            session_info.SetEmpty();
            return Status::OK();
        }
        else  if (result == "No cluster leader")
        {
            std::stringstream error_message;
            error_message << "Retrieve session info (No cluster leader)!" << this->ToString();
            session_info.SetEmpty();
            SetEmptyCurrentLeader();
            return Status(CONSUL_SERVICE_NO_CLUSTER_LEADER, error_message.str());
        }
        
        try 
        {
            json json_result = json::parse(result)[0];
            auto lock_delay = json_result["LockDelay"];
            auto checks = json_result["Checks"];
            auto node = json_result["Node"];
            auto name = json_result["Name"];
            auto sess_id = json_result["ID"];
            auto create_index = json_result["CreateIndex"];
            session_info.Update(node, name, sess_id);
            
            return Status::OK();
        }
        catch (const std::exception& e)
        {
            std::stringstream err_desc; 
            CONSUL_SRV_LOG_ERROR << "DEBUG string_result: " << result << std::endl;
            err_desc << "Parse retrieve-session-info request failed! " << this->ToString() << "; error: " << e.what();
            CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
            throw std::runtime_error(err_desc.str());
        }
    }
    catch (const std::exception& e)
    {
        std::stringstream err_desc; 
        err_desc << "Retrieve-session-info request failed! " << this->ToString() << "; error: " << e.what();
        // CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
    }    
}


Status ConsulSession::RegisterService()
{
    try
    {   
        Lock();
        
        http::Request req{RegisterServiceUrl()}; 
        json req_body; 
        req_body["Name"] = opts_.service_name; 
        req_body["ID"] = opts_.service_id; 
        
        std::string leader_or_follower_tag (AmITheLeader()? "leader": "follower");
        
        req_body["Tags"] = {};
        if (!opts_.service_tag.empty())
            req_body["Tags"].push_back(opts_.service_tag);
        if (!opts_.shard_name.empty())
            req_body["Tags"].push_back("shard_"+opts_.shard_name);
        req_body["Tags"].push_back(leader_or_follower_tag);
            
        req_body["Meta"] = {
           {"cluster_service_port", opts_.cluster_service_port},
           {"shard_name", opts_.shard_name}
        }; 
        
        //req_body["Address"] = opts_.service_addr;
        req_body["Port"] = std::stoi(opts_.service_port); 
        req_body["Checks"] = {
            {
                {"tcp", "localhost:"+opts_.service_port},
                {"interval", "10s"},
                {"timeout", "1s"}
            }, // main service check
            {
                {"tcp", "localhost:"+opts_.cluster_service_port},
                {"interval", "10s"},
                {"timeout", "1s"}  
            } // cluster service port check (leader-follower communicator)
        };
        
        std::string req_body_str = req_body.dump(); 
        const auto res = req.send("PUT", 
                                  req_body_str 
                                 );
        std::string result = std::string{res.body.begin(), res.body.end()};

        boost::trim(result);

        UnLock();
        
        if (result.empty() && res.status.code == http::Status::Ok)
            return Status::OK();
        else
            return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, "RegisterService res not ok!");
    }
    catch (const std::exception& e)
    {
        UnLock();
        std::stringstream err_desc; 
        err_desc << "Register service request failed! " << this->ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
    }
    return Status::OK();
}


Status ConsulSession::DeregisterService()
{
    try
    {   
        Lock();
        
        http::Request req{DeregisterServiceUrl(opts_.service_id)}; 
        const auto res = req.send("PUT", 
                                  "" 
                                 );
        std::string result = std::string{res.body.begin(), res.body.end()};
        boost::trim(result);
        
        UnLock();
        
        if (result.empty() && res.status.code == http::Status::Ok)
            return Status::OK();
        else
            return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, "DeregisterService res not Ok!");
    }
    catch (const std::exception& e)
    {
        UnLock();
        std::stringstream err_desc; 
        err_desc << "Deregister service request failed! " << this->ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
    }
    return Status::OK();
}


Status ConsulSession::HealthService()
{
    try 
    {
        http::Request req{HealthServiceUrl(opts_.service_name)};
        const auto res = req.send("GET");
        std::string result = std::string{res.body.begin(), res.body.end()};
        
        if (   !result.empty() 
            && (   res.status.code == 200  // All health checks of every matching service instance are passing
                || res.status.code == 404  // No such service id or name
                || res.status.code == 429  // Some health checks are passing, at least one is warning
                || res.status.code == 503  // At least one health check is critical
                )
        )
        {
            try
            {
                json json_res = json::parse(result);
                cluster_health_.update(json_res);
            }
            catch (const std::exception& e)
            {
                CONSUL_SRV_LOG_ERROR << "DEBUG service_name: " << opts_.service_name << ";code: " << res.status.code << "; string_result: " << result << std::endl;
                std::stringstream err_desc; 
                err_desc << "HealthService request failed! Parsing failed (see string_result above)!" << this->ToString() << "; error: " << e.what();
                CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
                return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
            }
            return Status::OK();
        }
        else
        {
            std::stringstream err_desc; 
            err_desc << "HealthService res not Ok: " << this->ToString() ; 
            CONSUL_SRV_LOG_ERROR << "DEBUG service_name: " << opts_.service_name << ";code: " << res.status.code << "; string_result: " << result << "\n" << err_desc.str() << std::endl; 
            return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str()); 
        }
    }
    catch (const std::exception& e)
    {
        std::stringstream err_desc; 
        err_desc << "HealthService service request failed! " << this->ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
    }
    return Status::OK();
}


std::vector<ServiceHealth> ClusterHealth::get_services_by_tags(const std::vector<std::string>& tags) const
{
    std::vector<ServiceHealth> result;
    for (auto& [service_id, service_health]: health_map_)
    {
        bool matched=true;
        for (auto tag: tags)
        {
            if (service_health.tags.size() == 0)
                matched = false;
            if (std::find(service_health.tags.begin(), service_health.tags.end(), tag) == service_health.tags.end())
                matched = false;
        }
        if (matched)
            result.push_back(service_health);
    }
    return result;
}


std::ostream& operator<<(std::ostream& os, const ServiceHealth& s_h)
{
    os << s_h.service_id << "(" << s_h.service_addr << ":" << s_h.port << "," << s_h.aggregated_status << ")";
    return os;
}


std::ostream& operator<<(std::ostream& os, const std::vector<ServiceHealth>& s_h_l)
{
    os << "[" ;
    for (auto s_h: s_h_l)
        os << ' ' << s_h << ',';
    os << "] " ;
    return os;
}


void ClusterHealth::update(const json& json_result)
{
    
    std::map<std::string, ServiceHealth> health_map_tmp = std::map<std::string, ServiceHealth>();
    for (auto item: json_result)
    {
        std::string node = item["Node"]["Node"];
        std::string node_addr = item["Node"]["Address"];
        std::string serv_id = item["Service"]["ID"];
        std::string serv_name = item["Service"]["Service"];
        std::vector<std::string> tags = item["Service"]["Tags"];
        std::string serv_addr = item["Service"]["Address"];
        int port = item["Service"]["Port"];
        std::string c_s_p = item["Service"]["Meta"]["cluster_service_port"];
        std::string shard_name = item["Service"]["Meta"]["shard_name"];
        int cluster_service_port = std::stoi(c_s_p);
        
        
        if (   serv_addr.empty() 
            || serv_addr == "localhost" 
            || serv_addr == "http://localhost"
            || serv_addr == "https://localhost"
            || serv_addr == "127.0.0.1"
        )
            serv_addr = node_addr;
        
        
        std::string aggr_status = "passing";
        for (auto check: item["Checks"])
            if (check["Status"] != "passing")
            {
                aggr_status = "critical";
                break;
            }

        if (aggr_status == "passing")
        {
            ServiceHealth serv_health(serv_id,
                                      serv_name,
                                      serv_addr,
                                      port,
                                      cluster_service_port,
                                      shard_name,
                                      tags,
                                      aggr_status
                                      );
            health_map_tmp[serv_id] = serv_health;
        }
    }
    std::swap(health_map_, health_map_tmp);
}


std::vector<ServiceHealth> ConsulSession::GetShardInstances() const
{
    return cluster_health_.get_services_by_tags({"shard", "shard_" + opts_.shard_name});
}


std::vector<ServiceHealth> ConsulSession::GetShardFollowers() const
{
    return cluster_health_.get_services_by_tags({"shard", "follower", "shard_" + opts_.shard_name});
}


ServiceHealth ConsulSession::GetShardLeader() const
{
    auto res = cluster_health_.get_services_by_tags({"shard", "leader", "shard_" + opts_.shard_name});
    assert (res.size() <= 1);
    if (res.size() == 0)
        return ServiceHealth();
    else
        return res[0];
}

std::vector<ServiceHealth> ConsulSession::GetShardLeaders() const
{
    return cluster_health_.get_services_by_tags({"shard", "leader"});
}


std::vector<ServiceHealth> ConsulSession::GetClusterInstances() const
{
    return cluster_health_.get_services_by_tags();
}


std::vector<ServiceHealth> ConsulSession::GetClusterFollowers() const
{
    return cluster_health_.get_services_by_tags({"cluster", "follower"});
}


ServiceHealth ConsulSession::GetClusterLeader() const
{
    auto res = cluster_health_.get_services_by_tags({"cluster", "leader"});
    assert (res.size() <= 1);
    if (res.size() == 0)
        return ServiceHealth();
    else
        return res[0];
}


bool ConsulSession::AcquireSession()
{
    try
    {   
        Lock();
        
        http::Request req{CreateAcquireSessionUrl()}; 
        json req_body; 
        req_body["leader_service_id"] = opts_.leader.GetName(); 
        std::string req_body_str = req_body.dump(); 
        const auto res = req.send("PUT", 
                                  req_body_str 
                                 );
        std::string result = std::string{res.body.begin(), res.body.end()};
        
        boost::trim(result);
        boost::algorithm::to_lower(result);

        UnLock();
        
        if (result == "true")
        {
            return true;
        }
        
        return false;
    }
    catch (const std::exception& e)
    {
        UnLock();
        std::stringstream err_desc; 
        err_desc << "Acquire session request failed! " << this->ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return false;
    }
}


Status ConsulSession::RetrieveLeader(int& index)
{
    std::string fake_wait = "1s";
    return RetrieveLeader(false, index, fake_wait);
}


Status ConsulSession::RetrieveLeader(bool blocking, int& index, const std::string& wait)
{
    try
    {
        auto _get_session_leader = [&] ()
        {
            std::string url;
            if (blocking)
                url = CreateSessionLeaderBlockingUrl(index, wait);
            else
                url = CreateBaseSessionLeaderUrl();
            
            http::Request req(url);
            const auto res = req.send("GET");
            
            //for (auto hf : res.headerFields)
            //    std::cout << "DEBUG (headers): " << hf.first << ' ' << hf.second << std::endl;

            std::string string_result = std::string{res.body.begin(), res.body.end()};
            return string_result;
        };

        std::string string_result = _get_session_leader();
        boost::trim(string_result);
        
        std::stringstream error_message;
        Status error_status;
        Leader o_l = GetCurrentLeader();
        if (string_result.empty())
        {
            error_message << "There is no Leader(key does not exist)! Old leader: " 
                          << o_l.ToString() << ". " << this->ToString() ;
            error_status = Status(CONSUL_SERVICE_LEADER_KEY_DOES_NOT_EXIST, error_message.str());
            SetEmptyCurrentLeader();
            // CONSUL_SRV_LOG_ERROR << error_message.str() << std::endl;
            return error_status;
        }
        else  if (string_result == "No cluster leader")
        {
            error_message << "There is no Leader(No cluster leader)! Old leader: "
                          << o_l.ToString() << ". " << this->ToString() ;
            error_status = Status(CONSUL_SERVICE_NO_CLUSTER_LEADER, error_message.str());
            SetEmptyCurrentLeader();
            // CONSUL_SRV_LOG_ERROR << error_message.str() << std::endl;
            return error_status;
        }

        // int result_lock_index;
        std::string result_key;
        std::string result_value;
        std::string result_session;
        // int result_create_index;
        int result_modify_index;
        try
        {
            json json_result = json::parse(string_result);
            // result_lock_index = json_result[0]["LockIndex"];
            result_key = json_result[0]["Key"];
            // auto result_flags = json_result[0]["Flags"];
            auto res_value = json_result[0]["Value"];
            if (res_value.empty())
                result_value = "";
            else
                result_value = res_value;

            if (json_result[0].contains("Session"))
            {
                auto res_sess = json_result[0]["Session"];
                if (res_sess == NULL)
                    result_session = "";
                else
                    result_session = res_sess;
            }

            // result_create_index = json_result[0]["CreateIndex"];
            result_modify_index = json_result[0]["ModifyIndex"];
            boost::trim(result_value);
        }
        catch (const std::exception& e)
        {
            CONSUL_SRV_LOG_ERROR << "DEBUG string_result: " << string_result << std::endl;
            std::stringstream err_desc; 
            err_desc << "Get current leader request failed! Parsing failed (see string_result above)!" << this->ToString() << "; error: " << e.what();
            CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
            return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
        }
        
        index = result_modify_index;
        
        if (result_value.empty() || result_session.empty())
        {
            error_message << "There is no Leader(empty key)! Old leader: "
                          << o_l.ToString() << ". " << this->ToString() ;
            error_status = Status(CONSUL_SERVICE_EMPTY_LEADER, error_message.str());
            SetEmptyCurrentLeader();
            // CONSUL_SRV_LOG_ERROR << error_message.str() << std::endl;
            return error_status;
        }

        std::string current_leader_value = json::parse(decode64(result_value))["leader_service_id"];
        SetCurrentLeader(result_session, current_leader_value);

        return Status::OK();
    }
    catch (const std::exception& e)
    {
        std::stringstream err_desc; 
        err_desc << "Get current leader request failed! " << this->ToString() << "; error: " << e.what();
        SetEmptyCurrentLeader();
        // CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return Status(CONSUL_SERVICE_UNEXPECTED_ERROR, err_desc.str());
    }
}


bool ConsulSession::ReleaseSession()
{
    if (opts_.id.empty())
        return false;
    
    if (!AmITheLeader())
        return false;

    try
    {
        Lock();
        
        http::Request req{ReleaseSessionUrl()};
        std::string req_body_str = "";
        const auto res = req.send("PUT",
                                  req_body_str,
                                  {
                                      {"Content-Type", "text/plain"}
                                  }
                                 );
        std::string result = std::string{res.body.begin(),res.body.end()};
        boost::trim(result);
        boost::algorithm::to_lower(result);
        if (result == "true"){
            // CONSUL_SRV_LOG_INFO << "Released " << this->ToString() << std::endl;
            SetEmptyCurrentLeader();
        }
        UnLock();

        return true;
    }
    catch (const std::exception& e)
    {
        UnLock();
        std::stringstream err_desc; 
        err_desc << "Release session request failed! " << this->ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
        return false;
    }
}


void LeaderShipTask::operator() ()
{
    
    auto _keep_going_on = [&] () {return consul_service_ && consul_service_->IsProgrammRunning();};
    
    if (!(_keep_going_on()))
        return;
    
    bool previous_request_ok = true;
    bool printed_consul_unreachable = false;
    bool was_leader_before, current_leader_ok, am_i_current_leader;
    ServiceStatus service_status_before;
    Status st;
    int index = 1;
    std::string wait = consul_wait_;
    
    auto _request_ok = [&] 
    {
        return (   st.ok() 
                || st.code() == CONSUL_SERVICE_LEADER_KEY_DOES_NOT_EXIST 
                || st.code() == CONSUL_SERVICE_EMPTY_LEADER
               ); 
    };
    
    auto _retrieve = [&] (bool blocking=false) 
    {
        was_leader_before = consul_service_->AmITheLeader();
        service_status_before = consul_service_->GetServiceStatus();
    
        previous_request_ok = _request_ok();
        if (blocking)
            st = consul_service_->GetSession().RetrieveLeader(true, index, wait);
        else
            st = consul_service_->GetSession().RetrieveLeader(index);
        
        am_i_current_leader = consul_service_->AmITheLeader();
        current_leader_ok = st.ok();
        
        if (!current_leader_ok && _request_ok() && consul_service_->PermissionToAcquireLeadership())
        {
            bool leadership_acquired = consul_service_->GetSession().AcquireSession();
            if (leadership_acquired)
            {
                st = consul_service_->GetSession().RetrieveLeader(index);
                current_leader_ok = st.ok();
                am_i_current_leader = consul_service_->AmITheLeader();
            }
        }
    };
    
    auto _health_service= [&]()
    {
        st = consul_service_->GetSession().HealthService();
        if (!st.ok())
        {
            CONSUL_SRV_LOG_ERROR << "Failed to get health service!" << std::endl;
        }
    };
        
    
    if (!(_keep_going_on()))
        return;

    _retrieve();
    if (!(_keep_going_on()))
        return;

    _health_service();
    if (!(_keep_going_on()))
        return;

    if (current_leader_ok && am_i_current_leader)
        consul_service_->Setup(ServiceStatus::BecameLeader);
    else if (current_leader_ok && !am_i_current_leader)
        consul_service_->Setup(ServiceStatus::DowngradedToAFollower);

    if (!(_keep_going_on()))
        return;

    while (_keep_going_on())
    {
        int old_index = index;

        _retrieve(true);
        _health_service();

        if (!_keep_going_on())
            break;

        if (index < 1 || old_index > index)
        {
            index = 1;
            continue;
        }

        ServiceStatus service_status;

        if (was_leader_before && am_i_current_leader && current_leader_ok)
        {
            service_status = ServiceStatus::Leader;
        }
        else if (!current_leader_ok)
        {
            if (   was_leader_before
                || service_status_before == ServiceStatus::MissedLeader
            )
            {
                service_status = ServiceStatus::MissedLeader;
            }
            else
            {
                service_status = ServiceStatus::MissedFollower;
            }
        }
        else if (
                (was_leader_before && !am_i_current_leader)
            ||  (service_status_before == ServiceStatus::MissedLeader && (!was_leader_before && !am_i_current_leader))
        )
        {
            // 1-st case: I was a leader before, but now I am not a leader
            // 2-nd case: I was a missed leader before,  than I came back as Follower
            service_status = ServiceStatus::DowngradedToAFollower;
        }
        else if (!was_leader_before && !am_i_current_leader)
        {
            service_status = ServiceStatus::Follower;
        }
        else if (!was_leader_before && am_i_current_leader)
        {
            service_status = ServiceStatus::BecameLeader;
        }
        
        if (_request_ok() && old_index == index)
        {
            consul_service_->Setup(service_status);
            continue;
        }

        std::stringstream s;
        s << this << ' ';
        s << "before: " << ServiceStatusToString.at(service_status_before) << "; ";
        s << "status: " << ServiceStatusToString.at(service_status) << "; ";
        s << consul_service_->GetSession();
        
        if (   (service_status_before == ServiceStatus::Follower && service_status == ServiceStatus::DowngradedToAFollower )
            || (service_status_before == ServiceStatus::Leader && service_status == ServiceStatus::BecameLeader )
            || (service_status_before == ServiceStatus::DowngradedToAFollower && service_status == ServiceStatus::DowngradedToAFollower )
            || (service_status_before == ServiceStatus::BecameLeader && service_status == ServiceStatus::BecameLeader )
            || (service_status_before == ServiceStatus::Leader && service_status == ServiceStatus::MissedFollower )
            || (service_status_before == ServiceStatus::Follower && service_status == ServiceStatus::MissedLeader )
            || (service_status_before == ServiceStatus::MissedLeader && service_status == ServiceStatus::MissedFollower )
            || (service_status_before == ServiceStatus::MissedFollower && service_status == ServiceStatus::MissedLeader )
        )
            throw std::runtime_error("Undefined behavior: " + s.str());

        if (!previous_request_ok && !_request_ok())
        {
            if (!printed_consul_unreachable)
            {
                CONSUL_SRV_LOG_ERROR << "Request was failed again (consul is unreachable) . . ." << std::endl;
                printed_consul_unreachable = true;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms_));
        }
        else
            printed_consul_unreachable = false;
        
        consul_service_->Setup(service_status);
    }
    
    if (!(_keep_going_on()))
        return;

    if (consul_service_)
        CONSUL_SRV_LOG_INFO << "LeaderShipTask finished " << consul_service_ << ' ' << &consul_service_->GetSession() << std::endl;
    else
        CONSUL_SRV_LOG_ERROR <<"LeaderShipTask finished (empty consul_service!)" << std::endl;
}


Status get_consul_service_opts_from_config(ConsulServiceOpts& opts, 
                                           Config& config
                                          )
{
    std::string endpoint;
    std::string service_name;
    std::string shard_name;
    std::string replica_name;
    int leadership_sleep_ms;
    std::string service_check;
    
    std::string service_addr, service_port;
    std::string consul_addr, consul_port;
    std::string consul_wait;
    std::string cluster_service_addr, cluster_service_port;
    
    std::string service_id;
        
    if (!config.GetConsulAddress(consul_addr).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get consul_addr from config!");
    }
    if (!config.GetConsulPort(consul_port).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get consul_port from config!");
    }
    boost::trim_right_if(consul_addr, &isSlashSymb);
    boost::trim_right_if(service_addr, &isSlashSymb);
    endpoint = consul_addr + ":" + consul_port;
    
    if (!config.GetServerServiceName(service_name).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get service_name from config!");
    }
    if (!config.GetServerShardName(shard_name).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get shard_name from config!");
    }
    if (!config.GetServerReplicaName(replica_name).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get replica_name from config!");
    }
    if (!config.GetServerConfigAddress(service_addr).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get service_addr from config!");
    }   
    if (!config.GetServerConfigPort(service_port).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get service_port from config!");
    }
    if (!config.GetLeadershipSleepMs(leadership_sleep_ms).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get leadership_sleep_ms from config!");
    }
    if (!config.GetConsulWait(consul_wait).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get consul_wait from config!");
    }
    if (!config.GetConsulServiceCheck(service_check).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get service_check from config!");
    }
    if (!config.GetServerConfigClusterServiceAddress(cluster_service_addr).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get cluster_service_addr from config!");
    }
    if (!config.GetServerConfigClusterServicePort(cluster_service_port).ok()) {
        return Status(SERVER_INVALID_ARGUMENT, "Failed to get cluster_service_port from config!");
    }
    
    opts.endpoint = endpoint;
    opts.service_name = service_name;
    opts.service_addr = service_addr;
    opts.service_port = service_port;
    opts.shard_name = shard_name;
    opts.replica_name = replica_name;
    opts.leadership_sleep_ms = leadership_sleep_ms;
    opts.consul_wait = consul_wait;
    opts.cluster_service_addr = cluster_service_addr;
    opts.cluster_service_port = cluster_service_port;
    opts.service_check = service_check;
    
    return Status::OK();
}


Status ConsulService::Start()
{
    if (opts_.endpoint.empty() 
        && opts_.service_name.empty() 
        && opts_.leader.Empty() 
    ) 
    {
        Config& config = Config::GetInstance();
        Status s = get_consul_service_opts_from_config(opts_, config);
        if (!s.ok())
            return s;
    }
    
    return Start(opts_);
}


Status ConsulService::Start(const ConsulServiceOpts& opts)
{
    opts_ = opts;
    programm_running_ = true;
    status_ = ServiceStatus::Follower;

    std::string service_id;
    
    std::string service_name = opts.service_name;
    std::string service_leadership_key = service_name;
    std::string service_tag;
    
    if (!service_name_suffix_.empty())
    {
        if (service_name_suffix_ == REPLICA_SUFFIX)
        {
            service_tag = "shard";
            if (!opts.shard_name.empty())
                service_leadership_key += "_" + service_tag + "_" + opts.shard_name;
        }
        else
        {
            service_tag = "cluster";
            service_leadership_key += "_" + service_tag;
        }   
    }
    else if (!opts.service_name_suffix.empty())
    {
        service_tag = opts.service_name_suffix;
    }
    
    service_id = service_name;
    if (!opts.shard_name.empty())
        service_id += ".shard." + opts.shard_name;
    if (!opts.replica_name.empty())
        service_id += ".replica." + opts.replica_name;
    if (!service_tag.empty() && service_tag == "shard")
        service_id += ".level." + service_tag;
    else if (!service_tag.empty() && service_tag == "cluster")
        {} // pass
    else
        service_id += "." + opts.service_tag;
    
    Leader leader = opts.leader;
    leader.Update("", service_id);
    
    session_ = ConsulSession(ConsulSessionOpts(opts.endpoint, 
                                               service_name,
                                               service_leadership_key,
                                               service_tag,
                                               service_id,
                                               opts.service_addr,
                                               opts.service_port,
                                               opts.shard_name,
                                               opts.replica_name,
                                               opts.id,
                                               leader,
                                               opts.current_leader,
                                               opts.leadership_sleep_ms,
                                               opts.consul_wait,
                                               opts.cluster_service_addr,
                                               opts.cluster_service_port,
                                               opts.service_check
                                              )
                            );

    Status s = session_.Start();
    if (!s.ok())
        return Status(SERVER_UNEXPECTED_ERROR, "Failed to start session. " + s.ToString());
    
    LeaderShipTask leadership_task  = LeaderShipTask(this);
    leadership_thread_ = std::make_shared<std::thread>(leadership_task);
    
    return Status(SERVER_SUCCESS, "ConsulService started");
}


void ConsulService::TryToEnsureSession()
{
    Status s = this->session_.HealthService();
    if (s.ok() && !this->session_.ServiceRegistered())
        s = session_.RegisterService();

    s = this->session_.HealthService();
    if (!this->session_.ServiceRegistered())
        return;
    
    ConsulSessionInfo csi;
    csi.SetEmpty();
    std::string current_sess_id = this->session_.GetOpts().id;
    s = this->session_.RetrieveSessionInfo(current_sess_id, csi);
    if (s.ok() && csi.id != current_sess_id)
    {
        // if consul is reachable but our session does not exist more => recreate session!
        this->session_.CreateSession(true);
    }
}


void ConsulService::Setup(const ServiceStatus& service_status)
{
    VecIndexerApp& app = VecIndexerApp::GetInstance();
    if (   status_ != service_status
        ||service_status == ServiceStatus::BecameLeader
        ||service_status == ServiceStatus::DowngradedToAFollower
    )
        GetSession().RegisterService();
        
    status_ = service_status;
    switch (status_)
    {
        case ServiceStatus::Follower:
            //CONSUL_SRV_LOG_DEBUG << "Follower: " << this->session_.ToString() << std::endl;
            break;
        case ServiceStatus::Leader:
            //CONSUL_SRV_LOG_DEBUG << "Leader: " << this->session_.ToString() << std::endl;
            break;
        case ServiceStatus::BecameLeader:
            // TODO upgrade steps
            CONSUL_SRV_LOG_INFO << "BecameLeader: " << this->session_.ToString() << std::endl;
            status_ = ServiceStatus::Leader;
            break;
        case ServiceStatus::DowngradedToAFollower:
            // TODO downgrade steps
            CONSUL_SRV_LOG_INFO << "DowngradedToAFollower: " << this->session_.ToString() << std::endl;
            status_ = ServiceStatus::Follower;
            break;
        case ServiceStatus::MissedLeader:
            CONSUL_SRV_LOG_ERROR << "MissedLeader: " << this->session_.ToString() << std::endl;
            TryToEnsureSession();
            break;
        case ServiceStatus::MissedFollower:
            CONSUL_SRV_LOG_ERROR << "MissedFollower: " << this->session_.ToString() << std::endl;
            TryToEnsureSession();
            break;
        case ServiceStatus::Undefined:
            break;
    }
}


void ConsulService::Stop()
{
    ProgrammStopped();
    
    try
    {
        if (leadership_thread_->joinable())
        {
            CONSUL_SRV_LOG_INFO << "Try to join leadership thread! " << this->session_.ToString() << std::endl;
            leadership_thread_->join();
            CONSUL_SRV_LOG_INFO << "Leadership thread joined! " << this->session_.ToString() << std::endl;
        }
        else
        {
            CONSUL_SRV_LOG_INFO << "Leadership thread is not joinable! " << this->session_.ToString() << std::endl;
        }
    }
    catch (const std::exception& e)
    {
        std::stringstream err_desc; 
        err_desc << "Leadership thread join failed! " << this->session_.ToString() << "; error: " << e.what();
        CONSUL_SRV_LOG_ERROR << err_desc.str() << std::endl;
    }

    this->session_.Stop();
    CONSUL_SRV_LOG_INFO << "ConsulService stoped! " << this->session_.ToString() << std::endl;
}

    
Status ConsulShardService::Start()
{
    Status s = ConsulService::Start();
    if (!s.ok()) 
        return s;
    
    return Status(SERVER_SUCCESS, "ConsulShardService started");
}


Status ConsulReplicaService::Start()
{
    Status s = ConsulService::Start();
    if (!s.ok()) 
        return s;

    return Status(SERVER_SUCCESS, "ConsulReplicaService started");
}


void ConsulShardService::Stop()
{
    ConsulService::Stop();
    SERVER_LOG_INFO << "ConsulShardService finished!" << std::endl;
}


void ConsulReplicaService::Stop()
{
    ConsulService::Stop();
    SERVER_LOG_INFO << "ConsulReplicaService finished!" << std::endl;
}


} // namespace vecindexer
