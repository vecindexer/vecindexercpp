
#include "indexer.h"

#include <boost/filesystem.hpp>
#include <iostream>
#include <shared_mutex>

#include <faiss/index_factory.h>
#include <faiss/index_io.h>

#include "nlohmann/json.hpp"
#include <fstream>

#include <faiss/gpu/StandardGpuResources.h>
#include <faiss/gpu/GpuIndex.h>
#include <faiss/gpu/GpuCloner.h>


#include <cuda_profiler_api.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <faiss/gpu/utils/DeviceUtils.h>


#include "vec_indexer_app.h"


namespace vecindexer {
namespace server {
    
using idx_t = int64_t;

void ensure_directory_created(const std::string& dirname)
{
    if (!boost::filesystem::exists(dirname))
        boost::filesystem::create_directory(dirname);
    
    if (!boost::filesystem::is_directory(dirname))
        throw std::runtime_error("Failed to create directory " + dirname);
}


Status Indexer::Start()
{
    ensure_directory_created(opts_.storage_path);
    return Status::OK();
}


void Indexer::Stop()
{       
    for (auto& [coll_id, collection]: coll_map_)
    {
        Status s = Status::OK();
        if (collection.on_gpu)
        {
            if (collection.count == 0 || ! collection.index_ptr->is_trained)
                continue;
            try 
            {
                s = collection.MoveToCpu();
                if (!s.ok())
                {
                    std::string str = std::to_string(coll_id) + " Failed to move to cpu: " + s.message();
                    SERVER_LOG_INFO << str << std::endl;
                }
            }
            catch (std::exception& ex) 
            {
                std::string str = std::to_string(coll_id) + " move to cpu error (skip saving): " + std::string(ex.what());
                SERVER_LOG_ERROR << str << std::endl;
                continue;
            }
        }
        
        collection.Save();
    }
    SERVER_LOG_INFO << "Indexer finished!" << std::endl;
}


Status Indexer::CreateCollection(
    int coll_id, 
    const std::string index_description,
    int dimension
)
{
    if (coll_map_.contains(coll_id) and coll_map_[coll_id].ok)
    {
        return Status(0, "Collection is already OK (loaded and ready). Notning to do.");
    }
    
    std::string coll_dir (opts_.storage_path);
    boost::trim_right_if(coll_dir, &isSlashSymb);
    coll_dir += "/"+std::to_string(coll_id);
    coll_map_[coll_id] = Collection(coll_id, 
                                    coll_dir,
                                    dimension, 
                                    index_description
                                   );
    if (coll_map_[coll_id].ok)
    {
        return Status(0, "Collection is already OK (loaded existing collection).");
    }

    Status s = coll_map_[coll_id].Create();
    if (s.ok())
        SERVER_LOG_INFO << "Created collection " << coll_id << " (" << index_description << ", " << dimension << ")" << std::endl;
    else
        SERVER_LOG_ERROR << "Failed to create collection " << coll_id << " (" << index_description << ", " << dimension << "):" << s.message() << std::endl;
    return s;
}


Status Indexer::Train(
    int coll_id, 
    const VectorsData& vectors
)
{
    Status s;
    int dimension = vectors.get_dimension();
    if (!coll_map_.contains(coll_id))
    {
        std::string coll_dir (opts_.storage_path);
        boost::trim_right_if(coll_dir, &isSlashSymb);
        coll_dir += "/"+std::to_string(coll_id);
        std::string index_file = make_index_filename(coll_dir, coll_id);
        if (boost::filesystem::exists(index_file))
        {
            coll_map_[coll_id] = Collection(coll_id, coll_dir);
            if (!coll_map_[coll_id].ok)
            {
                s = CreateCollection(coll_id, "IDMap,IVF1024,PQ64", dimension);
                if (!s.ok())
                    return s;
            }
        }
    }

    if (dimension != coll_map_[coll_id].dimension)
        return Status(SERVER_UNEXPECTED_ERROR,  "train-vectors dimension does not match with collection dimension!");
    
    return coll_map_[coll_id].Train(vectors);
}


bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}


bool Indexer::IsCollectionOk(int coll_id)
{
    return coll_map_.contains(coll_id) and coll_map_[coll_id].ok;
}


void Indexer::EnsureCollectionLoaded(int coll_id)
{
    if (IsCollectionOk(coll_id))
        return;

    
    std::string colls_dir (opts_.storage_path);
    boost::trim_right_if(colls_dir, &isSlashSymb);
    
    try
    {
        std::string coll_dir (colls_dir);
        coll_dir += "/"+std::to_string(coll_id);
        coll_map_[coll_id] = Collection(coll_id, coll_dir);
    }
    catch (std::exception& ex) 
    {
        std::string str = std::to_string(coll_id) + " load error: " + std::string(ex.what());
        SERVER_LOG_ERROR << str << std::endl;
    }
}


void Indexer::LoadCollectionsFromStorage(std::vector<int> collections)
{
    std::string colls_dir (opts_.storage_path);
    boost::trim_right_if(colls_dir, &isSlashSymb);
    
    if (!collections.empty())
    {
        for (auto coll_id : collections)
            EnsureCollectionLoaded(coll_id);

        return;
    }

    if (boost::filesystem::is_directory(colls_dir))
    {
        for (auto& item: boost::make_iterator_range(boost::filesystem::directory_iterator(colls_dir),{}))
        {
            if (boost::filesystem::is_directory(item))
            {
                std::string stem = item.path().stem().string();
                if (is_number(stem))
                {
                    EnsureCollectionLoaded(std::stoi(stem));
                }
            }
        }
    }
    else if (boost::filesystem::exists(colls_dir))
        throw std::runtime_error("check collections dir!");

    return;
}


Status Indexer::CollectionInfo(json& collections_info, std::vector<int> collections)
{
    LoadCollectionsFromStorage(collections);
    
    collections_info = json::array();
    
    std::set<int> select_colls;
    for (auto coll_id: collections)
        select_colls.insert(coll_id);
    
    for (auto& [coll_id, collection]: coll_map_)
    {
        if (!select_colls.empty() && select_colls.contains(coll_id))
            collections_info.push_back(collection.GetInfo());
        else
            collections_info.push_back(collection.GetInfo());
    }
    
    return Status::OK();
}


Status Indexer::Add(
    int coll_id, 
    const VectorsData& vectors
)
{
    Status s;
    int dimension = vectors.get_dimension();
    
    EnsureCollectionLoaded(coll_id);
    if (!IsCollectionOk(coll_id))
    {
        s = CreateCollection(coll_id, "IDMap,HNSW32", dimension);
        if (!s.ok())
            return s;
    }
    
    if (dimension != coll_map_[coll_id].dimension)
        return Status(SERVER_UNEXPECTED_ERROR,  "add-vectors dimension does not match with collection dimension!");
    
    return coll_map_[coll_id].Add(vectors);
}


Status Indexer::Search(int coll_id, const VectorsData& vectors, int k, ResultIds& I, ResultDistances& D)
{
    EnsureCollectionLoaded(coll_id);
    if (!IsCollectionOk(coll_id))
        return Status(SERVER_UNEXPECTED_ERROR,  "Collection does not exist or not ok!");

    if (vectors.get_dimension() != coll_map_[coll_id].dimension)
        return Status(SERVER_UNEXPECTED_ERROR,  "query-vectors dimension does not match with collection dimension!");

    return coll_map_[coll_id].Search(vectors, k, I, D);
}


bool IsCudaAvailable()
{
    // rewrited faiss::gpu::getNumDevices
    int numDev = -1;
    cudaError_t err = cudaGetDeviceCount(&numDev);
    if (cudaErrorNoDevice == err) {
        numDev = 0;
    } else if (err != cudaSuccess){
        return false;
    }
    return numDev != -1;
}


Status Indexer::MoveToGpu(int coll_id,
                          bool useFloat16CoarseQuantizer, 
                          bool useFloat16, 
                          bool usePrecomputed, 
                          bool storeTransposed)
{
    EnsureCollectionLoaded(coll_id);
    
    if (!IsCollectionOk(coll_id))
        return Status(SERVER_UNEXPECTED_ERROR,  "Collection does not exist or not ok!");
    else if (coll_map_[coll_id].on_gpu)
    {
        return Status(0,  "Already on GPU!");
    }
    else
    {
        // https://github.com/facebookresearch/faiss/wiki/Running-on-GPUs
        // https://faiss.ai/cpp_api/namespace/namespacefaiss_1_1gpu.html
        
        if (!IsCudaAvailable())
            return Status(SERVER_UNEXPECTED_ERROR,  "Failed to locate index on GPU(CUDA is not available)");
        
        try{
            if (faiss::gpu::getNumDevices() <= 0)
                return Status(SERVER_UNEXPECTED_ERROR,  "Failed to locate index on GPU(There is GPU devices)");
        }
        catch (std::exception& ex) 
        {
            return Status(SERVER_UNEXPECTED_ERROR,  "Failed to locate index on GPU(Failed to getNumDevices)");
        }
        
        coll_map_[coll_id].Save();
        uint64_t on_disk_size = coll_map_[coll_id].GetOnDiskSize();
        SERVER_LOG_DEBUG << "collection on_disk_size: " << on_disk_size <<std::endl;
        coll_map_[coll_id].Load();
        
        int extra_memory_bytes = 500*1024*1024; // 500 MiB
        
        int required_mem_bytes = on_disk_size + extra_memory_bytes;

        for (int device=0; device<faiss::gpu::getNumDevices();device++)        
        {
            if (faiss::gpu::getFreeMemory(device) > required_mem_bytes)
            {
                Status s = coll_map_[coll_id].MoveToGpu(device,
                                                        useFloat16CoarseQuantizer, 
                                                        useFloat16, 
                                                        usePrecomputed, 
                                                        storeTransposed);
                return s;
            }
        }
        return Status(SERVER_UNEXPECTED_ERROR,  "Failed to locate index on GPU(There is no memory on GPU(s))");
    }
}


Status Indexer::MoveToCpu(int coll_id)
{
    EnsureCollectionLoaded(coll_id);
    
    if (!IsCollectionOk(coll_id))
        return Status(SERVER_UNEXPECTED_ERROR,  "Collection does not exist or not ok!");
    else if (!coll_map_[coll_id].on_gpu)
    {
        return Status(0,  "Already on CPU!");
    }
    else
    {
        Status s = coll_map_[coll_id].MoveToCpu();
        return s;
    }
}


Status Collection::Create()
{
    std::unique_lock<std::shared_mutex> ul(*mutex_);
    ok = false;
    index_ptr = std::shared_ptr<faiss::Index>(index_factory(dimension, 
                                                            index_str.c_str(), 
                                                            faiss::METRIC_INNER_PRODUCT 
                                                            )
                                              );
    ok = true;
    return Status::OK();
}


json Collection::GetInfo() const
{
    json info;
    info = {
        {"coll_id", coll_id},
        {"dimension", dimension},
        {"count", count},
        {"index_description", index_str},
        {"trained", index_ptr->is_trained},
        {"on_gpu", on_gpu},
    };
    return info;
}

Status Collection::Train(const VectorsData& vectors)
{
    if (index_ptr->is_trained)
    {
        return Status(0, "Index is already trained!");
    }

    std::unique_lock<std::shared_mutex> ul(*mutex_);
    
    index_ptr->train(vectors.vector_count_, 
                     vectors.float_data_.data()
                    );
    return Status::OK();
}


Status Collection::Add(const VectorsData& vectors)
{
    count+=vectors.vector_count_;
    
    std::unique_lock<std::shared_mutex> ul(*mutex_);
    
    index_ptr->add_with_ids(vectors.vector_count_, 
                            vectors.float_data_.data(),
                            vectors.id_array_.data()
                           );
    return Status::OK();
}


Status Collection::Search(const VectorsData& vectors, int k, ResultIds& I, ResultDistances& D)
{
    if (!ok)
        return Status(SERVER_UNEXPECTED_ERROR,  "Collection not ok!");
    
    if (index_ptr->ntotal == 0 || index_ptr->ntotal < k)
        return Status(SERVER_UNEXPECTED_ERROR,  "Too few vectors in index!");
    
    if (k * vectors.vector_count_ <= 0)
        return Status(SERVER_UNEXPECTED_ERROR,  "Incorrect k*vector_count!");
    
    std::unique_lock<std::shared_mutex> sl(*mutex_);
    
    I.resize(k * vectors.vector_count_);
    D.resize(k * vectors.vector_count_);
    
    index_ptr->search(vectors.vector_count_,  
                      vectors.float_data_.data(),
                      k, D.data(), I.data());
    return Status::OK();
}


void Collection::Save()
{
    try
    {
        std::unique_lock<std::shared_mutex> ul(*mutex_);
        
        json index_params = 
        {
            {"coll_id", coll_id},
            {"dimenstion", dimension},
            {"count", count},
            {"index_description", index_str},
            {"on_gpu", on_gpu},
        };
        std::string index_params_file = index_file+".json";
        std::ofstream of(index_params_file.c_str());
        of << index_params;
        faiss::write_index(index_ptr.get(), index_file.c_str());
    }
    catch (std::exception& ex) 
    {
        std::string str = std::to_string(coll_id) + " save error: " + std::string(ex.what());
        SERVER_LOG_ERROR << str << std::endl;
    }
}


void Collection::Load()
{
    try
    {
        std::unique_lock<std::shared_mutex> ul(*mutex_);
        
        if (!boost::filesystem::exists(index_file))
            return;

        index_ptr = std::shared_ptr<faiss::Index>(faiss::read_index(index_file.c_str()));
        count = index_ptr->ntotal;
        
        std::string index_params_file = index_file+".json";
        std::string index_params_s;
        std::ifstream fi(index_params_file.c_str());
        fi >> index_params_s;
        json index_params = json::parse(index_params_s);
        index_str = index_params["index_description"];
        
        SERVER_LOG_INFO << "Collection " << coll_id << " loaded from " << index_file << ", dim: "<< index_ptr->d << ", ntotal: " << count << std::endl;
        
        if (dimension==0)
            dimension = index_ptr->d;
        else if (dimension != index_ptr->d)
        {
            SERVER_LOG_ERROR << "Index dimension is not matched with loaded!";
        }
        ok = true;
    }
    catch (std::exception& ex) 
    {
        ok = false;
        std::string str = std::to_string(coll_id) + " load error: " + std::string(ex.what());
        SERVER_LOG_ERROR << str << std::endl;
    }
}


uintmax_t Collection::GetOnDiskSize()
{
    boost::filesystem::path filepath (index_file);
    if ( !boost::filesystem::exists(index_file) )
    {
        SERVER_LOG_ERROR << index_file << ' ' << "not found!" << std::endl;
        return 0;
    }
    
    return boost::filesystem::file_size(filepath);
}


Status Collection::MoveToGpu(int device, 
                             bool useFloat16CoarseQuantizer, 
                             bool useFloat16, 
                             bool usePrecomputed, 
                             bool storeTransposed
                             )
{
    Status s;
    try
    {
        std::unique_lock<std::shared_mutex> ul(*mutex_);
        faiss::gpu::StandardGpuResources res;
        faiss::gpu::GpuClonerOptions co;
        co.useFloat16CoarseQuantizer = useFloat16CoarseQuantizer;
        co.useFloat16 = useFloat16;
        co.usePrecomputed = usePrecomputed;
        co.storeTransposed = storeTransposed;

        index_ptr = std::shared_ptr<faiss::Index>(faiss::gpu::index_cpu_to_gpu(&res, device, index_ptr.get(), &co));
        on_gpu = true;
        s = Status::OK();
    }
    catch (std::exception& ex) 
    {
        std::string str = std::to_string(coll_id) + " MoveToGpu error: " + std::string(ex.what());
        SERVER_LOG_ERROR << str << std::endl;
        s = Status(SERVER_UNEXPECTED_ERROR, str); 
    }
    return s;
}


Status Collection::MoveToCpu()
{
    Status s;
    try
    {
        std::unique_lock<std::shared_mutex> ul(*mutex_);
        index_ptr = std::shared_ptr<faiss::Index>(faiss::gpu::index_gpu_to_cpu(index_ptr.get()));
        on_gpu = false;
        s = Status::OK();
    }
    catch (std::exception& ex) 
    {
        std::string str = std::to_string(coll_id) + " MoveToCpu error: " + std::string(ex.what());
        SERVER_LOG_ERROR << str << std::endl;
        s = Status(SERVER_UNEXPECTED_ERROR, str); 
    }
    return s;
}

} // namespace server
} // namespace vecindexer
