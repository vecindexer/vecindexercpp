// main application

#include <csignal>
#include <iostream>
#include <boost/program_options.hpp>

#include "vec_indexer_app.h"

#include "config/config.h"
#include "utils/Log.h"
#include "utils/SignalUtil.h"

namespace po = boost::program_options;

using namespace vecindexer ;

VecIndexerAppOptions parse_cli_arguments(int argc, char* argv[])
{
    VecIndexerAppOptions opts = VecIndexerAppOptions();
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help,h", "print usage message")
    ("config,c", po::value(&opts.config_file), "config file (yaml)")
    ("log_config,l", po::value(&opts.config_file), "config file (yaml)")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    
    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        exit(0);
    }
    
    if (vm.count("config"))
        opts.config_file = vm["config"].as<std::string>();
    
    return opts;
}

int main(int argc, char* argv[])
{
    try 
    {
        VecIndexerAppOptions opts = parse_cli_arguments(argc, argv);
        
        VecIndexerApp& app = VecIndexerApp::GetInstance();
        
        /* Handle Signal */
        signal(SIGHUP, vecindexer::SignalUtil::HandleSignal);
        signal(SIGINT, vecindexer::SignalUtil::HandleSignal);
        signal(SIGUSR1, vecindexer::SignalUtil::HandleSignal);
        signal(SIGUSR2, vecindexer::SignalUtil::HandleSignal);
        signal(SIGTERM, vecindexer::SignalUtil::HandleSignal);
        
        /* Init and start */
        app.Init(opts.config_file);
        Status s = app.Start();
            
        if (s.ok()) {
            APP_LOG_INFO << "VecIndexerApp started" << std::endl;
        } else {
            APP_LOG_ERROR << "VecIndexerApp did not start: " + s.ToString() << std::endl;
            return SERVER_UNEXPECTED_ERROR;
        }
        
        /* wait signal */
        pause();
    }
    catch(std::exception& e)
    {
        APP_LOG_ERROR << "VecIndexerApp runtime error:" << e.what() << std::endl;
        return SERVER_UNEXPECTED_ERROR;
    }
    
    APP_LOG_INFO << "VecIndexerApp finished!" << std::endl;
    
    return 0;
}
