#include "server.h"

#include <faiss/utils/distances.h>
#include <boost/format.hpp>

#include <thread>
#include <iostream>
#include <queue>
#include <sys/sysinfo.h>
#include "DocLevelOrganizer.hpp"



namespace vecindexer {
namespace server {
namespace zeromq {



void ZeroMQServer::Start()
{
    std::thread(&ZeroMQServer::main_loop, this).detach();
}


void ZeroMQServer::main_loop()
{

    zmq::socket_t clients (zmq_context_, zmq::socket_type::router);
    clients.bind(opts_.endpoint);

    zmq::socket_t service (zmq_context_, zmq::socket_type::router);
    service.bind(opts_.cluster_service_endpoint);

    zmq::socket_t workers (zmq_context_, zmq::socket_type::router);
    workers.bind ("inproc://workers");

    zmq::socket_t service_workers (zmq_context_, zmq::socket_type::router);
    service_workers.bind ("inproc://cluster_service_workers");

    zmq::socket_t control (zmq_context_, zmq::socket_type::pair);
    control.bind ("inproc://control");

    for(int i = 0; i < opts_.threads; ++i)
        std::thread(&ZeroMQServer::process, this).detach();

    for(int i = 0; i < 2; ++i)
        std::thread(&ZeroMQServer::cluster_service_process, this).detach();

    std::queue<std::string> worker_queue;
    std::queue<std::string> cluster_service_worker_queue;

    while(true){
        zmq::pollitem_t items [] = {
            // 0 - control
            { control, 0, ZMQ_POLLIN, 0 },
            // 1 - Always poll for worker activity on backend
            { workers, 0, ZMQ_POLLIN, 0 },
            // 2 - Always poll for service worker activity on backend
            { service_workers, 0, ZMQ_POLLIN, 0 },
            // 3 - Poll front-end only if we have available workers
            { clients, 0, ZMQ_POLLIN, 0 },
            // 4 - Poll service only if we have available cluster service workers
            { service, 0, ZMQ_POLLIN, 0 },
        };

        int items_check = 5;

        try{
            zmq::poll(&items[0], items_check, -1);
            // SERVER_LOG_ERROR<<"out of poll"<<std::endl;
        }catch(const zmq::error_t& e){
            SERVER_LOG_ERROR<<"zmq error "<<e.what()<<std::endl;

            if (e.num() == EINTR)
                break;
            SERVER_LOG_ERROR<<"poll error: "<<"num - "<<e.num()<<" "<<e.what()<<std::endl;
        }

        if (items[0].revents & ZMQ_POLLIN)
            break;

        //  Handle worker activity on backend
        if (items[1].revents & ZMQ_POLLIN)
        {
            //  Queue worker address for LRU routing
            auto address = recv_str(workers);
            worker_queue.push(address);
            SERVER_LOG_DEBUG <<"worker_queue pushed. size: " << worker_queue.size() << std::endl;

            {
                //  Second frame is empty
                std::string empty = recv_str(workers);
                assert(empty.size() == 0);
            }

            //  Third frame is READY or else a client reply address
            std::string client_addr = recv_str(workers);

            //  If client reply, send rest back to frontend
            if (client_addr.compare("READY") != 0)
            {
                {
                    std::string empty = recv_str(workers);
                    assert(empty.size() == 0);
                }

                zmq::message_t resp;
                workers.recv(resp);
                clients.send(zmq::buffer(client_addr.data(), client_addr.size()),
                             zmq::send_flags::sndmore);
                clients.send(zmq::message_t(), zmq::send_flags::sndmore);
                clients.send(std::move(resp), zmq::send_flags::none);
            }

        }
        //  Handle service-worker activity on backend
        if (items[2].revents & ZMQ_POLLIN) {

            //  Queue worker address for LRU routing
            std::string address = recv_str(service_workers);
            cluster_service_worker_queue.push(address);
            SERVER_LOG_DEBUG <<"cluster_service_worker_queue pushed. size: " << cluster_service_worker_queue.size() << std::endl;

            {
                //  Second frame is empty
                std::string empty = recv_str(service_workers);
                assert(empty.size() == 0);
            }

            //  Third frame is READY or else a client reply address
            std::string client_addr = recv_str(service_workers);

            //  If client reply, send rest back to frontend
            if (client_addr.compare("READY") != 0)
            {
                {
                    std::string empty = recv_str(service_workers);
                    assert(empty.size() == 0);
                }

                zmq::message_t resp;
                service_workers.recv(resp);
                service.send(zmq::buffer(client_addr.data(), client_addr.size()),
                             zmq::send_flags::sndmore);
                service.send(zmq::message_t(), zmq::send_flags::sndmore);
                service.send(std::move(resp), zmq::send_flags::none);
            }
        }
        //  Handle client request
        if (items[3].revents & ZMQ_POLLIN) {
            if (worker_queue.size() == 0)
                // Skip client now, there is no workers!
                continue;

            //  Now get next client request, route to LRU worker
            //  Client request is [address][empty][request]
            std::string client_addr = recv_str(clients);

            {
                std::string empty = recv_str(clients);
                if (empty.size() != 0)
                    // Skip if not our client!
                    continue;
            }

            zmq::message_t req;
            clients.recv(req);

            std::string worker_addr = worker_queue.front();
            worker_queue.pop();
            SERVER_LOG_DEBUG <<"worker_queue poped. size: " << worker_queue.size() << std::endl;

            workers.send(zmq::buffer(worker_addr.data(), worker_addr.size()),
                         zmq::send_flags::sndmore);
            workers.send(zmq::message_t(), zmq::send_flags::sndmore);
            workers.send(zmq::buffer(client_addr.data(), client_addr.size()),
                         zmq::send_flags::sndmore);
            workers.send(zmq::message_t(), zmq::send_flags::sndmore);
            workers.send(std::move(req), zmq::send_flags::none);

        }
        //  Handle service request
        if (items[4].revents & ZMQ_POLLIN)
        {
            if (cluster_service_worker_queue.size() == 0)
                // Skip service-client now, there is no workers!
                continue;

            //  Now get next service request, route to LRU worker
            //  Service request is [address][empty][request]
            std::string service_addr = recv_str(service);

            {
                std::string empty = recv_str(service);
                if (empty.size() != 0)
                {
                    // Skip if not our service-client!
                    continue;
                }
            }

            zmq::message_t req;
            service.recv(req);

            std::string worker_addr = cluster_service_worker_queue.front();
            cluster_service_worker_queue.pop();
            SERVER_LOG_DEBUG <<"cluster_service_worker_queue poped. size: " << cluster_service_worker_queue.size() << std::endl;

            service_workers.send(zmq::buffer(worker_addr.data(), worker_addr.size()),
                                 zmq::send_flags::sndmore);
            service_workers.send(zmq::message_t(), zmq::send_flags::sndmore);
            service_workers.send(zmq::buffer(service_addr.data(), service_addr.size()),
                                 zmq::send_flags::sndmore);
            service_workers.send(zmq::message_t(), zmq::send_flags::sndmore);
            service_workers.send(std::move(req), zmq::send_flags::none);
        }
    }

    SERVER_LOG_DEBUG<<"Destroy everything!"<<std::endl;

}


void ZeroMQServer::Stop()
{
    try{
        zmq::socket_t socket (zmq_context_, zmq::socket_type::pair);
        socket.connect("inproc://control");
        socket.send(zmq::message_t("TERMINATE", 9), zmq::send_flags::none);
        SERVER_LOG_DEBUG << "Sent TERMINATE " << std::endl;
    }catch(const std::exception& e){
        SERVER_LOG_ERROR<<"Excep "<<e.what()<<std::endl;
    }
}


std::string ZeroMQServer::recv_str(zmq::socket_t& socket)
{
    zmq::message_t resp;
    socket.recv(resp);
    return std::string(resp.data<const char>(), resp.size());
}

void ZeroMQServer::try_process(zmq::socket_t& socket)
{
    auto client = recv_str(socket);

    {
        //  Second frame is empty
        std::string empty = recv_str(socket);
        assert(empty.size() == 0);
    }

    zmq::message_t request;
    socket.recv(request);

    std::vector<std::uint8_t> buf;
    try{
        buf = handle_req(request);
    }
    catch(const ServiceException& e)
    {
        json err_resp = {
            {"req_no", e.req_no},
            {"error", {{"msg", e.what()}, {"code", 255}}}

        };
        SERVER_LOG_ERROR << err_resp << std::endl;
        buf = json::to_cbor(err_resp);
    }
    catch(const std::exception& e)
    {
        json err_resp = {{"error", {{"msg", e.what()}, {"code", 255}}}};
        SERVER_LOG_ERROR << err_resp << std::endl;
        buf = json::to_cbor(err_resp);
    }

    socket.send(zmq::buffer(client.data(), client.size()),
                zmq::send_flags::sndmore);
    socket.send(zmq::message_t(), zmq::send_flags::sndmore);
    socket.send(zmq::buffer(buf.data(), buf.size()));
}


void ZeroMQServer::process()
{
    zmq::socket_t socket (zmq_context_, zmq::socket_type::req);
    socket.connect ("inproc://workers");

    // SERVER_LOG_DEBUG << "TID: "<<std::this_thread::get_id()<<" ON duty!!"<<std::endl;
    //  Tell backend we're ready for work
    socket.send(zmq::message_t("READY", 5), zmq::send_flags::none);
    while (true) {
        try{
            try_process(socket);
        }catch(const zmq::error_t& e){
            if (e.num() == ETERM)
                break;
            SERVER_LOG_ERROR<<"Error ("<<e.num()<<") in worker loop: "<<e.what()<<std::endl;
            break;
        }
    }
    // SERVER_LOG_DEBUG <<"end of worker: "<<std::this_thread::get_id()<<std::endl;
}


void ZeroMQServer::try_cluster_service_process(zmq::socket_t& socket)
{
    auto client = recv_str(socket);

    {
        //  Second frame is empty
        std::string empty = recv_str(socket);
        assert(empty.size() == 0);
    }

    zmq::message_t request;
    socket.recv(request);

    std::vector<std::uint8_t> buf;
    try{
        buf = handle_service_req(request);
    }
    catch(const std::exception& e)
    {
        json err_resp = {{"error", {{"msg", e.what()}, {"code", 255}}}};
        buf = json::to_cbor(err_resp);
    }

    socket.send(zmq::buffer(client.data(), client.size()),
                zmq::send_flags::sndmore);
    socket.send(zmq::message_t(), zmq::send_flags::sndmore);
    socket.send(zmq::buffer(buf.data(), buf.size()));
}


void ZeroMQServer::cluster_service_process()
{
    zmq::socket_t socket (zmq_context_, zmq::socket_type::req);
    socket.connect ("inproc://cluster_service_workers");

    // SERVER_LOG_DEBUG << "TID(cl_srv): "<<std::this_thread::get_id()<<" ON duty!!"<<std::endl;
    //  Tell backend we're ready for work
    socket.send(zmq::message_t("READY", 5), zmq::send_flags::none);
    while (true) {
        try{
            try_cluster_service_process(socket);
        }catch(const zmq::error_t& e){
            if (e.num() == ETERM)
                break;
            SERVER_LOG_ERROR<<"Error ("<<e.num()<<") in worker cl_srv loop: "<<e.what()<<std::endl;
            break;
        }
    }
    // SERVER_LOG_DEBUG <<"end of worker(cl_srv): "<<std::this_thread::get_id()<<std::endl;
}

json create_json_err_obj(const std::shared_ptr<Context>& pctx, const std::string& msg)
{

    json resp = {
        {"req_no", pctx->GetRequestId()},
        {"error", {{"msg", msg}, {"code", 255}}}

    };
    return resp;
}

json create_json_err_obj(const std::shared_ptr<Context>& pctx, const Status& status, const std::string& msg)
{

    json resp = {
        {"req_no", pctx->GetRequestId()},
        {"error",
            {
                {"msg", str(boost::format("%s (%d): %s") % msg % status.code() % status.message())},
                {"code", status.code()}

            }
        } };
    return resp;
}

std::vector<int64_t> decode_ids(Unpacker& unpacker);
void encode_ids(const std::vector<int64_t>& ids, Packer& packer);

std::vector<std::uint8_t>
ZeroMQServer::
handle_req(const zmq::message_t& msg)
{
    Unpacker unpacker(msg.data<uint8_t>(), msg.size());
    auto method = unpacker.unpack<string_decoder_t>().copy();
    auto req_id = unpacker.unpack<string_decoder_t>().copy();

    //TODO pass carrier from the client
    // auto span = tracer_->StartSpan(server_rpc_info->method(), {opentracing::ChildOf(span_context_maybe->get())});
    auto span = tracer_->StartSpan(method);
    auto trace_context = std::make_shared<TraceContext>(span);
    auto context = std::make_shared<Context>(req_id);
    context->SetTraceContext(trace_context);

    ConsulReplicaService& consul_replica_service = ConsulReplicaService::GetInstance();
    ConsulShardService& consul_shard_service = ConsulShardService::GetInstance();

    auto _shard_leader = [&] () {return consul_replica_service.AmITheLeader(); };
    auto _cluster_leader = [&] () {return consul_replica_service.AmITheLeader() && consul_shard_service.AmITheLeader(); };
    auto _get_shard_leader = [&] () {return consul_replica_service.GetSession().GetShardLeader(); };
    auto _get_shard_leaders = [&] () {return consul_replica_service.GetSession().GetShardLeaders(); };
    auto _get_cluster_leader = [&] () {return consul_shard_service.GetSession().GetClusterLeader(); };
    auto _get_shard_followers = [&] () {return consul_replica_service.GetSession().GetShardFollowers(); };
    auto _get_cluster_followers = [&] () {return consul_shard_service.GetSession().GetClusterFollowers(); };

    std::time_t current_time = std::time(nullptr);
    if (last_check_time + 10 <= current_time)
    {
        ConsulShardService& consul_shard_service = ConsulShardService::GetInstance();
        std::vector<ServiceHealth> shard_leaders_current = consul_shard_service.GetSession().GetShardLeaders();
        if (shard_leaders_current.size() < shard_leaders.size())
        {
            std::string mess = "Critical cluster state (low shard leaders count)";
            SERVER_LOG_ERROR << "handle req: " << msg.data<uint8_t>() << " " << mess << std::endl;
            return json::to_cbor(create_json_err_obj(context, mess));
        }
        else if (shard_leaders_current.size() == shard_leaders.size())
        {
            std::set<std::string> s_l_c;
            for (auto h: shard_leaders_current)
                s_l_c.insert(h.shard_name);

            std::set<std::string> s_l;
            for (auto h: shard_leaders)
                s_l.insert(h.shard_name);

            std::shared_lock<std::shared_mutex> sl(*mutex_);
            bool res = s_l_c != s_l;
            if (res)
            {
                std::string mess = "Critical cluster state (shards were changed)";
                SERVER_LOG_ERROR << "handle req: " << msg.data<uint8_t>() << " " << mess << std::endl;
                return json::to_cbor(create_json_err_obj(context, mess));
            }
            else
            {
                //std::string mess = "Shards were not changed (ok)!";
                //SERVER_LOG_DEBUG << "handle req: " << msg.data<uint8_t>() << " " << mess << std::endl;
            }
        }
        else
        {
            std::string mess = "Shards leaders count raised (ok)!";
            SERVER_LOG_INFO << "handle req: " << msg.data<uint8_t>() << " " << mess << std::endl;
            std::unique_lock<std::shared_mutex> ul(*mutex_);
            std::swap(shard_leaders, shard_leaders_current);
        }
        last_check_time = std::time(nullptr);
    }

    try {
        json resp;
        json this_instance_resp;
        std::vector<std::string> replicas_success;
        std::vector<std::string> replicas_failed;
        std::vector<std::string> replicas_failed_messages;

        auto _replicates_message_post_processing = [&] ()
        {
            if (this_instance_resp.contains("result"))
            {
                resp = this_instance_resp;
                resp["result"]["copies"] = replicas_success.size() + 1;
                if (replicas_success.size() > 0)
                    resp["result"]["success_replicas"] = replicas_success;
                if (replicas_failed.size() > 0)
                    resp["result"]["replicas_failed"] = replicas_failed;
                if (replicas_failed_messages.size() > 0)
                    resp["result"]["replicas_failed_messages"] = replicas_failed_messages;
            }
            else
            {
                resp = this_instance_resp;
                if (! resp.contains("req_no"))
                    resp["req_no"] = context->GetRequestId();

                std::string mess = "Leader failed";
                if (! resp.contains("error"))
                    resp["error"] = {{"msg", mess}};
                else if (resp["error"].contains("msg"))
                    resp["error"]["msg"] += "; " + mess;
                else
                    resp["error"]["msg"] = mess;

                resp["error"]["code"] = 255;

                resp["error"]["copies"] = replicas_success.size();

                if (replicas_success.size() > 0)
                    resp["error"]["success_replicas"] = replicas_success;
                if (replicas_failed.size() > 0)
                    resp["error"]["replicas_failed"] = replicas_failed;
                if (replicas_failed_messages.size() > 0)
                    resp["error"]["replicas_failed_messages"] = replicas_failed_messages;
            }
        };

        if (method == "add")
        {
            if (_shard_leader())
                replicated_message(context, msg, _get_shard_followers(), replicas_success, replicas_failed, replicas_failed_messages);

            handle_add(context, unpacker, this_instance_resp);
            _replicates_message_post_processing();
        }
        else if (method == "search")
            return handle_search(context, unpacker);
        else if (method == "create")
        {
            if (_shard_leader())
                replicated_message(context, msg, _get_shard_followers(), replicas_success, replicas_failed, replicas_failed_messages);

            handle_create_collection(context, unpacker, this_instance_resp);
            _replicates_message_post_processing();
        }
        else if (method == "train")
        {
            if (_shard_leader())
                replicated_message(context, msg, _get_shard_followers(), replicas_success, replicas_failed, replicas_failed_messages);

            handle_train(context, unpacker, this_instance_resp);
            _replicates_message_post_processing();
        }
    //     else if (method == "search_by_id")
    //         return handle_search_by_id(context, unpacker);
    //     else if (method == "compare_fragments_by_id")
    //         return handle_compare_fragments_by_id(context, unpacker);
    //     else if (method == "compare_fragments")
    //         return handle_compare_fragments(context, unpacker);
    //     else if (method == "drop_table")
    //         return handle_drop_table(context, unpacker);
    //     else if (method == "get_vectors")
    //         return handle_get_vectors(context, unpacker);
    //     else if (method == "clusterize")
    //         return handle_clusterize(context, unpacker);
        else if (method == "info")
            return handle_info(context, unpacker);
        else if (method == "manage_coll")
            return handle_manage_coll(context, unpacker);
        else
            throw ServiceException("Unknown method!", req_id);

        return json::to_cbor(resp);

    }
    catch(const ServiceException& e)
    {
        throw e;
    }
    catch(const std::exception& e)
    {
        throw ServiceException(e.what(), req_id);
    }
}


void ZeroMQServer::replicated_message(const std::shared_ptr<Context>& pctx,
                                      const zmq::message_t& msg,
                                      const std::vector<ServiceHealth>& followers,
                                      std::vector<std::string>& replicas_success,
                                      std::vector<std::string>& replicas_failed,
                                      std::vector<std::string>& replicas_failed_errors
                                      )
{
    for (auto follower: followers)
    {
        std::string follower_address = follower.service_addr  + ":" + std::to_string(follower.port);

        zmq::socket_t follower_socket (zmq_context_, zmq::socket_type::req);
        follower_socket.connect ("tcp://"+follower_address);

        zmq::message_t repl_mess(msg.data(), msg.size());
        follower_socket.send(std::move(repl_mess),zmq::send_flags::none);

        // Blocking call!!! TODO
        zmq::message_t f_resp;
        follower_socket.recv(f_resp);
        Unpacker unpacker(f_resp.data<uint8_t>(), f_resp.size());
        auto f_j_resp = json::from_cbor(unpacker.buffer<char>(),
                                        unpacker.buffer<char>() + unpacker.size());

        if (f_j_resp.contains("error"))
        {
            std::stringstream s_mess;
            s_mess << "replicate to " << follower_address << " failed: " << f_j_resp["error"];
            SERVER_LOG_ERROR << s_mess.str() << std::endl;
            replicas_failed.push_back(follower.service_id);
            replicas_failed_errors.push_back(s_mess.str());
        }
        else if (f_j_resp.contains("result"))
        {
            replicas_success.push_back(follower.service_id);
        }
        else
        {
            std::stringstream s_mess;
            s_mess << "Bad follower '" << follower_address << "' result: " << f_j_resp ;
            throw ServiceException(s_mess.str().c_str(), pctx->GetRequestId());
        }
    }
}


std::vector<int64_t> decode_ids(Unpacker& unpacker)
{
    auto ids_decoder = unpacker.unpack<typed_array_decoder_t<uint64_t>>();
    //Faiss ids are int64_t, why faiss... why!!!??????
    std::size_t ids_size;
    auto pids = ids_decoder.dec(ids_size);
    
    std::vector<int64_t> ids (reinterpret_cast<int64_t*>(pids),
                              reinterpret_cast<int64_t*>(pids+ids_size));
    return ids;
}

void encode_ids(const std::vector<int64_t>& ids, Packer& packer)
{

    using arr_t = std::vector<uint64_t>;
    packer.pack<typed_array_encoder_t<arr_t>>(reinterpret_cast<const arr_t&>(ids));

}


void
ZeroMQServer::handle_train
(const std::shared_ptr<Context>& pctx, Unpacker& unpacker, json& resp)
{
    VectorsData vectors;
    vectors.vector_count_ = unpacker.unpack<size_decoder_t>().copy();
    auto float_decoder = unpacker.unpack<typed_array_decoder_t<float>>();
    vectors.float_data_ = float_decoder.copy();

    auto params = json::from_cbor(unpacker.buffer<char>(),
                                  unpacker.buffer<char>() + unpacker.size());

    int coll_id = params.value("coll_id", 0);
    if (coll_id <= 0)
    {
        resp = create_json_err_obj(pctx, "incorrect coll_id");
        return;
    }

    bool norm = params.value("normalize_L2", false);
    if (norm)
        faiss::fvec_renorm_L2(vectors.float_data_.size() / vectors.vector_count_,
                              vectors.vector_count_,
                              vectors.float_data_.data());

    Indexer& indexer = Indexer::GetInstance();

    Status s = indexer.Train(coll_id, vectors);
    if (!s.ok())
    {
        resp = create_json_err_obj(pctx, s, "Failed to train");
        return;
    }

    resp = {{"req_no", pctx->GetRequestId()},
              {"result", {{"msg", s.message()},
                          {"normalize_L2", norm}
                         }}
            };
    return;
}


template<typename T>
std::vector<T> slice(std::vector<T> const& v, int m, int s)
{
    auto first = v.cbegin() + m;
    auto last = v.cbegin() + m + s;
    
    std::vector<T> vec(first, last);
    return vec;
}


void
ZeroMQServer::handle_add
(const std::shared_ptr<Context>& pctx, Unpacker& unpacker, json& resp)
{
    VectorsData vectors;
    auto float_decoder = unpacker.unpack<typed_array_decoder_t<float>>();
    // unpack embs
    vectors.float_data_ = float_decoder.copy();
    // unpack ids
    std::vector<int64_t> ids = decode_ids(unpacker);

    auto params = json::from_cbor(unpacker.buffer<char>(),
                                  unpacker.buffer<char>() + unpacker.size());
    
    bool multi_level = params.value("multi_level", false);
    std::vector<int64_t> id_to_insert[3];
    
    DocLevelOrganizer& frg_base = DocLevelOrganizer::getInstance();
    
    if (multi_level == true)
    {
        assert (ids.size() % 3 == 0);
        vectors.vector_count_ = ids.size() / 3;
        int cnt = vectors.vector_count_;
        id_to_insert[0] = slice(ids, 0, cnt);
        id_to_insert[1] = slice(ids, cnt + 1, cnt);
        id_to_insert[2] = slice(ids, 2 * cnt + 1, cnt);
        frg_base.insert(id_to_insert);
        vectors.id_array_ = std::vector<int64_t>(id_to_insert[2].begin(), id_to_insert[2].end());
    }
    else 
    {
        vectors.vector_count_ = ids.size();
        vectors.id_array_ = std::vector<int64_t>(ids.begin(), ids.end());
    }

    int coll_id = params.value("coll_id", 0);
    if (coll_id <= 0)
    {
        resp = create_json_err_obj(pctx, "incorrect coll_id");
        return;
    }

    bool norm = params.value("normalize_L2", false);
    if (norm)
        faiss::fvec_renorm_L2(vectors.float_data_.size() / vectors.vector_count_,
                              vectors.vector_count_,
                              vectors.float_data_.data());

    Indexer& indexer = Indexer::GetInstance();
    Status s = indexer.Add(coll_id, vectors);
    
    
    if (!s.ok())
    {
        resp = create_json_err_obj(pctx, s, "Failed to add vectors");
        return;
    }

    resp = {{"req_no", pctx->GetRequestId()},
              {"result", {{"msg", s.message()},
                          {"normalize_L2", norm},
                          {"inserted_vecs", vectors.vector_count_}
                         }}
            };
    return;
}

void
ZeroMQServer::
handle_create_collection(const std::shared_ptr<Context>& pctx, Unpacker& unpacker, json& resp)
{
    auto params = json::from_cbor(unpacker.buffer<char>(),
                                  unpacker.buffer<char>() + unpacker.size());

    int coll_id = params.value("coll_id", 0);
    if (coll_id <= 0)
    {
        resp = create_json_err_obj(pctx, "incorrect coll_id");
        return;
    }

    std::string index_descr = params.value("index", "");
    if (index_descr.empty())
    {
        resp = create_json_err_obj(pctx, "empty 'index'");
        return;
    }

    int dimension = params.value("dimension", 0);
    if (dimension <= 0)
    {
        resp = create_json_err_obj(pctx, "incorrect dimension");
        return;
    }

    bool use_gpu = params.value("use_gpu", false);
    bool useFloat16CoarseQuantizer = params.value("useFloat16CoarseQuantizer", true);
    bool useFloat16 = params.value("useFloat16", true);
    bool usePrecomputed = params.value("usePrecomputed", true);
    bool storeTransposed = params.value("storeTransposed", false);
    
    Indexer& indexer = Indexer::GetInstance();

    Status s = indexer.CreateCollection(coll_id, index_descr, dimension);
    if (!s.ok())
    {
        resp = create_json_err_obj(pctx, s, "Failed to create collection");
        return;
    }
    std::string result_message = s.message();
    Status move_to_gpu_s;
    if (use_gpu)
    {
        move_to_gpu_s = indexer.MoveToGpu(coll_id,
                                          useFloat16CoarseQuantizer, 
                                          useFloat16, 
                                          usePrecomputed, 
                                          storeTransposed
                                          );
        result_message = result_message + "; " + move_to_gpu_s.message();
    }

    resp = {{"req_no", pctx->GetRequestId()},{"result", {{"msg", result_message}}}};
}


std::vector<uint8_t>
ZeroMQServer::
handle_info(const std::shared_ptr<Context>& pctx, Unpacker& unpacker)
{
    Indexer& indexer = Indexer::GetInstance();
    json collections_info;
    Status s = indexer.CollectionInfo(collections_info);

    struct sysinfo sys_info;
    uint64_t total_ram_bytes = 0;
    uint64_t free_ram_bytes = 0;
    if (sysinfo(&sys_info) != -1)
    {
        total_ram_bytes = (uint64_t) sys_info.totalram * sys_info.mem_unit;
        free_ram_bytes = (uint64_t) sys_info.freeram * sys_info.mem_unit;
    }

    json resp = {{"req_no", pctx->GetRequestId()},
                 {"result", {
                              {"msg", s.message()},
                              {"collections_info", collections_info},
                              {"sysinfo",
                                {
                                 {"total_ram_bytes", total_ram_bytes},
                                 {"free_ram_bytes", free_ram_bytes},
                                 {"proc_count", getNumCores()},
                                 {"omp_threads", indexer.omp_threads}
                                }
                              }
                            }
                  }
                };
    return json::to_cbor(resp);
}


std::vector<uint8_t> 
ZeroMQServer::
handle_manage_coll(const std::shared_ptr<Context>& pctx, Unpacker& unpacker)
{
    auto params = json::from_cbor(unpacker.buffer<char>(),
                                  unpacker.buffer<char>() + unpacker.size());
    
    int coll_id = params.value("coll_id", 0);
    if (coll_id <= 0)
    {
        json resp = create_json_err_obj(pctx, "incorrect coll_id");
        return resp;
    }

    bool move_to_gpu = params.value("move_to_gpu", false);
    bool move_to_cpu = params.value("move_to_cpu", false);
    
    bool useFloat16CoarseQuantizer = params.value("useFloat16CoarseQuantizer", true);
    bool useFloat16 = params.value("useFloat16", true);
    bool usePrecomputed = params.value("usePrecomputed", true);
    bool storeTransposed = params.value("storeTransposed", false);

    Indexer& indexer = Indexer::GetInstance();
    
    Status s;
    if (move_to_gpu && move_to_cpu)
    {
        s = Status(0, "Nothing to do");
    }
    else if (move_to_gpu)
    {
        s = indexer.MoveToGpu(coll_id,
                              useFloat16CoarseQuantizer, 
                              useFloat16, 
                              usePrecomputed, 
                              storeTransposed
                              );
    }
    else if (move_to_cpu)
    {
        s = indexer.MoveToCpu(coll_id);
    }
    else
    {
        s = Status(0, "Nothing to do");
    }
    
    json resp = {{"req_no", pctx->GetRequestId()},
                 {"result", {
                              {"msg", s.message()}
                            }
                 }
                };
    
    return json::to_cbor(resp);
}


std::vector<uint8_t>
ZeroMQServer::
handle_search(const std::shared_ptr<Context>& pctx, Unpacker& unpacker)
{
    VectorsData vectors;
    vectors.vector_count_ = unpacker.unpack<size_decoder_t>().copy();
    auto float_decoder = unpacker.unpack<typed_array_decoder_t<float>>();
    vectors.float_data_ = float_decoder.copy();

    return search_impl(pctx, std::move(vectors), unpacker);
}


std::vector<uint8_t>
ZeroMQServer::
search_impl(const std::shared_ptr<Context>& pctx,
            VectorsData&& vectors,
            Unpacker& unpacker)
{
    auto params = json::from_cbor(unpacker.buffer<char>(),
                                  unpacker.buffer<char>() + unpacker.size());
    //SERVER_LOG_DEBUG<<" search params: "<<params<<std::endl;
    
    bool get_parents = params.value("parents", false);

    bool norm = params.value("normalize_L2", true);
    if (norm and not vectors.float_data_.empty())
        faiss::fvec_renorm_L2(vectors.float_data_.size() / vectors.vector_count_,
                              vectors.vector_count_,
                              vectors.float_data_.data());

    int64_t topk = params.value("topk", 10);
    if (topk <= 0)
        return json::to_cbor(create_json_err_obj(pctx, "incorrect topk"));

    int coll_id = params.value("coll_id", 0);
    if (coll_id <= 0)
        return json::to_cbor(create_json_err_obj(pctx, "incorrect coll_id"));

    Indexer& indexer = Indexer::GetInstance();

    ResultIds I;
    ResultDistances D;
    auto status = indexer.Search(coll_id, vectors, topk, I, D);
    if (not status.ok())
        return json::to_cbor(create_json_err_obj(pctx, status, "Failed to search"));

    if (I.size() != D.size())
        return json::to_cbor(create_json_err_obj(pctx, status, "Invalid search results"));

    uint64_t est_size;
    
    if (get_parents)
    {   
        est_size = 500 +
            3* I.size() * sizeof(ResultIds::value_type) + 50 +
            D.size() * sizeof(ResultDistances::value_type) + 50;
    }
    else
    {
        est_size = 500 +
            I.size() * sizeof(ResultIds::value_type) + 50 +
            D.size() * sizeof(ResultDistances::value_type) + 50;
    }
        

    json resp = {{"req_no", pctx->GetRequestId()},
                 {"result", {
                              {"msg", Status::OK().message()},
                              {"found_cnt", I.size()}
                            }
                  }
                };
    auto out_buf = json::to_cbor(resp);

    Packer packer(std::move(out_buf), out_buf.size(), est_size);
    DocLevelOrganizer& frg_base = DocLevelOrganizer::getInstance();
    
    if (get_parents)
    {
        ResultIds I_parent;
        I_parent.resize(I.size() * 3);
        
        int64_t idx_ = 0;
        for (int64_t i : I)
        {
            std::vector<int64_t> parents = frg_base.get_parents(i); 
            I_parent[idx_]            = parents[0]; // level 1
            I_parent[I.size()+idx_]   = parents[1]; // level 2
            I_parent[I.size()*2+idx_] = i;          // level 3
            idx_++;
        }
        encode_ids(I_parent, packer);
    }
    else
    {
        encode_ids(I, packer);
    }

    packer.pack<typed_array_encoder_t<ResultDistances>>(D);
    return packer.move_buffer();
}


std::vector<std::uint8_t>
ZeroMQServer::
handle_service_req(const zmq::message_t& msg)
{
    SERVER_LOG_DEBUG << "handle service req: " << msg.data<uint8_t>() <<  std::endl;
    try {
        // pass
    }
    catch(const ServiceException& e)
    {
        throw e;
    }
    catch(const std::exception& e)
    {
        throw e;
    }
}

} // namespace zeromq
} // namespace server
} // namespace vecindexer
