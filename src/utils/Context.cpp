#include "utils/Context.h"

namespace vecindexer {
namespace server {

TraceContext::TraceContext(std::unique_ptr<opentracing::Span>& span) : span_(std::move(span)) {
}

std::unique_ptr<TraceContext>
TraceContext::Child(const std::string& operation_name) const {
    auto child_span = span_->tracer().StartSpan(operation_name, {opentracing::ChildOf(&(span_->context()))});
    return std::make_unique<TraceContext>(child_span);
}

std::unique_ptr<TraceContext>
TraceContext::Follower(const std::string& operation_name) const {
    auto follower_span = span_->tracer().StartSpan(operation_name, {opentracing::FollowsFrom(&(span_->context()))});
    return std::make_unique<TraceContext>(follower_span);
}

const std::unique_ptr<opentracing::Span>&
TraceContext::GetSpan() const {
    return span_;
}

Context::Context(const std::string& request_id) : request_id_(request_id) {
}

const std::shared_ptr<TraceContext>&
Context::GetTraceContext() const {
    return trace_context_;
}

void
Context::SetTraceContext(const std::shared_ptr<TraceContext>& trace_context) {
    trace_context_ = trace_context;
}
std::shared_ptr<Context>
Context::Child(const std::string& operation_name) const {
    auto new_context = std::make_shared<Context>(request_id_);
    new_context->SetTraceContext(trace_context_->Child(operation_name));
    return new_context;
}

std::shared_ptr<Context>
Context::Follower(const std::string& operation_name) const {
    auto new_context = std::make_shared<Context>(request_id_);
    new_context->SetTraceContext(trace_context_->Follower(operation_name));
    return new_context;
}

}  // namespace server
}  // namespace vecindexer
