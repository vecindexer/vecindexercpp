
#include "utils/SignalUtil.h"

#include "vec_indexer_app.h"
#include "utils/Log.h"

#include <execinfo.h>
#include <signal.h>
#include <string>

namespace vecindexer {

void SignalUtil::HandleSignal(int signum) 
{
    VecIndexerApp& app = VecIndexerApp::GetInstance();
    
    switch (signum) 
    {
        case SIGINT: 
        case SIGUSR2: 
        case SIGTERM: 
        {
            SERVER_LOG_INFO << "Server received signal: " << signum;
            app.Stop();
            exit(0);
        }
        default: 
        {
            SERVER_LOG_INFO << "Server received critical signal: " << signum;
            SignalUtil::PrintStacktrace();
            app.Stop();
            exit(1);
        }
    }
}

void SignalUtil::PrintStacktrace() 
{
    SERVER_LOG_INFO << "Call stack:";

    const int size = 32;
    void* array[size];
    int stack_num = backtrace(array, size);
    char** stacktrace = backtrace_symbols(array, stack_num);
    for (int i = 0; i < stack_num; ++i) {
        std::string info = stacktrace[i];
        SERVER_LOG_INFO << info;
    }
    free(stacktrace);
}

}  // namespace vecindexer
