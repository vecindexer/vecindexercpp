// main application

#include "vec_indexer_app.h"

#include "config/config.h"
#include "utils/Log.h"
#include "utils/LogUtil.h"

#include "consul_wrapper.h"

#include "DocLevelOrganizer.hpp"


namespace vecindexer
{


Status VecIndexerApp::LoadConfig() 
{
    Config& config = Config::GetInstance();
    Status s = config.LoadConfigFile(opts_.config_file);  
    if (!s.ok()) {
        return s;
    }

    return Status::OK();
}


void VecIndexerApp::Init(const VecIndexerAppOptions& opts) 
{
    opts_ = opts;
};


Status VecIndexerApp::Start()
{
    try
    {
        /* Read config file */
        Status s = LoadConfig();
        if (!s.ok()) {
            APP_LOG_ERROR << "VecIndexerApp failed to load config!" << std::endl;
            return s;
        }
        
        std::string failed_base_message = "VecIndexerApp was not started: ";
        
        s = SetupTimezone();
        if (!s.ok()) {
            return Status(SERVER_UNEXPECTED_ERROR,  failed_base_message + s.ToString());
        }
        
        InitLog(opts_.log_config_file);
        
        s = StartService();
        
        if (!s.ok())
            return s;

        return Status(SERVER_SUCCESS, "VecIndexerApp started");
    }
    catch (std::exception& ex) 
    {
        std::string str = "VecIndexerApp failed with exception: " + std::string(ex.what());
        return Status(SERVER_UNEXPECTED_ERROR, str);
    }
}


Status SetupTimezone()
{
    Config& config = Config::GetInstance();
    std::string time_zone;
    Status s = config.GetServerConfigTimeZone(time_zone);
    if (!s.ok()) {
        APP_LOG_ERROR << "Fail to get server config timezone" << std::endl;
        return s;
    }

    if (time_zone.length() == 3) {
        time_zone = "CUT";
    } else {
        int time_bias = std::stoi(time_zone.substr(3, std::string::npos));
        if (time_bias == 0) {
            time_zone = "CUT";
        } else if (time_bias > 0) {
            time_zone = "CUT" + std::to_string(-time_bias);
        } else {
            time_zone = "CUT+" + std::to_string(-time_bias);
        }
    }

    if (setenv("TZ", time_zone.c_str(), 1) != 0) {
        return Status(SERVER_UNEXPECTED_ERROR, "Fail to setenv");
    }
    tzset();
    return Status::OK();
}


Status VecIndexerApp::StartService()
{
    BaseConfig& frg_conf = BaseConfig::getInstance();
    DocLevelOrganizer& frg_base = DocLevelOrganizer::getInstance();
    frg_conf.read_config_ini("etc/config/multilevel_conf.ini");
    frg_base.config = frg_conf;
    frg_base.initialize();


    ConsulReplicaService& consul_replica_service = ConsulReplicaService::GetInstance();
    ConsulShardService& consul_shard_service = ConsulShardService::GetInstance();
    
    server::Indexer& indexer = server::Indexer::GetInstance();
    server::zeromq::ZeroMQServer& server = server::zeromq::ZeroMQServer::GetInstance();

    server.Start();
    
    std::string failed_base_message = "VecIndexerApp was not started: "; 
    
    Status s = indexer.Start();
    if (s.ok())
        APP_LOG_INFO << s.ToString() << std::endl;
    else
        return Status(SERVER_UNEXPECTED_ERROR,  failed_base_message + s.ToString());
    
    s = consul_replica_service.Start();
    if (s.ok())
        APP_LOG_INFO << s.ToString() << std::endl;
    else
        return Status(SERVER_UNEXPECTED_ERROR,  failed_base_message + s.ToString());
    
    s = consul_shard_service.Start();
    if (s.ok())
        APP_LOG_INFO << s.ToString() << std::endl;
    else
        return Status(SERVER_UNEXPECTED_ERROR,  failed_base_message + s.ToString());


    return Status::OK();
}


void VecIndexerApp::Stop() 
{
    APP_LOG_INFO << "VecIndexerApp finishing ..." << std::endl;
    StopService();
    APP_LOG_INFO << "VecIndexerApp finished!" << std::endl;
}


void
VecIndexerApp::StopService()
{
    server::zeromq::ZeroMQServer::GetInstance().Stop();
    ConsulShardService::GetInstance().Stop();
    ConsulReplicaService::GetInstance().Stop();
    server::Indexer::GetInstance().Stop();
}


} // namespace vecindexer
