#define BOOST_TEST_MODULE base_consul_wrapper_test

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>

#include <iostream>
#include <fstream>
#include <cstdio>

#include "config/config.h"
#include "consul_wrapper.h"

namespace bunt = boost::unit_test;

using namespace vecindexer;


void load_config(const std::string& config_filename, Config& config)
{
    Status s = config.LoadConfigFile(config_filename);  
    if (!s.ok()) {
        throw std::runtime_error(s.ToString());
    }
}


const int EXTRA_SLEEP_MS = 1000 + 150;


class Fixture{
  public:
    const std::string default_config = R"(
version: 0.1

consul:
  address: http://localhost
  port: 8500
  wait: 1s
  leadership_sleep_ms: 300
  service_check: 

server_config:
  service_name: testvecindexer
  shard_name: 1
  address: 127.0.0.1
  port: 21000
  cluster_service_address: 127.0.0.1
  cluster_service_port: 21001
  time_zone: UTC+3

storage_config:
  primary_path: /data/testvecindexer/
)";
    
    std::string temp_filename;
    
    inline Fixture() 
    {
        temp_filename = create_test_unique_path();
        save_config(temp_filename, default_config);
        
        Config& config = Config::GetInstance();
        Status s = config.LoadConfigFile(temp_filename);  
        if (!s.ok()) {
            throw std::runtime_error(s.ToString());
        }
        
    }
    
    inline std::string create_test_unique_path()
    {
        std::string base_dir = "/tmp/vecindexer_test.d";
        if (!boost::filesystem::exists(base_dir))
            boost::filesystem::create_directory(base_dir);
        return base_dir + "/" + boost::filesystem::unique_path().native();
    }
    
    inline void save_config(const std::string& config_filename,
                            const std::string& config_raw_string
                           )
    {
        std::ofstream of;
        of.open(config_filename);
        of << config_raw_string ;
        of.close();
    }
    
    inline ~Fixture()
    {
        if (!temp_filename.empty())
            std::remove(temp_filename.c_str());
    }
};

class LoadConfigTestingFixture: Fixture 
{
  public:
    const std::string config_t = R"(
version: 0.1

consul:
  address: http://test_localhost
  port: 8499
  wait: 1s
  leadership_sleep_ms: 12345
  service_check: 1

server_config:
  service_name: testvecindexertest
  shard_name: test_shard_name
  replica_name: test_replica_name
  address: 196.1.1.1
  port: 21005
  cluster_service_address: 196.1.1.2
  cluster_service_port: 21015
  time_zone: UTC+1

storage_config:
  primary_path: /data/testvecindexertest/
)";
    std::string temp_filename_t;

    inline LoadConfigTestingFixture() 
    {
        temp_filename_t = Fixture::create_test_unique_path(); 
        Fixture::save_config(temp_filename_t, config_t);
    }
    
    inline void EnsureFilesRemoved()
    {
        if (!temp_filename_t.empty())
            std::remove(temp_filename_t.c_str());

        temp_filename_t = "";
    }
    
    inline ~LoadConfigTestingFixture()
    {
        EnsureFilesRemoved();
    }
};


class ReplicationTestingFixture: Fixture 
{
  public:
        const std::string config_1 = R"(
version: 0.1

consul:
  address: http://localhost
  port: 8500
  wait: 1s
  leadership_sleep_ms: 300
  service_check: 

server_config:
  service_name: testvecindexer
  shard_name: 1
  address: 127.0.0.1
  port: 21001
  cluster_service_address: 127.0.0.1
  cluster_service_port: 21011
  time_zone: UTC+3

storage_config:
  primary_path: /data/testvecindexer1/
)";
    const std::string config_2 = R"(
version: 0.1

consul:
  address: http://localhost
  port: 8500
  wait: 1s
  leadership_sleep_ms: 300
  service_check: 

server_config:
  service_name: testvecindexer
  shard_name: 1
  replica_name: 2
  address: 127.0.0.1
  port: 21002
  cluster_service_address: 127.0.0.1
  cluster_service_port: 21012
  time_zone: UTC+3

storage_config:
  primary_path: /data/testvecindexer2/
)";
    const std::string config_3 = R"(
version: 0.1

consul:
  address: http://localhost
  port: 8500
  wait: 1s
  leadership_sleep_ms: 350
  service_check: 

server_config:
  service_name: testvecindexer
  shard_name: 1
  replica_name: 3
  address: 127.0.0.1
  port: 21003
  cluster_service_address: 127.0.0.1
  cluster_service_port: 21013
  time_zone: UTC+3

storage_config:
  primary_path: /data/testvecindexer3/
)";
    std::string temp_filename_1;
    std::string temp_filename_2;
    std::string temp_filename_3;

    inline ReplicationTestingFixture() 
    {
        temp_filename_1 = Fixture::create_test_unique_path(); 
        temp_filename_2 = Fixture::create_test_unique_path(); 
        temp_filename_3 = Fixture::create_test_unique_path(); 
        Fixture::save_config(temp_filename_1, config_1);
        Fixture::save_config(temp_filename_2, config_2);
        Fixture::save_config(temp_filename_3, config_3);   
    }
    
    inline void EnsureFilesRemoved()
    {
        if (!temp_filename_1.empty())
            std::remove(temp_filename_1.c_str());
        if (!temp_filename_2.empty())
            std::remove(temp_filename_2.c_str());
        if (!temp_filename_3.empty())
            std::remove(temp_filename_3.c_str());
        
        temp_filename_1 = "";
        temp_filename_2 = "";
        temp_filename_3 = "";
    }
    
    inline ~ReplicationTestingFixture()
    {
        EnsureFilesRemoved();
    }
};


template <typename ServiceType>
void default_start_acquire_and_release_leadership_test_func()
{
    ServiceType service = ServiceType::GetInstance();
    Status s = service.Start();
    BOOST_TEST(s.ok());
    sleep_for(service.GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    
    // Check that I am the Leader
    BOOST_TEST(service.GetSession().AmITheLeader());
    // Release leadership
    
    BOOST_TEST(service.GetSession().ReleaseSession());
    // I have no leadership more
    BOOST_TEST(!service.GetSession().AmITheLeader());
    
    sleep_for(service.GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
     // I am the Leader again!
    BOOST_TEST(service.GetSession().AmITheLeader()); 
    
    service.Stop();
}


BOOST_FIXTURE_TEST_CASE(default_start_acquire_and_release_leadership_test, Fixture)
{   
    default_start_acquire_and_release_leadership_test_func<ConsulReplicaService>();
    
    ConsulReplicaService& consul_replica_service_ = ConsulReplicaService::GetInstance();
    consul_replica_service_.Start();
    default_start_acquire_and_release_leadership_test_func<ConsulShardService>();
    consul_replica_service_.Stop();
}


BOOST_FIXTURE_TEST_CASE(load_config_test_case, LoadConfigTestingFixture)
{
    Config config = Config();
    load_config(LoadConfigTestingFixture::temp_filename_t, config);
    
    std::string value;
    
    config.GetServerConfigAddress(value);
    BOOST_TEST(value == "196.1.1.1");
    config.GetServerConfigPort(value);
    BOOST_TEST(value == "21005");
    config.GetServerConfigClusterServiceAddress(value);
    BOOST_TEST(value == "196.1.1.2");
    config.GetServerConfigClusterServicePort(value);
    BOOST_TEST(value == "21015");
    config.GetServerConfigTimeZone(value);
    BOOST_TEST(value == "UTC+1");
    
    config.GetServerServiceName(value);
    BOOST_TEST(value == "testvecindexertest");
    config.GetServerShardName(value);
    BOOST_TEST(value == "test_shard_name");
    config.GetServerReplicaName(value);
    BOOST_TEST(value == "test_replica_name");
    config.GetConsulServiceCheck(value);
    BOOST_TEST(value == "1");
    
    config.GetConsulAddress(value);
    BOOST_TEST(value == "http://test_localhost");
    config.GetConsulPort(value) ;
    BOOST_TEST(value == "8499");
    
    config.GetConsulWait(value) ;
    BOOST_TEST(value == "1s");
    
    int i_value;
    config.GetLeadershipSleepMs(i_value) ;
    BOOST_TEST(i_value == 12345);

    /* storage config */
    config.GetStorageConfigPrimaryPath(value);
    BOOST_TEST(value == "/data/testvecindexertest/");

    /* tracing config */
    config.GetTracingConfigJsonConfigPath(value);
    // pass
}


BOOST_FIXTURE_TEST_CASE(leadership_test, ReplicationTestingFixture)
{
    auto init_and_get_service = [] (const std::string& config_filename, ConsulService* service)
    {
        Config config = Config();
        load_config(config_filename, config);
        ConsulServiceOpts opts = ConsulServiceOpts();
        Status s = get_consul_service_opts_from_config(opts, config);
        opts.service_name_suffix = "_leadership_test";
        BOOST_TEST(s.ok());
        s = service->Start(opts);
        BOOST_TEST(s.ok());
    };
    
    ConsulService* service_1 = new ConsulService();
    init_and_get_service(ReplicationTestingFixture::temp_filename_1, 
                         service_1);
    
    ConsulService* service_2 = new ConsulService();
    init_and_get_service(ReplicationTestingFixture::temp_filename_2, 
                         service_2);

    ConsulService* service_3 = new ConsulService();
    init_and_get_service(ReplicationTestingFixture::temp_filename_3, 
                         service_3);
    
    sleep_for(service_3->GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    
    auto check_leaders_cnt = [] (std::vector<ConsulService*>& services)
    {
        int leaders_count = 0;
        for (auto s: services)
        {
            if (s->GetSession().AmITheLeader())
                leaders_count++;
        }
        return leaders_count;
    };
    
    auto release_leadership = [] (std::vector<ConsulService*>& services)
    {
        for (auto s: services)
        {
            if (s->GetSession().AmITheLeader() && s->GetSession().ReleaseSession())
                return true;
        }
        return false;
    };
    
    std::vector<ConsulService*> services = {service_1, service_2, service_3};
    
    // Check that there is a one leader
    BOOST_TEST(check_leaders_cnt(services) == 1);
    
    // Release current leader
    BOOST_TEST(release_leadership(services));
    BOOST_TEST(check_leaders_cnt(services) == 0);
    sleep_for(service_1->GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    
    // Check that there is a one leader
    BOOST_TEST(check_leaders_cnt(services) == 1);

    service_1->Stop();
    
    // There is must be still one leader
    sleep_for(service_2->GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    BOOST_TEST(check_leaders_cnt(services) == 1);
    
    service_2->Stop();
    
    // There is must be still one leader
    sleep_for(service_3->GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    BOOST_TEST(check_leaders_cnt(services) == 1);

    service_3->Stop();
    
    // There is no leader more
    sleep_for(service_3->GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    BOOST_TEST(check_leaders_cnt(services) == 0);
    
    sleep_for(service_3->GetOpts().leadership_sleep_ms + EXTRA_SLEEP_MS);
    
    // There is no leader more (again)
    BOOST_TEST(check_leaders_cnt(services) == 0);
    
    ReplicationTestingFixture::EnsureFilesRemoved();
}
